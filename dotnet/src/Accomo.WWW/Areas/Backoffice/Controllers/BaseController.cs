using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Accomo.Db;
using Accomo.Models;
using Accomo.Models.Utilities;
using Accomo.Models.Security;
using Microsoft.AspNetCore.Authorization;


namespace  Accomo.WWW.Areas.Backoffice.Controllers 
{
    //[Area("Backoffice")]
    [Authorize(Roles = "Administrator,Editor")]
    public abstract class BaseController : Controller 
    {
        public ApplicationDbContext ApplicationDbContext { get; set; }
        public UserManager<ApplicationUser>  _userManager  { get; set; }
        public RoleManager<ApplicationRole>  _roleManager  { get; set; }
        
        public BaseController() 
        {
        }

        public BaseController([FromServices]ApplicationDbContext applicationDbContext) 
        {
            ApplicationDbContext = applicationDbContext;
        }

        public BaseController([FromServices]ApplicationDbContext applicationDbContext, [FromServices]UserManager<ApplicationUser>  applicationUserManager, [FromServices]RoleManager<ApplicationRole>  applicationRoleManager) 
        {
            _userManager = applicationUserManager;
            _roleManager = applicationRoleManager;
            ApplicationDbContext = applicationDbContext;
            
        }

        public void AddAlert(AlertType alertType, string message, bool dismissable)
        {
            var alerts = TempData.ContainsKey("SRVALERT")
                ? (List<Alert>)TempData["SRVALERT"]
                : new List<Alert>();
    
            alerts.Add(new Alert
            {
                Type = alertType,
                Message = message,
                Dismissable = dismissable
            });
    
            TempData["SRVALERT"] = alerts;
        }

        public void AddAlert(Alert alert)
        {
            
            var alerts = TempData.ContainsKey("SRVALERT")
                ? (List<Alert>)TempData["SRVALERT"]
                : new List<Alert>();
    
            if(alert != null)
            {
                alerts.Add(alert);
            }
    
            TempData["SRVALERT"] = alerts;
            
        }
    }
}