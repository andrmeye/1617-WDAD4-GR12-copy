using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Accomo.Db;
using Accomo.Models;
using Accomo.Models.Security;
using Accomo.Models.Utilities;
using Accomo.Models.ViewModels;

namespace Accomo.WWW.Areas.Backoffice.Controllers 
{
    

    [Area("Backoffice")]
    public class UserController : Controller
    {
        public  UserManager<ApplicationUser> _userManager;
        public  RoleManager<ApplicationRole> _roleManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ILogger _logger;
        public ApplicationDbContext _applicationdbContext;
        public List<ApplicationUser> users;

        public UserController(
            UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> roleManager,            
            SignInManager<ApplicationUser> signInManager,
            ILoggerFactory loggerFactory,
            ApplicationDbContext applicationdbContext
)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _signInManager = signInManager;
            _logger = loggerFactory.CreateLogger<UserController>();
            _applicationdbContext = applicationdbContext;

            
        }
        public async Task<IActionResult> Index(string sortParam) {
          
            // Add usernames to user
            users = 
            await _userManager.Users
            .Include(u => u.Roles)
            .ToListAsync();
          
          // Add rolenames into not mapped-property RoleNames
            foreach(var user in users){
                List<string> rolnamen = new List<string>();
                foreach(var role in user.Roles){
                    if(role.RoleId != null){

                        string rolz;
                        rolz =_applicationdbContext.Roles.FirstOrDefault(o => o.Id == role.RoleId).Name;
                        rolnamen.Add(rolz);
                        
                    }

                }
                user.RoleNames = rolnamen;


            }
              
            // Sorting
            ViewBag.UserNameSortParam = "username_asc";
            ViewBag.NameSortParam = "name_asc";
            ViewBag.CreatedSortParam = "created_asc";
            
            if(String.IsNullOrEmpty(sortParam))
            {
                users = users.OrderBy(o => o.UserName).ToList();
            }
            else
            {
                switch(sortParam)
                {
                    case "username_asc":
                        ViewBag.UserNameSortParam = "username_desc";
                        users = users.OrderBy(o => o.UserName).ToList();
                        break;
                    case "username_desc":
                        ViewBag.UserNameSortParam = "username_asc";
                        users = users.OrderByDescending(o => o.UserName).ToList();
                        break;
                    case "name_asc":
                        ViewBag.NameSortParam = "name_desc";
                        users = users.OrderBy(o => o.LastName).ThenBy(o => o.FirstName).ToList();
                        break;
                    case "name_desc":
                        ViewBag.NameSortParam = "name_asc";
                        users = users.OrderByDescending(o => o.LastName).ThenBy(o => o.FirstName).ToList();
                        break; 
                    case "created_asc":
                        ViewBag.CreatedSortParam = "created_desc";
                        users = users.OrderBy(o => o.CreatedAt).ToList();
                        break;
                    case "created_desc":
                        ViewBag.CreatedSortParam = "created_asc";
                        users = users.OrderByDescending(o => o.CreatedAt).ToList();
                        break;                    
                }
                
            }
            if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest") 
            {
                return PartialView("_ListPartial", users);
            }
            
            return View(users);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LogOff()
        {
            
            await _signInManager.SignOutAsync();
            _logger.LogInformation(4, "User logged out.");
            return RedirectToAction("Login", "Account", new { area = "" });
        }

        
        
        [HttpGet]
        public async Task<IActionResult> Edit(Guid id)
        {
            ApplicationUser model = _applicationdbContext.ApplicationUsers.Include(o => o.Roles).FirstOrDefault(o => o.Id == id);
          // Add rolenames into not mapped-property RoleNames
         
                List<string> rolnamen = new List<string>();
                foreach(var role in model.Roles){
                    if(role.RoleId != null){

                        string rolz;
                        rolz =_applicationdbContext.Roles.FirstOrDefault(o => o.Id == role.RoleId).Name;
                        rolnamen.Add(rolz);
                        
                    }

                }
                model.RoleNames = rolnamen;


       
            if(model == null)
            {
                TempData["Status"] = "Er is geen gelijknamige record gevonden.";
                return RedirectToAction("Index");
            }

            var viewModel = await ViewModel(model);
            
            return View(model);
        }

        [HttpGet("[area]/[controller]/[action]/{id:guid}")]
        public async Task<IActionResult> DeleteRoleFromUser(Guid id, string roleName)
        {
            try{
            var user = await _userManager.FindByIdAsync(Convert.ToString(id));
            
            if(user == null)
                {
                    TempData["Status"] = ApplicationDbContextMessage.NOTEXISTS;
                    throw new Exception();
                }

            await _userManager.RemoveFromRoleAsync(user, roleName);

            // Test result

            var result = await _userManager.UpdateAsync(user);
            if (result.Succeeded)
                {
                TempData["Status"] = "Rol met naam: " + roleName + " is succesvol verwijderd van" + " " + user.UserName;
                }
                return RedirectToAction("Index");
        
            
            } catch (Exception ex)
            {

                TempData["Status"] = ex.Message;
                return RedirectToAction("Index");

            }
            
            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<IActionResult> AddRoleToUser(Guid id)
        {
            if (id == null)
            {
                return new StatusCodeResult(400);
            }
            
            var model = _applicationdbContext.ApplicationUsers.Include(o => o.Roles).FirstOrDefault(o => o.Id == id);
            
            if(model == null)
            {
                TempData["Status"] = "User niet gevonden";
                return RedirectToAction("Index");
            }

            var viewModel = await ViewModel(model);
            
            return View(viewModel);
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddRoleToUser(UserViewModel model)
        {
            // Add with DbContext
            //var roleName = _applicationdbContext.ApplicationRoles.FirstOrDefault(o => o.Id == model.ApplicationRole.Id);
            //Console.WriteLine(roleName);
            //await _userManager.AddToRoleAsync(model.ApplicationUser, model.ApplicationRole.Name);

          
            var alert = new Alert();
            try 
            {
                if(!ModelState.IsValid)
                {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.INVALID;
                    TempData["Status"] = ApplicationDbContextMessage.INVALID;
                    throw new Exception();
                }   

                // Convert Guid to string
                var newid = Convert.ToString(model.ApplicationRole.Id);
                // Get role by Id
                var originalRole = await _roleManager.FindByIdAsync(newid);

                // Get user by Id
                var originalModel = _userManager.Users.FirstOrDefault(m => m.Id == model.ApplicationUser.Id);

                // Check if user exists
                 if(originalModel == null) 
                {
                    TempData["Status"] = ApplicationDbContextMessage.NOTEXISTS;
                    throw new Exception();
                }

                var duplicate = await _userManager.IsInRoleAsync(originalModel, originalRole.Name);
                Console.WriteLine("/////////////////////Duplicaat///////////////");
                Console.WriteLine(duplicate);

                if(duplicate){
                    TempData["Status"] = "Deze rol behoort al toe aan deze gebruiker";
                    return RedirectToAction("Index");
                }
                // Add role to user
                await _userManager.AddToRoleAsync(model.ApplicationUser, originalRole.Name);

                // Test result
                var result = await _userManager.UpdateAsync(originalModel);
                if (result.Succeeded)
                    {
                    TempData["Status"] = "Role met naam: " + originalRole.Name + " is succesvol toegevoegd aan" + " " + originalModel.UserName;
                    }
                    return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                alert.Type = AlertType.Error;
                alert.ExceptionMessage = ex.Message;
                TempData["Status"] = ex.Message;

                model = await ViewModel(model.ApplicationUser);
                ModelState.AddModelError(string.Empty, alert.ExceptionMessage);
            }
            
                return RedirectToAction("Index");
        }
        /*
        //[HttpGet("[area]/[controller]/[action]/{id:int}")]
        [HttpGet]
        public async Task<IActionResult> Delete(string id, [FromQuery] ActionType actionType)
        {
            var model = await _roleManager.FindByIdAsync(id);  
            Console.WriteLine("Naam van de role: " + id);
            //var newId = new Guid(id);
            //var model = await _applicationdbContext.ApplicationRoles.FirstOrDefaultAsync(o => o.Name == roleName);
            if(model == null)
            {
                Console.WriteLine("BROLLLL");
                return RedirectToAction("Index");
            }
            
            var viewModel = new ActionRoleViewModel()
            {
                ApplicationRole = model,
                ActionType = actionType
            };
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(ActionRoleViewModel model)
        {
            var alert = new Alert();// Alert    
            try
            {
                var originalModel = _roleManager.Roles.FirstOrDefault(m => m.Id == model.ApplicationRole.Id);
                if(originalModel == null)
                {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.NOTEXISTS;
                    TempData["Status"] = ApplicationDbContextMessage.NOTEXISTS;
                    throw new Exception();
                }
                try{

                
                    switch(model.ActionType)
                    {
                        case ActionType.Delete:
                            alert.Message = ApplicationDbContextMessage.DELETEOK;
                            TempData["Status"] = ApplicationDbContextMessage.DELETEOK;
                            await _roleManager.DeleteAsync(originalModel);
                            break;
                        case ActionType.SoftDelete:
                            alert.Message = ApplicationDbContextMessage.SOFTDELETEOK;
                            TempData["Status"] = ApplicationDbContextMessage.SOFTDELETEOK;
                            originalModel.DeletedAt = DateTime.Now;
                            await _roleManager.UpdateAsync(originalModel);
                            break;
                        case ActionType.SoftUnDelete:
                            alert.Message = ApplicationDbContextMessage.SOFTUNDELETEOK;
                            TempData["Status"] = ApplicationDbContextMessage.SOFTUNDELETEOK;
                            originalModel.DeletedAt = (Nullable<DateTime>)null;
                            await _roleManager.UpdateAsync(originalModel);
                            break;
                    }
                }catch(Exception ex)
                {
                        switch(model.ActionType)
                    {
                        case ActionType.Delete:
                            TempData["Status"] = ApplicationDbContextMessage.DELETENOK;
                            break;
                        case ActionType.SoftDelete:
                            TempData["Status"] = ApplicationDbContextMessage.SOFTDELETENOK;
                            break;
                        case ActionType.SoftUnDelete:
                            TempData["Status"] = ApplicationDbContextMessage.SOFTUNDELETENOK;
                            break;
                    }

                }
                
                    
                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(alert);
                }
                else
                {
                    return RedirectToAction("Index");
                }

            }
            catch (Exception ex)
            {
                alert.Type = AlertType.Error;
                alert.ExceptionMessage = ex.Message;

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(alert);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
        }
        */
        private async Task<UserViewModel> ViewModel(ApplicationUser applicationUser = null) 
        {
            
            var roles = await _applicationdbContext.ApplicationRoles.Select(o => new SelectListItem {
                Text = o.Name,
                Value = Convert.ToString(o.Id)
            }).ToListAsync();
            roles.Add(new SelectListItem() {Text="-- Selecteer een categorie --", Value=""});
            roles = roles.OrderBy(g => g.Text).ToList();
            
            var viewModel = new UserViewModel 
            {
                Roles = roles,
                ApplicationUser = (applicationUser != null  )?applicationUser:new ApplicationUser(),
            };

            return viewModel;
        }
    }
}