using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Accomo.Db;
using Accomo.Models;
using Accomo.Models.Security;
using Microsoft.AspNetCore.Authorization;

namespace  Accomo.WWW.Areas.Backoffice.Controllers 
{
    [Area("Backoffice")]
    public class HomeController : BaseController 
    {
        
        public HomeController():base()
        {
        }

        public IActionResult Index() 
        {
            return View();
        }
    }
}