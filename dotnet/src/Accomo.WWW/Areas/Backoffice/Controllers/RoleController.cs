using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Accomo.Db;
using Accomo.Models;
using Accomo.Models.Security;
using Accomo.Models.Utilities;
using Accomo.Models.ViewModels;

namespace Accomo.WWW.Areas.Backoffice.Controllers 
{
    [Area("Backoffice")]
    public class RoleController  : Controller
    {   
        public  UserManager<ApplicationUser> _userManager;
        public  RoleManager<ApplicationRole> _roleManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        public ApplicationDbContext _applicationdbContext;
        public RoleController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            RoleManager<ApplicationRole> roleManager,
            ApplicationDbContext applicationdbContext)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _applicationdbContext = applicationdbContext;

        }

        public async Task<IActionResult> Index(string sortParam) {
            List<ApplicationRole> roles = 
            await _roleManager.Roles.ToListAsync();

            // Sorting
            ViewBag.NameSortParam = "name_asc";


            if(String.IsNullOrEmpty(sortParam))
            {
                roles = roles.OrderBy(o => o.Name).ToList();
            }
            else
            {
                switch(sortParam)
                {
                    case "name_asc":
                        ViewBag.NameSortParam = "name_desc";
                        roles = roles.OrderBy(o => o.Name).ToList();
                        break;
                    case "name_desc":
                        ViewBag.NameSortParam = "name_asc";
                        roles = roles.OrderByDescending(o => o.Name).ToList();
                        break;                    
                }
            }
            if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest") 
            {
                return PartialView("_ListPartial", roles);
            }
            
            return View(roles);
        }
        
        [HttpGet]
        public async Task<IActionResult> Create() 
        {  
            var viewModel = await ViewModel();
            
            return View(viewModel);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(RoleViewModel model)
        {
            var alert = new Alert();
            if (ModelState.IsValid)
            {
                try{
                    var role = new ApplicationRole{ Name = model.ApplicationRole.Name, Beschrijving = model.ApplicationRole.Beschrijving};
                    var result = await _roleManager.CreateAsync(role);
                    if (result.Succeeded)
                    {
                    TempData["Status"] = "Role met naam: " + model.ApplicationRole.Name + " is succesvol aangemaakt.";
                    }
                    return RedirectToAction("Index");
                }catch(Exception ex)
            {
               
                TempData["Status"] = ex.Message;
                                
                return View(model);

            }
                

            }
            return View(model);
        }
        
        [HttpGet]
        public async Task<IActionResult> Edit(string id)
        {
            
            var model = await _roleManager.FindByIdAsync(id);         
            if(model == null)
            {
                TempData["Status"] = "Er is geen gelijknamige record gevonden.";
                return RedirectToAction("Index");
            }

            var viewModel = await ViewModel(model);
            
            return View(viewModel);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(RoleViewModel model)
        {
            var alert = new Alert();
            try 
            {
                if(!ModelState.IsValid)
                {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.INVALID;
                    TempData["Status"] = ApplicationDbContextMessage.INVALID;
                    throw new Exception();
                }   
                    
                var originalModel = _roleManager.Roles.FirstOrDefault(m => m.Id == model.ApplicationRole.Id);
                
                if(originalModel == null) 
                {
                    TempData["Status"] = ApplicationDbContextMessage.NOTEXISTS;
                    throw new Exception();
                }

                originalModel.Name = model.ApplicationRole.Name;
                originalModel.Beschrijving = model.ApplicationRole.Beschrijving;


                var result = await _roleManager.UpdateAsync(originalModel);
                if (result.Succeeded)
                    {
                    TempData["Status"] = "Role met naam: " + model.ApplicationRole.Name + " is succesvol bewerkt.";
                    }
                    return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                alert.Type = AlertType.Error;
                alert.ExceptionMessage = ex.Message;
                TempData["Status"] = ex.Message;

                model = await ViewModel(model.ApplicationRole);
                ModelState.AddModelError(string.Empty, alert.ExceptionMessage);
            }
            return View(model);
        }
        
        //[HttpGet("[area]/[controller]/[action]/{id:int}")]
        [HttpGet]
        public async Task<IActionResult> Delete(string id, [FromQuery] ActionType actionType)
        {
            var model = await _roleManager.FindByIdAsync(id);  
            Console.WriteLine("Naam van de role: " + id);
            //var newId = new Guid(id);
            //var model = await _applicationdbContext.ApplicationRoles.FirstOrDefaultAsync(o => o.Name == roleName);
            if(model == null)
            {
                Console.WriteLine("BROLLLL");
                return RedirectToAction("Index");
            }
            
            var viewModel = new ActionRoleViewModel()
            {
                ApplicationRole = model,
                ActionType = actionType
            };
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(ActionRoleViewModel model)
        {
            var alert = new Alert();// Alert    
            try
            {
                var originalModel = _roleManager.Roles.FirstOrDefault(m => m.Id == model.ApplicationRole.Id);
                if(originalModel == null)
                {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.NOTEXISTS;
                    TempData["Status"] = ApplicationDbContextMessage.NOTEXISTS;
                    throw new Exception();
                }
                try{

                
                    switch(model.ActionType)
                    {
                        case ActionType.Delete:
                            alert.Message = ApplicationDbContextMessage.DELETEOK;
                            TempData["Status"] = ApplicationDbContextMessage.DELETEOK;
                            await _roleManager.DeleteAsync(originalModel);
                            break;
                        case ActionType.SoftDelete:
                            alert.Message = ApplicationDbContextMessage.SOFTDELETEOK;
                            TempData["Status"] = ApplicationDbContextMessage.SOFTDELETEOK;
                            originalModel.DeletedAt = DateTime.Now;
                            await _roleManager.UpdateAsync(originalModel);
                            break;
                        case ActionType.SoftUnDelete:
                            alert.Message = ApplicationDbContextMessage.SOFTUNDELETEOK;
                            TempData["Status"] = ApplicationDbContextMessage.SOFTUNDELETEOK;
                            originalModel.DeletedAt = (Nullable<DateTime>)null;
                            await _roleManager.UpdateAsync(originalModel);
                            break;
                    }
                }catch(Exception ex)
                {
                        switch(model.ActionType)
                    {
                        case ActionType.Delete:
                            TempData["Status"] = ApplicationDbContextMessage.DELETENOK;
                            break;
                        case ActionType.SoftDelete:
                            TempData["Status"] = ApplicationDbContextMessage.SOFTDELETENOK;
                            break;
                        case ActionType.SoftUnDelete:
                            TempData["Status"] = ApplicationDbContextMessage.SOFTUNDELETENOK;
                            break;
                    }

                }
                
                    
                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(alert);
                }
                else
                {
                    return RedirectToAction("Index");
                }

            }
            catch (Exception ex)
            {
                alert.Type = AlertType.Error;
                alert.ExceptionMessage = ex.Message;

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(alert);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
        }
        
        private async Task<RoleViewModel> ViewModel(ApplicationRole applicationRole = null) 
        {
            var viewModel = new RoleViewModel 
            {
                ApplicationRole = (applicationRole != null)?applicationRole:new ApplicationRole(),
            };

            return viewModel;
        }
    }
}