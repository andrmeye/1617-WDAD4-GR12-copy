using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Accomo.Db;
using Accomo.Models;
using Accomo.Models.Security;
using Accomo.Models.Utilities;
using Accomo.Models.ViewModels;

namespace Accomo.WWW.Areas.Backoffice.Controllers 
{
    [Area("Backoffice")]
    public class DemoController : BaseController 
    {
        
        public DemoController(ApplicationDbContext applicationDbContext, UserManager<ApplicationUser> ApplicationUserManager, RoleManager<ApplicationRole> ApplicationRoleManager):base(applicationDbContext, ApplicationUserManager, ApplicationRoleManager)
        {
            
        }
        public async Task<IActionResult> Index() {
            
            var userId = _userManager.GetUserId(this.User);
            var klote = new Guid(userId);
           //var usermodel = await ApplicationDbContext.ApplicationUsers.Where(o => o.Id == klote).ToListAsync();
            var usermodel = await _userManager.Users.FirstOrDefaultAsync(o => o.Id == klote);
            ViewBag.UserId = usermodel.FirstName;
            return View();
        }
        private async Task<AccomodatieTypeViewModel> ViewModel(AccomodatieType accomodatieType = null) 
        {
            var viewModel = new AccomodatieTypeViewModel 
            {
                AccomodatieType = (accomodatieType != null)?accomodatieType:new AccomodatieType(),
            };

            return viewModel;
        }
        public string GuidToBase64(Guid guid)
        {
            return Convert.ToBase64String(guid.ToByteArray()).Replace("/", "-").Replace("+", "_").Replace("=", "");
        }

        public Guid Base64ToGuid(string base64)
        {
        Guid guid = default(Guid);
        base64 = base64.Replace("-", "/").Replace("_", "+") + "==";

        try {
            guid = new Guid(Convert.FromBase64String(base64));
        }
        catch (Exception ex) {
            throw new Exception("Bad Base64 conversion to GUID", ex);
        }

        return guid;
        }
    }
}