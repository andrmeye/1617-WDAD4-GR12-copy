using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Accomo.Db;
using Accomo.Models;
using Accomo.Models.Security;
using Accomo.Models.Utilities;
using Accomo.Models.ViewModels;

namespace Accomo.WWW.Areas.Backoffice.Controllers 
{
    [Area("Backoffice")]
    public class BoekingController : BaseController 
    {
        public BoekingController(ApplicationDbContext applicationDbContext):base(applicationDbContext)
        {
        }

        public async Task<IActionResult> Index(string sortParam) {
            List<Boeking> boekingen = 
            await ApplicationDbContext.Boekingen
            .Include(o => o.Transacties)
            .Include(o => o.Accomodatie)
            .Include(o => o.User)
            .ToListAsync();

            // Sorting
            ViewBag.PrijsPerDagSortParam = "priceday_asc";
            ViewBag.PrijsPerVerblijfSortParam = "pricestay_asc";
            ViewBag.TotalSortParam = "total_asc";
            ViewBag.CheckInSortParam = "checkin_asc";
            ViewBag.CheckOutSortParam = "checkout_asc";
            ViewBag.GuestSortParam = "guestamount_asc";
            ViewBag.CancelSortParam = "canceldate_asc";
            ViewBag.AccomodatieSortParam = "accomodatie_asc";
            ViewBag.UserSortParam = "username_asc";
            ViewBag.CreatedAtSortParam = "createdat_asc";

            

            if(String.IsNullOrEmpty(sortParam))
            {
                boekingen = boekingen.OrderBy(o => o.CreatedAt).ToList();
            }
            else
            {
                switch(sortParam)
                {
                    case "priceday_asc":
                        ViewBag.PrijsPerDagSortParam = "priceday_desc";
                        boekingen = boekingen.OrderBy(o => o.PrijsPerDag).ToList();
                        break;
                    case "priceday_desc":
                        ViewBag.PrijsPerDagSortParam = "priceday_asc";
                        boekingen = boekingen.OrderByDescending(o => o.PrijsPerDag).ToList();
                        break; 
                    case "pricestay_asc":
                        ViewBag.PrijsPerVerblijfSortParam = "pricestay_desc";
                        boekingen = boekingen.OrderBy(o => o.PrijsPerVerblijf).ToList();
                        break;
                    case "pricestay_desc":
                        ViewBag.PrijsPerVerblijfSortParam = "pricestay_asc";
                        boekingen = boekingen.OrderByDescending(o => o.PrijsPerVerblijf).ToList();
                        break;    
                    case "total_asc":
                        ViewBag.TotalSortParam = "total_desc";
                        boekingen = boekingen.OrderBy(o => o.TotaalBedrag).ToList();
                        break;    
                    case "total_desc":
                        ViewBag.TotalSortParam = "total_asc";
                        boekingen = boekingen.OrderByDescending(o => o.TotaalBedrag).ToList();
                        break;    
                    case "checkin_asc":
                        ViewBag.CheckInSortParam = "checkin_desc";
                        boekingen = boekingen.OrderByDescending(o => o.CheckIn).ToList();
                        break;    
                    case "checkin_desc":
                        ViewBag.CheckInSortParam = "checkin_asc";
                        boekingen = boekingen.OrderBy(o => o.CheckIn).ToList();
                        break;    
                    case "checkout_asc":
                        ViewBag.CheckOutSortParam = "checkout_desc";
                        boekingen = boekingen.OrderByDescending(o => o.CheckOut).ToList();
                        break;    
                    case "checkout_desc":
                        ViewBag.CheckOutSortParam = "checkout_asc";
                        boekingen = boekingen.OrderBy(o => o.CheckOut).ToList();
                        break;    
                    case "guestamount_asc":
                        ViewBag.GuestSortParam = "guestamount_desc";
                        boekingen = boekingen.OrderBy(o => o.AantalGasten).ToList();
                        break;    
                    case "guestamount_desc":
                        ViewBag.GuestSortParam = "guestamount_asc";
                        boekingen = boekingen.OrderByDescending(o => o.AantalGasten).ToList();
                        break;     
                    case "canceldate_asc":
                        ViewBag.CancelSortParam = "canceldate_desc";
                        boekingen = boekingen.OrderBy(o => o.AnnuleringsDatum).ToList();
                        break;    
                    case "canceldate_desc":
                        ViewBag.CancelSortParam = "canceldate_asc";
                        boekingen = boekingen.OrderByDescending(o => o.AnnuleringsDatum).ToList();
                        break;     
                    case "accomodatie_asc":
                        ViewBag.AccomodatieSortParam = "accomodatie_desc";
                        boekingen = boekingen.OrderBy(o => o.Accomodatie.Naam).ToList();
                        break;    
                    case "accomodatie_desc":
                        ViewBag.AccomodatieSortParam = "accomodatie_asc";
                        boekingen = boekingen.OrderByDescending(o => o.Accomodatie.Naam).ToList();
                        break;      
                    case "username_asc":
                        ViewBag.UserSortParam = "username_desc";
                        boekingen = boekingen.OrderBy(o => o.User.FirstName).ThenBy(o => o.User.LastName).ToList();
                        break;    
                    case "username_desc":
                        ViewBag.UserSortParam = "username_asc";
                        boekingen = boekingen.OrderByDescending(o => o.User.FirstName).ThenByDescending(o => o.User.LastName).ToList();
                        break;      
                    case "createdat_asc":
                        ViewBag.CreatedAtSortParam = "createdat_desc";
                        boekingen = boekingen.OrderBy(o => o.CreatedAt).ToList();
                        break;    
                    case "createdat_desc":
                        ViewBag.CreatedAtSortParam = "createdat_asc";
                        boekingen = boekingen.OrderByDescending(o => o.CreatedAt).ToList();
                        break;                    
                }
            }
            if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest") 
            {
                return PartialView("_ListPartial", boekingen);
            }
            
            return View(boekingen);
        }
        
        
        [HttpGet]
        public async Task<IActionResult> Edit(Int16? id)
        {
            if (id == null)
            {
                return new StatusCodeResult(400);
            }
            
            var model = await ApplicationDbContext.Boekingen.FirstOrDefaultAsync(m => m.Id == id);
            
            if(model == null)
            {
                return RedirectToAction("Index");
            }

            var viewModel = await ViewModel(model);
            
            return View(viewModel);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(BoekingViewModel model)
        {
            var alert = new Alert();
            try 
            {
                if(!ModelState.IsValid)
                {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.INVALID;
                    TempData["Status"] = ApplicationDbContextMessage.INVALID;
                    throw new Exception();
                }   
                    
                var originalModel = ApplicationDbContext.Boekingen.FirstOrDefault(m => m.Id == model.Boeking.Id);
                
                if(originalModel == null) 
                {
                    TempData["Status"] = ApplicationDbContextMessage.NOTEXISTS;
                    throw new Exception();
                }

                originalModel.CheckIn = model.Boeking.CheckIn;
                originalModel.CheckOut = model.Boeking.CheckOut;
                originalModel.AantalGasten = model.Boeking.AantalGasten;
                originalModel.TotaalBedrag = model.Boeking.TotaalBedrag;
                
                ApplicationDbContext.Attach(originalModel);
                ApplicationDbContext.Entry(originalModel).State = EntityState.Modified;
                
                if (await ApplicationDbContext.SaveChangesAsync() == 0)
                {
                    TempData["Status"] = ApplicationDbContextMessage.EDITNOK;
                    throw new Exception();
                } 
                
                alert.Message = ApplicationDbContextMessage.EDITOK;
                TempData["Status"] = "Boeking met Id: " + model.Boeking.Id + " is succesvol bewerkt.";

                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                alert.Type = AlertType.Error;
                alert.ExceptionMessage = ex.Message;
                TempData["Status"] = ex.Message;

                model = await ViewModel(model.Boeking);
                ModelState.AddModelError(string.Empty, alert.ExceptionMessage);
            }
            return View(model);
        }
        
        [HttpGet("[area]/[controller]/[action]/{id:int}")]
        public async Task<IActionResult> Delete(Int16 id, [FromQuery] ActionType actionType)
        {
            var model = await ApplicationDbContext.Boekingen.FirstOrDefaultAsync(m => m.Id == id);
            if(model == null)
            {
                return RedirectToAction("Index");
            }
            
            var viewModel = new ActionBoekingViewModel()
            {
                Boeking = model,
                ActionType = actionType
            };
            return View(viewModel);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken] 
        public async Task<IActionResult> Delete(ActionBoekingViewModel model)
        {
            var alert = new Alert();// Alert    
            try
            {
                var originalModel = ApplicationDbContext.Boekingen.FirstOrDefault(m => m.Id == model.Boeking.Id);
                if(originalModel == null)
                {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.NOTEXISTS;
                    TempData["Status"] = ApplicationDbContextMessage.NOTEXISTS;
                    throw new Exception();
                }

                switch(model.ActionType)
                {
                    case ActionType.Delete:
                        alert.Message = ApplicationDbContextMessage.DELETEOK;
                        TempData["Status"] = ApplicationDbContextMessage.DELETEOK;
                        ApplicationDbContext.Entry(originalModel).State = EntityState.Deleted;
                        break;
                    case ActionType.SoftDelete:
                        alert.Message = ApplicationDbContextMessage.SOFTDELETEOK;
                        TempData["Status"] = ApplicationDbContextMessage.SOFTDELETEOK;
                        originalModel.DeletedAt = DateTime.Now;
                        ApplicationDbContext.Entry(originalModel).State = EntityState.Modified;
                        break;
                    case ActionType.SoftUnDelete:
                        alert.Message = ApplicationDbContextMessage.SOFTUNDELETEOK;
                        TempData["Status"] = ApplicationDbContextMessage.SOFTUNDELETEOK;
                        originalModel.DeletedAt = (Nullable<DateTime>)null;
                        ApplicationDbContext.Entry(originalModel).State = EntityState.Modified;
                        break;
                }

                
                if (await ApplicationDbContext.SaveChangesAsync() == 0)
                {                   
                    switch(model.ActionType)
                    {
                        case ActionType.Delete:
                            TempData["Status"] = ApplicationDbContextMessage.DELETENOK;
                            break;
                        case ActionType.SoftDelete:
                            TempData["Status"] = ApplicationDbContextMessage.SOFTDELETENOK;
                            break;
                        case ActionType.SoftUnDelete:
                            TempData["Status"] = ApplicationDbContextMessage.SOFTUNDELETENOK;
                            break;
                    }
                    throw new Exception();
                } 

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(alert);
                }
                else
                {
                    return RedirectToAction("Index");
                }

            }
            catch (Exception ex)
            {
                alert.Type = AlertType.Error;
                alert.ExceptionMessage = ex.Message;

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(alert);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
        }
        
        private async Task<BoekingViewModel> ViewModel(Boeking boeking = null) 
        {
            var viewModel = new BoekingViewModel 
            {
                Boeking = (boeking != null)?boeking:new Boeking(),
            };

            return viewModel;
        }
    }
}