using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Accomo.Db;
using Accomo.Models;
using Accomo.Models.Security;
using Accomo.Models.Utilities;
using Accomo.Models.ViewModels;

namespace Accomo.WWW.Areas.Backoffice.Controllers 
{
    [Area("Backoffice")]
    public class AccomodatieLocatieController : BaseController 
    {
        public AccomodatieLocatieController(ApplicationDbContext applicationDbContext):base(applicationDbContext)
        {
        }

        public async Task<IActionResult> Index(string sortParam) 
        {
            List<AccomodatieLocatie> accomodatielocaties = 
            await ApplicationDbContext.AccomodatieLocaties.ToListAsync();

            // Sorting
            ViewBag.AdressSortParam = "adress_asc";
            ViewBag.LocationSortParam = "location_asc";


            if(String.IsNullOrEmpty(sortParam))
            {
                accomodatielocaties = accomodatielocaties.OrderByDescending(o => o.CreatedAt).ToList();
            }
            else
            {
                switch(sortParam)
                {
                    case "adress_asc":
                        ViewBag.AdressSortParam = "adress_desc";
                        accomodatielocaties = accomodatielocaties.OrderBy(o => o.Straat).ThenBy(o => o.HuisNummer).ToList();
                        break;
                    case "adress_desc":
                        ViewBag.AdressSortParam = "adress_asc";
                        accomodatielocaties = accomodatielocaties.OrderByDescending(o => o.Straat).ThenByDescending(o => o.HuisNummer).ToList();
                        break;
                    case "location_asc":
                        ViewBag.LocationSortParam = "location_desc";
                        accomodatielocaties = accomodatielocaties.OrderBy(o => o.Land).ThenBy(o => o.Stad).ToList();
                        break;
                    case "location_desc":
                        ViewBag.LocationSortParam = "location_asc";
                        accomodatielocaties = accomodatielocaties.OrderByDescending(o => o.Land).ThenByDescending(o => o.Stad).ToList();
                        break;
                    
                }
            }
            if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest") 
            {
                return PartialView("_ListPartial", accomodatielocaties);
            }
            
            return View(accomodatielocaties);
        }

        [HttpGet]
        public async Task<IActionResult> Create() 
        {
            var viewModel = await ViewModel();
            
            return View(viewModel);
        } 

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(AccomodatieLocatieViewModel model)
        {
            var alert = new Alert();
            try
            {
                if(!ModelState.IsValid) {;
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.INVALID;
                    throw new Exception();
                }
                
                ApplicationDbContext.AccomodatieLocaties.Add(model.AccomodatieLocatie);
                if (await ApplicationDbContext.SaveChangesAsync() == 0)
                {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.CREATENOK;
                    throw new Exception();
                }  
                
                TempData["Status"] = "Locatie met ID:" + model.AccomodatieLocatie.Id + " is succesvol aangemaakt";
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                alert.Type = AlertType.Error;
                alert.ExceptionMessage = ex.Message;
                TempData["Status"] = ex.Message;
                
                model = await ViewModel(model.AccomodatieLocatie);
                
                ModelState.AddModelError(string.Empty, alert.ExceptionMessage);
            }
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(Int16? id)
        {
            if (id == null)
            {
                return new StatusCodeResult(400);
            }
            
            var model = await ApplicationDbContext.AccomodatieLocaties.FirstOrDefaultAsync(m => m.Id == id);
            
            if(model == null)
            {
                return RedirectToAction("Index");
            }

            var viewModel = await ViewModel(model);
            
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(AccomodatieLocatieViewModel model)
        {
            var alert = new Alert();
            try 
            {
                if(!ModelState.IsValid)
                {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.INVALID;
                    throw new Exception();
                }   
                    
                var originalModel = ApplicationDbContext.AccomodatieLocaties.FirstOrDefault(m => m.Id == model.AccomodatieLocatie.Id);
                
                if(originalModel == null) 
                {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.NOTEXISTS;
                    throw new Exception();
                }

                originalModel.Straat = model.AccomodatieLocatie.Straat;
                originalModel.HuisNummer = model.AccomodatieLocatie.HuisNummer;
                originalModel.KamerNummer = model.AccomodatieLocatie.KamerNummer;
                originalModel.Stad = model.AccomodatieLocatie.Stad;
                originalModel.PostCode = model.AccomodatieLocatie.PostCode;
                originalModel.Land = model.AccomodatieLocatie.Land;
                originalModel.Streek = model.AccomodatieLocatie.Streek;
                
                ApplicationDbContext.Attach(originalModel);
                ApplicationDbContext.Entry(originalModel).State = EntityState.Modified;
                
                if (await ApplicationDbContext.SaveChangesAsync() == 0)
                {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.EDITNOK;
                    throw new Exception();
                } 
                
                alert.Message = ApplicationDbContextMessage.EDITOK;
                TempData["Status"] = "Locatie met ID: " + model.AccomodatieLocatie.Id + " is succesvol bewerkt";

                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                alert.Type = AlertType.Error;
                alert.ExceptionMessage = ex.Message;
                
                model = await ViewModel(model.AccomodatieLocatie);
                TempData["Status"] = ex.Message;
                ModelState.AddModelError(string.Empty, alert.ExceptionMessage);
            }
            return View(model);
        }

        [HttpGet("[area]/[controller]/[action]/{id:int}")]
        public async Task<IActionResult> Delete(Int16 id, [FromQuery] ActionType actionType)
        {
            var model = await ApplicationDbContext.AccomodatieLocaties.FirstOrDefaultAsync(m => m.Id == id);
            if(model == null)
            {
                return RedirectToAction("Index");
            }
            
            var viewModel = new ActionAccomodatieLocatieViewModel()
            {
                AccomodatieLocatie = model,
                ActionType = actionType
            };
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(ActionAccomodatieLocatieViewModel model)
        {
            var alert = new Alert();// Alert    
            try
            {
                var originalModel = ApplicationDbContext.AccomodatieLocaties.FirstOrDefault(m => m.Id == model.AccomodatieLocatie.Id);
                if(originalModel == null)
                {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.NOTEXISTS;
                    TempData["Status"] = ApplicationDbContextMessage.NOTEXISTS;
                    throw new Exception();
                }

                switch(model.ActionType)
                {
                    case ActionType.Delete:
                        alert.Message = ApplicationDbContextMessage.DELETEOK;
                        TempData["Status"] = ApplicationDbContextMessage.DELETEOK;
                        ApplicationDbContext.Entry(originalModel).State = EntityState.Deleted;
                        break;
                    case ActionType.SoftDelete:
                        alert.Message = ApplicationDbContextMessage.SOFTDELETEOK;
                        TempData["Status"] = ApplicationDbContextMessage.SOFTDELETEOK;
                        originalModel.DeletedAt = DateTime.Now;
                        ApplicationDbContext.Entry(originalModel).State = EntityState.Modified;
                        break;
                    case ActionType.SoftUnDelete:
                        alert.Message = ApplicationDbContextMessage.SOFTUNDELETEOK;
                        TempData["Status"] = ApplicationDbContextMessage.SOFTUNDELETEOK;
                        originalModel.DeletedAt = (Nullable<DateTime>)null;
                        ApplicationDbContext.Entry(originalModel).State = EntityState.Modified;
                        break;
                }

                
                if (await ApplicationDbContext.SaveChangesAsync() == 0)
                {                   
                    switch(model.ActionType)
                    {
                        case ActionType.Delete:
                            TempData["Status"] = ApplicationDbContextMessage.DELETENOK;
                            break;
                        case ActionType.SoftDelete:
                            TempData["Status"] = ApplicationDbContextMessage.SOFTDELETENOK;
                            break;
                        case ActionType.SoftUnDelete:
                            TempData["Status"] = ApplicationDbContextMessage.SOFTUNDELETENOK;
                            break;
                    }
                    throw new Exception();
                } 

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(alert);
                }
                else
                {
                    return RedirectToAction("Index");
                }

            }
            catch (Exception ex)
            {
                alert.Type = AlertType.Error;
                alert.ExceptionMessage = ex.Message;

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(alert);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
        }

        private async Task<AccomodatieLocatieViewModel> ViewModel(AccomodatieLocatie AccomodatieLocatie = null) 
        {

            var viewModel = new AccomodatieLocatieViewModel 
            {
                AccomodatieLocatie = (AccomodatieLocatie != null)?AccomodatieLocatie:new AccomodatieLocatie(),
            };

            return viewModel;
        }
    }
}