using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Accomo.Db;
using Accomo.Models;
using Accomo.Models.Security;
using Accomo.Models.Utilities;
using Accomo.Models.ViewModels;

namespace  Accomo.WWW.Areas.Backoffice.Controllers 
{
    [Area("Backoffice")]
    public class ReviewController : BaseController 
    {
        public ReviewController(ApplicationDbContext applicationDbContext):base(applicationDbContext) 
        {
        }
        public async Task<IActionResult> Index(string sortParam) {
            List<AccomodatieReview> reviews = 
            await ApplicationDbContext.AccomodatieReviews.ToListAsync();

            // Sorting
            ViewBag.TitleSortParam = "title_asc";
            ViewBag.CreatedSortParam = "created_asc";
            ViewBag.ScoreSortParam = "score_asc";

            if(String.IsNullOrEmpty(sortParam))
            {
                reviews = reviews.OrderByDescending(o => o.CreatedAt).ToList();
            }
            else
            {
                switch(sortParam)
                {
                    case "title_asc":
                        ViewBag.TitleSortParam = "title_asc";
                        reviews = reviews.OrderBy(o => o.Titel).ToList();
                        break;
                    case "title_desc":
                       ViewBag.TitleSortParam = "title_desc";
                        reviews = reviews.OrderByDescending(o => o.Titel).ToList();
                        break;
                    case "created_asc":
                        ViewBag.CreatedSortParam = "created_desc";
                        reviews = reviews.OrderBy(o => o.CreatedAt).ToList();
                        break;
                    case "created_desc":
                        ViewBag.CreatedSortParam = "created_asc";
                        reviews = reviews.OrderByDescending(o => o.CreatedAt).ToList();
                        break;
                    case "score_asc":
                        ViewBag.ScoreSortParam = "score_desc";
                        reviews = reviews.OrderBy(o => o.Beoordeling).ToList();
                        break;
                    case "score_desc":
                        ViewBag.ScoreParam = "score_asc";
                        reviews = reviews.OrderByDescending(o => o.Beoordeling).ToList();
                        break;
                    
                }
            }
            if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest") 
            {
                return PartialView("_ListPartial", reviews);
            }
            
            return View(reviews);
        }
        public async Task<IActionResult> IndexbyId(string sortParam, Int16 Id) {
            List<AccomodatieReview> reviews = 
            await ApplicationDbContext.AccomodatieReviews.Where(o => o.AccomodatieId == Id).ToListAsync();

            // Sorting
            ViewBag.TitleSortParam = "title_asc";
            ViewBag.CreatedSortParam = "created_asc";
            ViewBag.ScoreSortParam = "score_asc";

            if(String.IsNullOrEmpty(sortParam))
            {
                reviews = reviews.OrderByDescending(o => o.CreatedAt).ToList();
            }
            else
            {
                switch(sortParam)
                {
                    case "title_asc":
                        ViewBag.TitleSortParam = "title_asc";
                        reviews = reviews.OrderBy(o => o.Titel).ToList();
                        break;
                    case "title_desc":
                       ViewBag.TitleSortParam = "title_desc";
                        reviews = reviews.OrderByDescending(o => o.Titel).ToList();
                        break;
                    case "created_asc":
                        ViewBag.CreatedSortParam = "created_desc";
                        reviews = reviews.OrderBy(o => o.CreatedAt).ToList();
                        break;
                    case "created_desc":
                        ViewBag.CreatedSortParam = "created_asc";
                        reviews = reviews.OrderByDescending(o => o.CreatedAt).ToList();
                        break;
                    case "score_asc":
                        ViewBag.ScoreSortParam = "score_desc";
                        reviews = reviews.OrderBy(o => o.Beoordeling).ToList();
                        break;
                    case "score_desc":
                        ViewBag.ScoreParam = "score_asc";
                        reviews = reviews.OrderByDescending(o => o.Beoordeling).ToList();
                        break;
                    
                }
            }
            if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest") 
            {
                return PartialView("_ListPartial", reviews);
            }
            
            return View(reviews);
        }

        [HttpGet("[area]/[controller]/[action]/{id:int}")]
        public async Task<IActionResult> Delete(Int16 id, [FromQuery] ActionType actionType)
        {
            var model = await ApplicationDbContext.AccomodatieReviews.FirstOrDefaultAsync(m => m.Id == id);            
            if(model == null)
            {
                TempData["Status"] = String.Format("Kan het record met ID {0} niet vinden.", id);
                return RedirectToAction("Index");
            }
            
            var viewModel = new ActionReviewViewModel()
            {
                Review = model,
                ActionType = actionType
            };
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(ActionReviewViewModel model)
        {
            var alert = new Alert();// Alert    
            try
            {
                var originalModel = ApplicationDbContext.AccomodatieReviews.FirstOrDefault(m => m.Id == model.Review.Id);

                if(originalModel == null)
                {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.NOTEXISTS;
                    TempData["Status"] = ApplicationDbContextMessage.NOTEXISTS;
                    throw new Exception();
                }

                switch(model.ActionType)
                {
                    case ActionType.Delete:
                        alert.Message = ApplicationDbContextMessage.DELETEOK;
                        TempData["Status"] = ApplicationDbContextMessage.DELETEOK;
                        ApplicationDbContext.Entry(originalModel).State = EntityState.Deleted;
                        break;
                    case ActionType.SoftDelete:
                        alert.Message = ApplicationDbContextMessage.SOFTDELETEOK;
                        TempData["Status"] = ApplicationDbContextMessage.SOFTDELETEOK;
                        originalModel.DeletedAt = DateTime.Now;
                        ApplicationDbContext.Entry(originalModel).State = EntityState.Modified;
                        break;
                    case ActionType.SoftUnDelete:
                        alert.Message = ApplicationDbContextMessage.SOFTUNDELETEOK;
                        TempData["Status"] = ApplicationDbContextMessage.SOFTUNDELETEOK;
                        originalModel.DeletedAt = (Nullable<DateTime>)null;
                        ApplicationDbContext.Entry(originalModel).State = EntityState.Modified;
                        break;
                }
                if (await ApplicationDbContext.SaveChangesAsync() == 0)
                {                   
                    switch(model.ActionType)
                    {
                        case ActionType.Delete:
                            TempData["Status"] = ApplicationDbContextMessage.DELETENOK;
                            break;
                        case ActionType.SoftDelete:
                            TempData["Status"] = ApplicationDbContextMessage.SOFTDELETENOK;
                            break;
                        case ActionType.SoftUnDelete:
                            TempData["Status"] = ApplicationDbContextMessage.SOFTUNDELETENOK;
                            break;
                    }
                    throw new Exception();
                } 

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(alert);
                }
                else
                {
                    return RedirectToAction("Index");
                }

            }
            catch (Exception ex)
            {
                alert.Type = AlertType.Error;
                alert.ExceptionMessage = ex.Message;
                TempData["Status"] = ex.Message;
                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(alert);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
        }
    }
}