using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Accomo.Db;
using Accomo.Models;
using Accomo.Models.Security;
using Accomo.Models.Utilities;
using Accomo.Models.ViewModels;

namespace Accomo.WWW.Areas.Backoffice.Controllers 
{
    [Area("Backoffice")]
    public class CategorieController : BaseController 
    {
        public CategorieController(ApplicationDbContext applicationDbContext):base(applicationDbContext)
        {
        }

        public async Task<IActionResult> Index(string sortParam) {
            List<Categorie> categories = 
            await ApplicationDbContext.Categorieën.ToListAsync();

            // Sorting
            ViewBag.NameSortParam = "name_asc";


            if(String.IsNullOrEmpty(sortParam))
            {
                categories = categories.OrderByDescending(o => o.Naam).ToList();
            }
            else
            {
                switch(sortParam)
                {
                    case "name_asc":
                        ViewBag.NameSortParam = "name_desc";
                        categories = categories.OrderBy(o => o.Naam).ToList();
                        break;
                    case "name_desc":
                        ViewBag.NameSortParam = "name_asc";
                        categories = categories.OrderByDescending(o => o.Naam).ToList();
                        break;                    
                }
            }
            if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest") 
            {
                return PartialView("_ListPartial", categories);
            }
            
            return View(categories);
        }

        [HttpGet]
        public async Task<IActionResult> Create() 
        {  
            var viewModel = await ViewModel();
            
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CategorieViewModel model)
        {
            var alert = new Alert();
            try
            {
                if(!ModelState.IsValid) {;
                    TempData["Status"] = alert.ExceptionMessage = ApplicationDbContextMessage.INVALID;
                    throw new Exception();
                }
                
                ApplicationDbContext.Categorieën.Add(model.Categorie);
                if (await ApplicationDbContext.SaveChangesAsync() == 0)
                {
                    TempData["Status"] = ApplicationDbContextMessage.CREATENOK;
                    throw new Exception();
                }  
                
                TempData["Status"] = "Categorie met naam:" + model.Categorie.Naam + " is succesvol aangemaakt";
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                alert.Type = AlertType.Error;
                alert.ExceptionMessage = ex.Message;
                TempData["Status"] = ex.Message;
                
                model = await ViewModel(model.Categorie);
                
                ModelState.AddModelError(string.Empty, alert.ExceptionMessage);
            }
            return View(model);
        }
        
        [HttpGet]
        public async Task<IActionResult> Edit(Int16? id)
        {
            if (id == null)
            {
                return new StatusCodeResult(400);
            }
            
            var model = await ApplicationDbContext.Categorieën.FirstOrDefaultAsync(m => m.Id == id);
            
            if(model == null)
            {
                return RedirectToAction("Index");
            }

            var viewModel = await ViewModel(model);
            
            return View(viewModel);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(CategorieViewModel model)
        {
            var alert = new Alert();
            try 
            {
                if(!ModelState.IsValid)
                {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.INVALID;
                    throw new Exception();
                }   
                    
                var originalModel = ApplicationDbContext.Categorieën.FirstOrDefault(m => m.Id == model.Categorie.Id);
                
                if(originalModel == null) 
                {
                    TempData["Status"] = ApplicationDbContextMessage.NOTEXISTS;
                    throw new Exception();
                }

                originalModel.Naam = model.Categorie.Naam;
                originalModel.Beschrijving = model.Categorie.Beschrijving;
                
                ApplicationDbContext.Attach(originalModel);
                ApplicationDbContext.Entry(originalModel).State = EntityState.Modified;
                
                if (await ApplicationDbContext.SaveChangesAsync() == 0)
                {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.EDITNOK;
                    throw new Exception();
                } 
                
                alert.Message = ApplicationDbContextMessage.EDITOK;
                TempData["Status"] = "Categorie met naam: " + model.Categorie.Naam + " is succesvol aangemaakt";

                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                alert.Type = AlertType.Error;
                alert.ExceptionMessage = ex.Message;
                
                model = await ViewModel(model.Categorie);
                TempData["Status"] = ex.Message;
                ModelState.AddModelError(string.Empty, alert.ExceptionMessage);
            }
            return View(model);
        }

        [HttpGet("[area]/[controller]/[action]/{id:int}")]
        public async Task<IActionResult> Delete(Int16 id, [FromQuery] ActionType actionType)
        {
            var model = await ApplicationDbContext.Categorieën.FirstOrDefaultAsync(m => m.Id == id);
            if(model == null)
            {
                return RedirectToAction("Index");
            }
            
            var viewModel = new ActionCategorieViewModel()
            {
                Categorie = model,
                ActionType = actionType
            };
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(ActionCategorieViewModel model)
        {
            var alert = new Alert();// Alert    
            try
            {
                var originalModel = ApplicationDbContext.Categorieën.FirstOrDefault(m => m.Id == model.Categorie.Id);
                if(originalModel == null)
                {
                    TempData["Status"] = ApplicationDbContextMessage.NOTEXISTS;
                    throw new Exception();
                }

                switch(model.ActionType)
                {
                    case ActionType.Delete:
                        TempData["Status"] = ApplicationDbContextMessage.DELETEOK;
                        ApplicationDbContext.Entry(originalModel).State = EntityState.Deleted;
                        break;
                    case ActionType.SoftDelete:
                        TempData["Status"] = ApplicationDbContextMessage.SOFTDELETEOK;
                        originalModel.DeletedAt = DateTime.Now;
                        ApplicationDbContext.Entry(originalModel).State = EntityState.Modified;
                        break;
                    case ActionType.SoftUnDelete:
                        TempData["Status"] = ApplicationDbContextMessage.SOFTUNDELETEOK;
                        originalModel.DeletedAt = (Nullable<DateTime>)null;
                        ApplicationDbContext.Entry(originalModel).State = EntityState.Modified;
                        break;
                }

                
                if (await ApplicationDbContext.SaveChangesAsync() == 0)
                {                   
                    switch(model.ActionType)
                    {
                        case ActionType.Delete:
                            TempData["Status"] = ApplicationDbContextMessage.DELETENOK;
                            break;
                        case ActionType.SoftDelete:
                            TempData["Status"] = ApplicationDbContextMessage.SOFTDELETENOK;
                            break;
                        case ActionType.SoftUnDelete:
                            TempData["Status"] = ApplicationDbContextMessage.SOFTUNDELETENOK;
                            break;
                    }
                    throw new Exception();
                } 

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(alert);
                }
                else
                {
                    return RedirectToAction("Index");
                }

            }
            catch (Exception ex)
            {
                alert.Type = AlertType.Error;
                alert.ExceptionMessage = ex.Message;

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(alert);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
        }
        private async Task<CategorieViewModel> ViewModel(Categorie Categorie = null) 
        {
            var viewModel = new CategorieViewModel 
            {
                Categorie = (Categorie != null)?Categorie:new Categorie(),
            };

            return viewModel;
        }
    }
}