using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Accomo.Db;
using Accomo.Models;
using Accomo.Models.Security;
using Accomo.Models.Utilities;
using Accomo.Models.ViewModels;
using Microsoft.Extensions.Logging;

namespace  Accomo.WWW.Areas.Backoffice.Controllers 
{
    [Area("Backoffice")]
    public class AccomodatieController : BaseController 
    {
        IHostingEnvironment _hostingEnvironment;
        SignInManager<ApplicationUser> _signInManager;
        ILogger _logger;
        public AccomodatieController(ApplicationDbContext applicationDbContext,
        IHostingEnvironment hostingEnvironment,
        UserManager<ApplicationUser> userManager, 
        SignInManager<ApplicationUser> signInManager,
        ILoggerFactory loggerFactory):base(applicationDbContext) 
        {
            _userManager = userManager;
            _hostingEnvironment = hostingEnvironment;
            _signInManager = signInManager;
            _logger = loggerFactory.CreateLogger<AccomodatieController>();
        }

        public async Task<IActionResult> Index(string sortParam, string searchString) {
            List<Accomodatie> accomodaties = null;
            if(!String.IsNullOrEmpty(searchString))
            {
                accomodaties = await ApplicationDbContext.Accomodaties
                .Include(al => al.AccomodatieLocatie)
                .Where(o => o.Naam.Contains(searchString) || o.Beschrijving.Contains(searchString) || o.AccomodatieLocatie.Stad.Contains(searchString))
                .OrderBy(o => o.Naam)
                .ToListAsync();
            }
            else
            {
                accomodaties = await ApplicationDbContext.Accomodaties
                .Include(al => al.AccomodatieLocatie)
                .OrderBy(o => o.Naam).ToListAsync();
            }

            // Sorting
            ViewBag.NameSortParam = "name_asc";
            ViewBag.CreatedSortParam = "created_asc";
            ViewBag.LocationSortParam = "location_asc";

            if(String.IsNullOrEmpty(sortParam))
            {
                accomodaties = accomodaties.OrderByDescending(o => o.CreatedAt).ToList();
            }
            else
            {
                switch(sortParam)
                {
                    case "name_asc":
                        ViewBag.NameSortParam = "name_asc";
                        accomodaties = accomodaties.OrderBy(o => o.Naam).ToList();
                        break;
                    case "name_desc":
                       ViewBag.NameSortParam = "name_desc";
                        accomodaties = accomodaties.OrderByDescending(o => o.Naam).ToList();
                        break;
                    case "created_asc":
                        ViewBag.CreatedSortParam = "created_desc";
                        accomodaties = accomodaties.OrderBy(o => o.CreatedAt).ToList();
                        break;
                    case "created_desc":
                        ViewBag.CreatedSortParam = "created_asc";
                        accomodaties = accomodaties.OrderByDescending(o => o.CreatedAt).ToList();
                        break;
                    case "location_asc":
                        ViewBag.LocationSortParam = "location_desc";
                        accomodaties = accomodaties.OrderBy(o => o.AccomodatieLocatie.Land).ThenBy(o => o.AccomodatieLocatie.Stad).ToList();
                        break;
                    case "location_desc":
                        ViewBag.LocationParam = "location_asc";
                        accomodaties = accomodaties.OrderByDescending(o => o.AccomodatieLocatie.Land).ThenBy(o => o.AccomodatieLocatie.Stad).ToList();
                        break;
                    
                }
            }
            if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest") 
            {
                return PartialView("_ListPartial", accomodaties);
            }
            
            return View(accomodaties);
        }

        [HttpGet]
        public async Task<IActionResult> Create() 
        {  
            var viewModel = await ViewModel();
            
            return View(viewModel);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(AccomodatieViewModel model)
        {
            var alert = new Alert();
            try
            {
                if(!ModelState.IsValid) {;
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.INVALID;
                    TempData["Status"] = ApplicationDbContextMessage.INVALID;
                    throw new Exception();
                }
                
                //// ACCOMODATIEPRIJS

                // Insert  ID from newly created Prijs into foreign key
                ApplicationDbContext.AccomodatiePrijzen.Add(model.Prijs);

                if (await ApplicationDbContext.SaveChangesAsync() == 0)
                {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.CREATENOK;
                    TempData["Status"] = ApplicationDbContextMessage.CREATENOK;
                    throw new Exception();
                }  

                // Get the Id of the last added Prijs
                var lasPriceId = ApplicationDbContext.AccomodatiePrijzen.OrderByDescending(o => o.Id).FirstOrDefault().Id;

                ///// ACCOMODATIELOCATIE

                // Insert  ID from newly created Locatie into foreign key
                ApplicationDbContext.AccomodatieLocaties.Add(model.Locatie);

                if (await ApplicationDbContext.SaveChangesAsync() == 0)
                {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.CREATENOK;
                    TempData["Status"] = ApplicationDbContextMessage.CREATENOK;
                    throw new Exception();
                }  

                var lastlocatieId = ApplicationDbContext.AccomodatieLocaties.OrderByDescending(o => o.Id).FirstOrDefault().Id;

                
                ///// USER
                // Get current user
                var user = await GetCurrentUserAsync();
                if (user != null)
                {
                    model.Accomodatie.UserId = user.Id;
                }else{
                    TempData["Status"] = "User bestaat niet";
                    return RedirectToAction("Index");
                }

                ///// add values to model
                model.Accomodatie.AccomodatiePrijsId = lasPriceId;
                model.Accomodatie.AccomodatieLocatieId = lastlocatieId;

                


                //// Add model accomodatie to database
                ApplicationDbContext.Accomodaties.Add(model.Accomodatie);
                
                if (await ApplicationDbContext.SaveChangesAsync() == 0)
                {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.CREATENOK;
                    TempData["Status"] = ApplicationDbContextMessage.CREATENOK;
                    throw new Exception();
                }  

                ///// File Upload

                 var uploadspath = Path.Combine(_hostingEnvironment.WebRootPath, "uploads");
                 Console.WriteLine("Uploadpad: " + uploadspath);
                 var file = model.Bestand;
                 if (file.Length > 0)
                {
                    using (var fileStream = new FileStream(Path.Combine(uploadspath, file.FileName), FileMode.Create))
                    {
                        await file.CopyToAsync(fileStream);
                    }
                }
                 Console.WriteLine("filenaam: " + file.FileName);
                if (model.Media == null){
                    TempData["Status"] = "model media bestaat niet";
                    return RedirectToAction("Index");
                }
                var lastAccoId = ApplicationDbContext.Accomodaties.OrderByDescending(o => o.Id).FirstOrDefault().Id;

                // insert values into model Media
                model.Media.Naam = file.FileName;
                model.Media.URL = Path.Combine(uploadspath, file.FileName);
                model.Media.AccomodatieId = model.Accomodatie.Id;

                // Insert proper Id!!
                model.Media.MediaTypeId = (short) 1;
                
                ApplicationDbContext.AccomodatieMedia.Add(model.Media);

                if (await ApplicationDbContext.SaveChangesAsync() == 0)
                {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.CREATENOK;
                    TempData["Status"] = ApplicationDbContextMessage.CREATENOK;
                    throw new Exception();
                }  
                
                TempData["Status"] = "Accomodatie met Naam: " + model.Accomodatie.Naam + " is succesvol aangemaakt";
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                alert.Type = AlertType.Error;
                alert.ExceptionMessage = ex.Message;    
                TempData["Status"] = ex.Message;
                
                model = await ViewModel(model.Accomodatie, model.Prijs, model.Locatie, model.Media);
                
                ModelState.AddModelError(string.Empty, alert.ExceptionMessage);
            }
            return View(model);
        }
        
        [HttpGet]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new StatusCodeResult(400);
            }
            
            var modelAccomodatie = await ApplicationDbContext.Accomodaties.FirstOrDefaultAsync(m => m.Id == id);
            var modelPrijs = await ApplicationDbContext.AccomodatiePrijzen.FirstOrDefaultAsync(m => m.Accomodatie.Id == id);
            var modelLocatie = await ApplicationDbContext.AccomodatieLocaties.FirstOrDefaultAsync(m => m.Accomodatie.Id == id);
            var modelMedia = await ApplicationDbContext.AccomodatieMedia.FirstOrDefaultAsync(m => m.AccomodatieId == id);

            if(modelAccomodatie == null)
            {
                TempData["Status"] = "Geen record gevonden.";
                return RedirectToAction("Index");
            }

            var viewModel = await ViewModel(modelAccomodatie, modelPrijs, modelLocatie, modelMedia);
            
            return View(viewModel);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(AccomodatieViewModel model)
        {
            var alert = new Alert();
            try 
            {
                if(!ModelState.IsValid)
                {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.INVALID;
                    throw new Exception();
                }   
                // Edit Prijs
                var originalModelPrijs = ApplicationDbContext.AccomodatiePrijzen.FirstOrDefault(m => m.Accomodatie.Id == model.Accomodatie.Id);
                
                if(originalModelPrijs == null) 
                {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.NOTEXISTS;
                    throw new Exception();
                }
                    
                originalModelPrijs.Prijs = model.Prijs.Prijs;
                originalModelPrijs.ValutaId = model.Prijs.ValutaId;
                
                ApplicationDbContext.AccomodatiePrijzen.Attach(originalModelPrijs);
                ApplicationDbContext.Entry(originalModelPrijs).State = EntityState.Modified;
                
                if (await ApplicationDbContext.SaveChangesAsync() == 0)
                {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.EDITNOK;
                    throw new Exception();
                } 

                // Edit Locatie
                 var originalModelLocatie = ApplicationDbContext.AccomodatieLocaties.FirstOrDefault(m => m.Accomodatie.Id == model.Accomodatie.Id);
                
                if(originalModelLocatie == null) 
                {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.NOTEXISTS;
                    throw new Exception();
                }
                    
                originalModelLocatie.Straat = model.Locatie.Straat;
                originalModelLocatie.HuisNummer = model.Locatie.HuisNummer;
                originalModelLocatie.KamerNummer = model.Locatie.KamerNummer;
                originalModelLocatie.Stad = model.Locatie.Stad;
                originalModelLocatie.PostCode = model.Locatie.PostCode;
                originalModelLocatie.Land = model.Locatie.Land;
                originalModelLocatie.Streek = model.Locatie.Streek;
                
                ApplicationDbContext.AccomodatieLocaties.Attach(originalModelLocatie);
                ApplicationDbContext.Entry(originalModelLocatie).State = EntityState.Modified;
                
                if (await ApplicationDbContext.SaveChangesAsync() == 0)
                {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.EDITNOK;
                    throw new Exception();
                } 

                

                // Edit Accomodatie
                var originalModelAccomodatie = ApplicationDbContext.Accomodaties.FirstOrDefault(m => m.Id == model.Accomodatie.Id);
                
                if(originalModelAccomodatie == null) 
                {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.NOTEXISTS;
                    throw new Exception();
                }
                
                originalModelAccomodatie.Naam = model.Accomodatie.Naam;
                originalModelAccomodatie.Beschrijving = model.Accomodatie.Beschrijving;
                originalModelAccomodatie.MaxAantalGasten = model.Accomodatie.MaxAantalGasten;
                originalModelAccomodatie.BadkamerAantal = model.Accomodatie.BadkamerAantal;
                originalModelAccomodatie.Beschikbaarheid = model.Accomodatie.Beschikbaarheid;
                originalModelAccomodatie.MinLengteVerblijf = model.Accomodatie.MinLengteVerblijf;
                originalModelAccomodatie.AccomodatieTypeId = model.Accomodatie.AccomodatieTypeId;
                originalModelAccomodatie.CategorieId = model.Accomodatie.CategorieId;
                
                ApplicationDbContext.Accomodaties.Attach(originalModelAccomodatie);
                ApplicationDbContext.Entry(originalModelAccomodatie).State = EntityState.Modified;
                
                if (await ApplicationDbContext.SaveChangesAsync() == 0)
                {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.EDITNOK;
                    throw new Exception();
                } 
                alert.Message = ApplicationDbContextMessage.EDITOK;
                alert.Type = AlertType.Success;

                // Edit Media
                 var originalModelMedia = ApplicationDbContext.AccomodatieMedia.FirstOrDefault(m => m.AccomodatieId == model.Accomodatie.Id);
                
                if(originalModelMedia == null) 
                {
                    TempData["Status"] = ApplicationDbContextMessage.NOTEXISTS;
                    throw new Exception();
                }
                if (model.Bestand != null){
                    var uploadspath = Path.Combine(_hostingEnvironment.WebRootPath, "uploads");
                    var file = model.Bestand;
                    if (file.Length > 0)
                    {
                            using (var fileStream = new FileStream(Path.Combine(uploadspath, file.FileName), FileMode.Create))
                            {
                                await file.CopyToAsync(fileStream);
                            }
                    }
                    if (model.Media == null){
                    TempData["Status"] = "model media bestaat niet";
                    return RedirectToAction("Index");
                    }

                    originalModelMedia.Naam = file.FileName;
                    originalModelMedia.URL = Path.Combine(uploadspath, file.FileName);
                    ApplicationDbContext.AccomodatieMedia.Attach(originalModelMedia);
                    ApplicationDbContext.Entry(originalModelMedia).State = EntityState.Modified;
                    if (await ApplicationDbContext.SaveChangesAsync() == 0)
                    {
                        alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.EDITNOK;
                        throw new Exception();
                    } 
                }
                
               
                
                
                TempData["Status"] = "Accomodatie met Naam: " + model.Accomodatie.Naam + " is succesvol bewerkt";
                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                alert.Type = AlertType.Error;
                alert.ExceptionMessage = ex.Message;
                TempData["Status"]  = ex.Message;
                model = await ViewModel(model.Accomodatie, model.Prijs, model.Locatie, model.Media);

                ModelState.AddModelError(string.Empty, alert.ExceptionMessage);
            }
            return View(model);
        }

        [Authorize(Roles = "Administrator,Editor")]
        [HttpGet("[area]/[controller]/[action]/{id:int}")]
        public async Task<IActionResult> Delete(Int16 id, [FromQuery] ActionType actionType)
        {
            var modelAccomodatie = await ApplicationDbContext.Accomodaties.FirstOrDefaultAsync(m => m.Id == id);
            var modelPrijs = await ApplicationDbContext.AccomodatiePrijzen.FirstOrDefaultAsync(m => m.Accomodatie.Id == id);
            var modelLocatie = await ApplicationDbContext.AccomodatieLocaties.FirstOrDefaultAsync(m => m.Accomodatie.Id == id);
            var modelMedia = await ApplicationDbContext.AccomodatieMedia.FirstOrDefaultAsync(m => m.AccomodatieId == id);
            
            if(modelAccomodatie == null)
            {
                TempData["Status"] = String.Format("Kan het record met ID {0} niet vinden.", id);
                return RedirectToAction("Index");
            }
            
            var viewModel = new ActionAccomodatieViewModel()
            {
                Accomodatie = modelAccomodatie,
                Prijs = modelPrijs,
                Locatie = modelLocatie,
                Media = modelMedia,
                ActionType = actionType
            };
            return View(viewModel);
        }

       [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(ActionAccomodatieViewModel model)
        {
            var alert = new Alert();// Alert    
            try
            {
                var originalModelPrijs = ApplicationDbContext.AccomodatiePrijzen.FirstOrDefault(m => m.Accomodatie.Id == model.Accomodatie.Id);
                var originalModelLocatie = ApplicationDbContext.AccomodatieLocaties.FirstOrDefault(m => m.Accomodatie.Id == model.Accomodatie.Id);
                var originalModelMedia = ApplicationDbContext.AccomodatieMedia.FirstOrDefault(m => m.AccomodatieId == model.Accomodatie.Id);
                var originalModelAccomodatie = ApplicationDbContext.Accomodaties.FirstOrDefault(m => m.Id == model.Accomodatie.Id);

                if(originalModelAccomodatie == null)
                {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.NOTEXISTS;
                    TempData["Status"] = ApplicationDbContextMessage.NOTEXISTS;
                    throw new Exception();
                }

                switch(model.ActionType)
                {
                    case ActionType.Delete:
                        alert.Message = ApplicationDbContextMessage.DELETEOK;
                        TempData["Status"] = ApplicationDbContextMessage.DELETEOK;
                        ApplicationDbContext.Entry(originalModelPrijs).State = EntityState.Deleted;
                        ApplicationDbContext.Entry(originalModelMedia).State = EntityState.Deleted;
                        ApplicationDbContext.Entry(originalModelLocatie).State = EntityState.Deleted;
                        ApplicationDbContext.Entry(originalModelAccomodatie).State = EntityState.Deleted;
                        break;
                    case ActionType.SoftDelete:
                        alert.Message = ApplicationDbContextMessage.SOFTDELETEOK;
                        TempData["Status"] = ApplicationDbContextMessage.SOFTDELETEOK;
                        originalModelPrijs.DeletedAt = DateTime.Now;
                        originalModelMedia.DeletedAt = DateTime.Now;
                        originalModelLocatie.DeletedAt = DateTime.Now;
                        originalModelAccomodatie.DeletedAt = DateTime.Now;
                        ApplicationDbContext.Entry(originalModelPrijs).State = EntityState.Modified;
                        ApplicationDbContext.Entry(originalModelMedia).State = EntityState.Modified;
                        ApplicationDbContext.Entry(originalModelLocatie).State = EntityState.Modified;
                        ApplicationDbContext.Entry(originalModelAccomodatie).State = EntityState.Modified;
                        break;
                    case ActionType.SoftUnDelete:
                        alert.Message = ApplicationDbContextMessage.SOFTUNDELETEOK;
                        TempData["Status"] = ApplicationDbContextMessage.SOFTUNDELETEOK;
                        originalModelPrijs.DeletedAt = (Nullable<DateTime>)null;
                        originalModelMedia.DeletedAt = (Nullable<DateTime>)null;
                        originalModelLocatie.DeletedAt = (Nullable<DateTime>)null;
                        originalModelAccomodatie.DeletedAt = (Nullable<DateTime>)null;
                        ApplicationDbContext.Entry(originalModelPrijs).State = EntityState.Modified;
                        ApplicationDbContext.Entry(originalModelMedia).State = EntityState.Modified;
                        ApplicationDbContext.Entry(originalModelLocatie).State = EntityState.Modified;
                        ApplicationDbContext.Entry(originalModelAccomodatie).State = EntityState.Modified;
                        break;
                }

                
                if (await ApplicationDbContext.SaveChangesAsync() == 0)
                {                   
                    switch(model.ActionType)
                    {
                        case ActionType.Delete:
                            TempData["Status"] = ApplicationDbContextMessage.DELETENOK;
                            break;
                        case ActionType.SoftDelete:
                            TempData["Status"] = ApplicationDbContextMessage.SOFTDELETENOK;
                            break;
                        case ActionType.SoftUnDelete:
                            TempData["Status"] = ApplicationDbContextMessage.SOFTUNDELETENOK;
                            break;
                    }
                    throw new Exception();
                } 

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(alert);
                }
                else
                {
                    return RedirectToAction("Index");
                }

            }
            catch (Exception ex)
            {
                alert.Type = AlertType.Error;
                alert.ExceptionMessage = ex.Message;
                TempData["Status"] = ex.Message;
                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(alert);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
        }

        private async Task<AccomodatieViewModel> ViewModel(Accomodatie accomodatie = null, AccomodatiePrijs prijs = null, AccomodatieLocatie locatie = null , AccomodatieMedia media = null ) 
        {
           var categories = await ApplicationDbContext.Categorieën.Select(o => new SelectListItem {
                Text = o.Naam,
                Value = o.Id.ToString()
            }).ToListAsync();
            categories.Add(new SelectListItem() {Text="-- Selecteer een categorie --", Value=""});
            categories = categories.OrderBy(g => g.Text).ToList();
            
            var types = await ApplicationDbContext.AccomodatieTypes.Select(o => new SelectListItem {
                Text = o.Naam,
                Value = o.Id.ToString()
            }).ToListAsync();
            types.Add(new SelectListItem() {Text="-- Selecteer een accomodatietype --", Value=""});
            types = types.OrderBy(g => g.Text).ToList();
            
            var valuta = await ApplicationDbContext.Valutas.Select(o => new SelectListItem {
                Text = o.Naam,
                Value = o.Id.ToString()
            }).ToListAsync();
            valuta.Add(new SelectListItem() {Text="-- Selecteer een valuta --", Value=""});
            valuta = valuta.OrderBy(g => g.Text).ToList();

            var viewModel = new AccomodatieViewModel
            {
                Prijs = (prijs != null)?prijs:new AccomodatiePrijs(),
                Locatie = (locatie != null)?locatie:new AccomodatieLocatie(),
                Media = (media != null)?media:new AccomodatieMedia(),
                Accomodatie = (accomodatie != null)?accomodatie:new Accomodatie(),
                Categorieën = categories,
                AccomodatieTypes = types,
                Valuta = valuta
            };
            return viewModel;
        }
                       
        private Task<ApplicationUser> GetCurrentUserAsync()
        {
            return _userManager.GetUserAsync(HttpContext.User);
        }
 

    }
}