using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Accomo.Db;
using Accomo.Models;
using Accomo.Models.Security;
using Accomo.Models.Utilities;
using Accomo.Models.ViewModels;

namespace Accomo.WWW.Areas.Backoffice.Controllers 
{
    [Area("Backoffice")]
    public class TransactieController : BaseController 
    {
        public TransactieController(ApplicationDbContext applicationDbContext):base(applicationDbContext)
        {
        }

        public async Task<IActionResult> Index(string sortParam) {
            List<Transactie> types = 
            await ApplicationDbContext.Transacties
            .Include(o => o.Accomodatie)
            .Include(o => o.Korting)
            .Include(o => o.Valuta)
            .ToListAsync();

            // Sorting
            ViewBag.NameSortParam = "name_asc";
            ViewBag.TotalAmountSortParam = "total_asc";
            ViewBag.PayedAmountSortParam = "payed_asc";
            ViewBag.TransferSortParam = "transfer_asc";
            ViewBag.DiscountSortParam = "discount_asc";

            

            if(String.IsNullOrEmpty(sortParam))
            {
                types = types.OrderBy(o => o.CreatedAt).ToList();
            }
            else
            {
                switch(sortParam)
                {
                    case "name_asc":
                        ViewBag.NameSortParam = "name_desc";
                        types = types.OrderBy(o => o.Accomodatie.Naam).ToList();
                        break;
                    case "name_desc":
                        ViewBag.NameSortParam = "name_asc";
                        types = types.OrderByDescending(o => o.Accomodatie.Naam).ToList();
                        break; 
                    case "total_asc":
                        ViewBag.TotalAmountSortParam = "total_desc";
                        types = types.OrderBy(o => o.TotaalBedrag).ToList();
                        break;
                    case "total_desc":
                        ViewBag.TotalAmountSortParam = "total_asc";
                        types = types.OrderByDescending(o => o.TotaalBedrag).ToList();
                        break;    
                    case "payed_asc":
                        ViewBag.PayedAmountSortParam = "payed_desc";
                        types = types.OrderBy(o => o.BetaaldBedrag).ToList();
                        break;    
                    case "payed_desc":
                        ViewBag.PayedAmountSortParam = "payed_asc";
                        types = types.OrderByDescending(o => o.BetaaldBedrag).ToList();
                        break;    
                    case "transfer_asc":
                        ViewBag.TransferSortParam = "transfer_desc";
                        types = types.OrderBy(o => o.TransferAt).ToList();
                        break;    
                    case "transfer_desc":
                        ViewBag.TransferSortParam = "transfer_asc";
                        types = types.OrderByDescending(o => o.TransferAt).ToList();
                        break;                    
                }
            }
            if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest") 
            {
                return PartialView("_ListPartial", types);
            }
            
            return View(types);
        }
        
        
        [HttpGet]
        public async Task<IActionResult> Edit(Int16? id)
        {
            if (id == null)
            {
                return new StatusCodeResult(400);
            }
            
            var model = await ApplicationDbContext.Transacties.FirstOrDefaultAsync(m => m.Id == id);
            
            if(model == null)
            {
                return RedirectToAction("Index");
            }

            var viewModel = await ViewModel(model);
            
            return View(viewModel);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(TransactieViewModel model)
        {
            var alert = new Alert();
            try 
            {
                if(!ModelState.IsValid)
                {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.INVALID;
                    TempData["Status"] = ApplicationDbContextMessage.INVALID;
                    throw new Exception();
                }   
                    
                var originalModel = ApplicationDbContext.Transacties.FirstOrDefault(m => m.Id == model.Transactie.Id);
                
                if(originalModel == null) 
                {
                    TempData["Status"] = ApplicationDbContextMessage.NOTEXISTS;
                    throw new Exception();
                }

                originalModel.TransferAt = model.Transactie.TransferAt;
                originalModel.BetaaldBedrag = model.Transactie.BetaaldBedrag;
                originalModel.TotaalBedrag = model.Transactie.TotaalBedrag;
                
                ApplicationDbContext.Attach(originalModel);
                ApplicationDbContext.Entry(originalModel).State = EntityState.Modified;
                
                if (await ApplicationDbContext.SaveChangesAsync() == 0)
                {
                    TempData["Status"] = ApplicationDbContextMessage.EDITNOK;
                    throw new Exception();
                } 
                
                alert.Message = ApplicationDbContextMessage.EDITOK;
                TempData["Status"] = "Transactie met Id: " + model.Transactie.Id + " is succesvol bewerkt.";

                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                alert.Type = AlertType.Error;
                alert.ExceptionMessage = ex.Message;
                TempData["Status"] = ex.Message;

                model = await ViewModel(model.Transactie);
                ModelState.AddModelError(string.Empty, alert.ExceptionMessage);
            }
            return View(model);
        }
        
        [HttpGet("[area]/[controller]/[action]/{id:int}")]
        public async Task<IActionResult> Delete(Int16 id, [FromQuery] ActionType actionType)
        {
            var model = await ApplicationDbContext.Transacties.FirstOrDefaultAsync(m => m.Id == id);
            if(model == null)
            {
                return RedirectToAction("Index");
            }
            
            var viewModel = new ActionTransactieViewModel()
            {
                Transactie = model,
                ActionType = actionType
            };
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken] 
        public async Task<IActionResult> Delete(ActionTransactieViewModel model)
        {
            var alert = new Alert();// Alert    
            try
            {
                var originalModel = ApplicationDbContext.Transacties.FirstOrDefault(m => m.Id == model.Transactie.Id);
                if(originalModel == null)
                {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.NOTEXISTS;
                    TempData["Status"] = ApplicationDbContextMessage.NOTEXISTS;
                    throw new Exception();
                }

                switch(model.ActionType)
                {
                    case ActionType.Delete:
                        alert.Message = ApplicationDbContextMessage.DELETEOK;
                        TempData["Status"] = ApplicationDbContextMessage.DELETEOK;
                        ApplicationDbContext.Entry(originalModel).State = EntityState.Deleted;
                        break;
                    case ActionType.SoftDelete:
                        alert.Message = ApplicationDbContextMessage.SOFTDELETEOK;
                        TempData["Status"] = ApplicationDbContextMessage.SOFTDELETEOK;
                        originalModel.DeletedAt = DateTime.Now;
                        ApplicationDbContext.Entry(originalModel).State = EntityState.Modified;
                        break;
                    case ActionType.SoftUnDelete:
                        alert.Message = ApplicationDbContextMessage.SOFTUNDELETEOK;
                        TempData["Status"] = ApplicationDbContextMessage.SOFTUNDELETEOK;
                        originalModel.DeletedAt = (Nullable<DateTime>)null;
                        ApplicationDbContext.Entry(originalModel).State = EntityState.Modified;
                        break;
                }

                
                if (await ApplicationDbContext.SaveChangesAsync() == 0)
                {                   
                    switch(model.ActionType)
                    {
                        case ActionType.Delete:
                            TempData["Status"] = ApplicationDbContextMessage.DELETENOK;
                            break;
                        case ActionType.SoftDelete:
                            TempData["Status"] = ApplicationDbContextMessage.SOFTDELETENOK;
                            break;
                        case ActionType.SoftUnDelete:
                            TempData["Status"] = ApplicationDbContextMessage.SOFTUNDELETENOK;
                            break;
                    }
                    throw new Exception();
                } 

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(alert);
                }
                else
                {
                    return RedirectToAction("Index");
                }

            }
            catch (Exception ex)
            {
                alert.Type = AlertType.Error;
                alert.ExceptionMessage = ex.Message;

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(alert);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
        }
        
        private async Task<TransactieViewModel> ViewModel(Transactie transactie = null) 
        {
            var viewModel = new TransactieViewModel 
            {
                Transactie = (transactie != null)?transactie:new Transactie(),
            };

            return viewModel;
        }
    }
}