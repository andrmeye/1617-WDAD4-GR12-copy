using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Accomo.Db;
using Accomo.Models;
using Accomo.Models.ViewModels;
using Accomo.Models.Security;

namespace  Accomo.WWW.Areas.Backoffice.ViewComponents 
{
    [ViewComponent(Name="AmountForEntity")]
    public class AmountForEntityViewComponent : BaseViewComponent 
    {
        public AmountForEntityViewComponent(ApplicationDbContext applicationDbContext) : base(applicationDbContext) 
        {
        }

        public async Task<IViewComponentResult> InvokeAsync(string entityType)
        {
            var viewModel = await GetAmountForEntityAsync(entityType);
            return View(viewModel);
        }

        private Task<AmountForEntityViewModel> GetAmountForEntityAsync(string entityType)
        {
            return Task.FromResult(GetAmountForEntity(entityType));
        }

        private AmountForEntityViewModel GetAmountForEntity(string entityType)
        {
            var amount = 0;
            var name = entityType;
            var pluralizeName = "Entities";

            switch(entityType)
            {
                case "Accomodatie":
                    amount = ApplicationDbContext.Accomodaties.AsEnumerable().Count();
                    entityType = "Accomodatie";
                    name = "Accomodatie";
                    pluralizeName = "Accomodaties";
                    break;
                case "AccomodatieDetail":
                    amount = ApplicationDbContext.AccomodatieDetails.AsEnumerable().Count();
                    entityType = "AccomodatieDetail";
                    name = "Detail";
                    pluralizeName = "Details";
                    break;
                case "AccomodatieMedia":
                    amount = ApplicationDbContext.AccomodatieMedia.AsEnumerable().Count();
                    entityType = "AccomodatieMedia";
                    name = "Media";
                    pluralizeName = "Media";
                    break;
                case "AccomodatiePrijs":
                    amount = ApplicationDbContext.AccomodatiePrijzen.AsEnumerable().Count();
                    entityType = "AccomodatiePrijs";
                    name = "Prijs";
                    pluralizeName = "Prijzen";
                    break;
                case "AccomodatieLocatie":
                    amount = ApplicationDbContext.AccomodatieLocaties.AsEnumerable().Count();
                    entityType = "AccomodatieLocatie";
                    name = "Locatie";
                    pluralizeName = "Locaties";
                    break;
                case "AccomodatieReview":
                    amount = ApplicationDbContext.AccomodatieReviews.AsEnumerable().Count();
                    entityType = "AccomodatieReview";
                    name = "AccomodatieReview";
                    pluralizeName = "AccomodatieReviews";
                    break;
                case "AccomodatieScore":
                    amount = ApplicationDbContext.AccomodatieScores.AsEnumerable().Count();
                    entityType = "AccomodatieScore";
                    name = "AccomodatieScore";
                    pluralizeName = "AccomodatieScores";
                    break;
                case "AccomodatieType":
                    amount = ApplicationDbContext.AccomodatieTypes.AsEnumerable().Count();
                    entityType = "AccomodatieType";
                    name = "AccomodatieType";
                    pluralizeName = "AccomodatieTypes";
                    break;
                case "BedType":
                    amount = ApplicationDbContext.BedTypes.AsEnumerable().Count();
                    entityType = "BedType";
                    name = "BedType";
                    pluralizeName = "BedTypes";
                    break;
                case "Boeking":
                    amount = ApplicationDbContext.Boekingen.AsEnumerable().Count();
                    entityType = "Boeking";
                    name = "Boeking";
                    pluralizeName = "Boekingen";
                    break;
                case "Categorie":
                    amount = ApplicationDbContext.Categorieën.AsEnumerable().Count();
                    entityType = "Categorie";
                    name = "Categorie";
                    pluralizeName = "Categorieën";
                    break;
                case "Huisregel":
                    amount = ApplicationDbContext.Huisregels.AsEnumerable().Count();
                    entityType = "Huisregel";
                    name = "Huisregel";
                    pluralizeName = "Huisregels";
                    break;
                case "Transactie":
                    amount = ApplicationDbContext.Transacties.AsEnumerable().Count();
                    entityType = "Transactie";
                    name = "Transactie";
                    pluralizeName = "Transacties";
                    break;
                case "Valuta":
                    amount = ApplicationDbContext.Valutas.AsEnumerable().Count();
                    entityType = "Valuta";
                    name = "Valuta";
                    pluralizeName = "Valutas";
                    break;
                case "Veiligheidsvoorziening":
                    amount = ApplicationDbContext.Veiligheidsvoorzieningen.AsEnumerable().Count();
                    entityType = "Veiligheidsvoorziening";
                    name = "Veiligheidsvoorziening";
                    pluralizeName = "Veiligheidsvoorzieningen";
                    break;
                case "VoorzieningType":
                    amount = ApplicationDbContext.VoorzieningTypes.AsEnumerable().Count();
                    entityType = "VoorzieningType";
                    name = "VoorzieningType";
                    pluralizeName = "VoorzieningTypes";
                    break;
                case "Wishlist":
                    amount = ApplicationDbContext.Wishlists.AsEnumerable().Count();
                    entityType = "Wishlist";
                    name = "Wishlist";
                    pluralizeName = "Wishlists";
                    break;
                case "User":
                    amount = ApplicationDbContext.ApplicationUsers.AsEnumerable().Count();
                    entityType = "User";
                    name = "User";
                    pluralizeName = "Users";
                    break;
            }

            var viewModel = new AmountForEntityViewModel()
            {
                Amount = amount,
                EntityType = entityType,
                Name = name,
                PluralizeName = pluralizeName
            };

            return viewModel;
        }
    }
}