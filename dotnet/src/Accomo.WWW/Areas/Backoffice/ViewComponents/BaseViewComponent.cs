using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Accomo.Db;
using Accomo.Models;
using Accomo.Models.ViewModels;
using Accomo.Models.Security;

namespace  Accomo.WWW.Areas.Backoffice.ViewComponents 
{
    public abstract class BaseViewComponent : ViewComponent 
    {
        public ApplicationDbContext ApplicationDbContext { get; set; }
        public UserManager<ApplicationUser>  ApplicationUserManager  { get; set; }
        public RoleManager<ApplicationRole>  ApplicationRoleManager  { get; set; }
        
        public BaseViewComponent() 
        {
        }

        public BaseViewComponent([FromServices]ApplicationDbContext applicationDbContext) 
        {
            ApplicationDbContext = applicationDbContext;
        }

        public BaseViewComponent([FromServices]ApplicationDbContext applicationDbContext, [FromServices]UserManager<ApplicationUser>  ApplicationUserManager, [FromServices]RoleManager<ApplicationRole>  ApplicationRoleManager) 
        {
            ApplicationDbContext = applicationDbContext;
        }
    }
}