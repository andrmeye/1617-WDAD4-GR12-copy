using System;
using Accomo.Models;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Accomo.Models.ViewModels
{
    public class ActionTransactieViewModel : ActionViewModel
    {
        public Transactie Transactie { get; set;}
    }
}