using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Accomo.Models;

namespace Accomo.Models.ViewModels
{
    public class HuisregelViewModel
    {
        public Huisregel Huisregel { get; set; }
    }
}