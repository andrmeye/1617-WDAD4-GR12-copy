using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Accomo.Models.Security;

namespace Accomo.Models.ViewModels
{
    public class RoleViewModel
    {
        public ApplicationRole ApplicationRole { get; set; }
    }
}