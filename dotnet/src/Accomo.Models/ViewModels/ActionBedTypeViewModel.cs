using System;
using Accomo.Models;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Accomo.Models.ViewModels
{
    public class ActionBedTypeViewModel : ActionViewModel
    {
        public BedType BedType { get; set;}
    }
}