using System;
using Accomo.Models;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Accomo.Models.ViewModels
{
    public class ActionAccomodatieLocatieViewModel : ActionViewModel
    {
        public AccomodatieLocatie AccomodatieLocatie { get; set; }

    }
}