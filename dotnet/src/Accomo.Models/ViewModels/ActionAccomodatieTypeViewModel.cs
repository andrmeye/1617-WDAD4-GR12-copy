using System;
using Accomo.Models;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Accomo.Models.ViewModels
{
    public class ActionAccomodatieTypeViewModel : ActionViewModel
    {
        public AccomodatieType AccomodatieType { get; set; }
    }
}