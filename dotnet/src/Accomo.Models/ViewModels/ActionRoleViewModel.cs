using System;
using Accomo.Models;
using Accomo.Models.Security;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Accomo.Models.ViewModels
{
    public class ActionRoleViewModel : ActionViewModel
    {
        public ApplicationRole ApplicationRole { get; set; }
    }
}