using System;
using Accomo.Models;

namespace Accomo.Models.ViewModels
{
    public class ActionAccomodatieViewModel : ActionViewModel
    {
        public Accomodatie Accomodatie { get; set; }
        public AccomodatiePrijs Prijs { get; set; } 
        public AccomodatieLocatie Locatie { get; set;}
        public AccomodatieMedia Media { get; set;}
    }
}