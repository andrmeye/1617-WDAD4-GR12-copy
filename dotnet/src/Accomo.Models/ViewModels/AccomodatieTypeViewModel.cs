using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Accomo.Models;

namespace Accomo.Models.ViewModels
{
    public class AccomodatieTypeViewModel
    {
        public AccomodatieType AccomodatieType { get; set; }
    }
}