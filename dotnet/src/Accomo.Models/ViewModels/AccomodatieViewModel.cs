using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Http;
using Accomo.Models;

namespace Accomo.Models.ViewModels
{
    public class AccomodatieViewModel
    {
        public Accomodatie Accomodatie { get; set; }
        public AccomodatiePrijs Prijs { get; set; } 
        public AccomodatieLocatie Locatie { get; set;}
        public AccomodatieMedia Media { get; set;}
        public List<SelectListItem> AccomodatieLocaties { get; set; }
        public List<SelectListItem> AccomodatieTypes { get; set; }
        public List<SelectListItem> Valuta { get; set; }
        public List<SelectListItem> Categorieën { get; set; }
        public IFormFile Bestand { get; set;} 
    }
}