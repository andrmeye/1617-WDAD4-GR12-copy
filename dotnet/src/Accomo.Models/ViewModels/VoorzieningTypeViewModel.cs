using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Accomo.Models;

namespace Accomo.Models.ViewModels
{
    public class VoorzieningTypeViewModel
    {
        public VoorzieningType VoorzieningType { get; set; }
    }
}