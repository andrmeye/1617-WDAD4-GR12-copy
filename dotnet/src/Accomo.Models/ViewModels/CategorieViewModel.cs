using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Accomo.Models;

namespace Accomo.Models.ViewModels
{
    public class CategorieViewModel
    {
        public Categorie Categorie { get; set; }
    }
}