using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Accomo.Models;

namespace Accomo.Models.ViewModels
{
    public class MediaTypeViewModel
    {
        public MediaType MediaType { get; set; }
    }
}