using System;
using Accomo.Models;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Accomo.Models.ViewModels
{
    public class ActionReviewViewModel : ActionViewModel
    {
        public AccomodatieReview Review { get; set; }
    }
}