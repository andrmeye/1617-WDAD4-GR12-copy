using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Accomo.Models.Security;

namespace Accomo.Models.ViewModels
{
    public class UserViewModel
    {
        public ApplicationUser ApplicationUser { get; set; }
        public ApplicationRole ApplicationRole { get; set;}
        public List<SelectListItem> Roles { get; set; }

    }
}