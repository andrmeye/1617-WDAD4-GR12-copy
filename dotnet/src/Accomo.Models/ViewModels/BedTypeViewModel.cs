using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Accomo.Models;

namespace Accomo.Models.ViewModels
{
    public class BedTypeViewModel
    {
        public BedType BedType { get; set; }
    }
}