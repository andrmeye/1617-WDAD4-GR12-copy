using System;
using Accomo.Models;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Accomo.Models.ViewModels
{
    public class ActionVeiligheidsvoorzieningViewModel : ActionViewModel
    {
        public Veiligheidsvoorziening Veiligheidsvoorziening { get; set; }
    }
}