using System;
using System.Collections.Generic;

namespace Accomo.Models
{
    public class Korting
    {
        public Int16 Id { get; set; }
        public string Naam { get; set; }
        public string Beschrijving { get; set; }
        public double Percentage { get; set; }
        public DateTime CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public Nullable<DateTime> DeletedAt { get; set; }
        
        // FK's
        ////// Transacties
        public List<Transactie> Transacties { get; set; }
    }
}