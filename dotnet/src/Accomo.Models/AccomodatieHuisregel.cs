using System;
using System.Collections.Generic;

namespace Accomo.Models
{
    public class AccomodatieHuisregel
    {
        public Int16 AccomodatieId { get; set; }
        public Accomodatie Accomodatie { get; set; }
        public Int16 HuisregelId { get; set; }
        public Huisregel Huisregel { get; set; }
    }
}