using System;
using System.Collections.Generic;

namespace Accomo.Models
{
    public class BedTypeAccomodatie
    {
        // FK's many-to-many
       public Int16 BedTypeId { get; set; }
       public BedType BedType { get; set; }
       public Int16 AccomodatieId { get; set; }
       public Accomodatie Accomodatie { get; set; }
       
    }
}
