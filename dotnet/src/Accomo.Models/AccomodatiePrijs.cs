using System;
using System.Collections.Generic;

namespace Accomo.Models
{
    public class AccomodatiePrijs
    {
        // Prijstype staat voor de korting die men krijgt door te reserveren voor een bepaalde periode die een bepaalde korting heeft
        // Hoe langer hoe groter de korting
        public Int16 Id { get; set; }
        public double Prijs { get; set; }
        public string PrijsTypeNaam { get; set; }
        public double PrijsTypePercentage { get; set; }
        public DateTime CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public Nullable<DateTime> DeletedAt { get; set; }

        ////// Accomodatie
        public Accomodatie Accomodatie { get; set; }
        ////// Valuta
        public Int16 ValutaId { get; set;}
        public Valuta Valuta { get; set; }
    }
}
