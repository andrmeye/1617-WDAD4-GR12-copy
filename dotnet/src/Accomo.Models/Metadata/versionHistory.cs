using System;
using System.Collections.Generic;

namespace Accomo.Models.Metadata
{
    public class versionHistory
    {
        public Int16 Id { get; set;}
        public string versie { get; set;}
        public DateTime GepubliceerdOp { get; set; }
        public DateTime CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public Nullable<DateTime> DeletedAt { get; set; }
    }  
}