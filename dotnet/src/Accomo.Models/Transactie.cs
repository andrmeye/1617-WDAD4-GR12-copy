using System;
using System.Collections.Generic;

namespace Accomo.Models
{
    public class Transactie
    {
        public Int16 Id { get; set; }
        // public double Tarieven { get; set; } ??
        public double TotaalBedrag { get; set; }
        public double BetaaldBedrag { get; set; }
        public DateTime TransferAt { get; set; }
        public DateTime CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public Nullable<DateTime> DeletedAt { get; set; }
        
        // FK's
        ////// Accomodatie
        public Int16 AccomodatieId { get; set; }
        public Accomodatie Accomodatie { get; set; }

        ////// Korting
        public Int16 KortingId { get; set; }
        public Korting Korting { get; set; }

        ////// Boeking
        public Int16 BoekingId { get; set; }
        public Boeking Boeking { get; set; }
        ////// Valuta
        public Int16 ValutaId { get; set; }
        public Valuta Valuta { get; set; }
    }
}
