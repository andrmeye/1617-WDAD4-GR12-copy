using System;
using System.Collections.Generic;
using Accomo.Models.Security;

namespace Accomo.Models
{
    public class Boeking
    {
        public Int16 Id { get; set; }
        public double PrijsPerDag { get; set; }
        public double PrijsPerVerblijf { get; set; }
        public double TotaalBedrag { get; set; }
        public DateTime CheckIn { get; set; }
        public DateTime CheckOut { get; set; }
        public int AantalGasten { get; set; }
        public DateTime AnnuleringsDatum { get; set; }
        public DateTime CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public Nullable<DateTime> DeletedAt { get; set; }
        
        // FK's
        ////// Accomodatie
        public Int16 AccomodatieId { get; set; }
        public Accomodatie Accomodatie { get; set; }
        ////// User
        public Guid UserId { get; set;}
        public ApplicationUser User { get; set;}

        ////// Transacties
        public List<Transactie> Transacties { get; set; }
    }
}