using System;
using System.Collections.Generic;

namespace Accomo.Models
{
    public class Valuta 
    {
        public Int16 Id { get; set; }
        public string Naam { get; set; }
        public string Code { get; set; }

        // FK's
        ////// AccomodatiePrijzen
        public List<AccomodatiePrijs> AccomodatiePrijzen { get; set; }
        ////// Transactie
        public List<Transactie> Transacties { get; set; }
    }
}