using System;
using System.Collections.Generic;

namespace Accomo.Models
{
    public class AccomodatieWishlist
    {
    
       public Int16 AccomodatieId { get; set; }
       public Accomodatie Accomodatie { get; set; }
       public Int16 WishlistId { get; set; }
       public Wishlist Wishlist { get; set; }

    }
}
