using System;
using System.Collections.Generic;
using Accomo.Models.Security;


namespace Accomo.Models
{
    public class AccomodatieReview
    {
        
        public Int16 Id { get; set; }
        public string Titel { get; set; }
        public string Inhoud { get; set; }
        public Int16 Beoordeling { get; set;}
        public DateTime CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public Nullable<DateTime> DeletedAt { get; set; }


        // Foreign Keys
        ////// Accomodatie
        public Int16 AccomodatieId { get; set; }
        public Accomodatie Accomodatie { get; set; }

        ////// User
        public Guid UserId { get; set;}
        public ApplicationUser User { get; set;}

        ////// Scores
        public List<AccomodatieScore> AccomodatieScores { get; set; }

    }
}
