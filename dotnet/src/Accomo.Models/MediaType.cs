using System;
using System.Collections.Generic;

namespace Accomo.Models
{
    public class MediaType
    {
        public Int16 Id { get; set; }
        public string Naam { get; set; }
        public string Beschrijving { get; set; }
        public DateTime CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public Nullable<DateTime> DeletedAt { get; set; }

        ////// AccomodatieMedia
        public List<AccomodatieMedia> AccomodatieMedia { get; set; }
        

    }
}
