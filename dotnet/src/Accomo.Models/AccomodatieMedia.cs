using System;
using System.Collections.Generic;

namespace Accomo.Models
{
    public class AccomodatieMedia
    {
        public Int16 Id { get; set; }
        public string Naam { get; set;}
        public string URL { get; set; }
        public DateTime CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public Nullable<DateTime> DeletedAt { get; set; }

        // FK's
        public Int16 MediaTypeId { get; set;}
        public MediaType MediaType { get; set;}

        public Int16 AccomodatieId { get; set; }
        public Accomodatie Accomodatie { get; set; }
    }
}
