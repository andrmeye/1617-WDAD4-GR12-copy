using System;
using System.Collections.Generic;

namespace Accomo.Models
{
    public class AccomodatieLocatie
    {
        public Int16 Id { get; set; }
        public string Straat { get; set; }
        public string HuisNummer { get; set; }
        public string KamerNummer { get; set; }
        public string Stad { get; set;}
        public string PostCode { get; set;}
        public string Streek { get; set;}
        public string Land { get; set;}
        public DateTime CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public Nullable<DateTime> DeletedAt { get; set; }
        
        ////// Accomodatie
        public Accomodatie Accomodatie { get; set; }
    }
}