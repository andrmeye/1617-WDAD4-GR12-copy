using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Accomo.Models.Security
{
    public class ApplicationRole : IdentityRole<Guid> 
    {
        public string Beschrijving { get; set; }
        public DateTime CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public Nullable<DateTime> DeletedAt { get; set; }
    }
}