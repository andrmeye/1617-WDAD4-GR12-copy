using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using OpenIddict;
using Accomo.Models;

namespace Accomo.Models.Security
{
    public enum GenderType : byte {
        Unknown = 0,
        Male = 1,
        Female = 2,
        NotApplicable = 9
        }
    public class ApplicationUser : IdentityUser<Guid> 
    {
        
        public string FirstName { get; set;}
        public string LastName { get; set;}
        public GenderType Gender { get; set;}
        public string plainPassword { get; set;}
        public Nullable<DateTime> DayOfBirth { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public Nullable<DateTime> DeletedAt { get; set; }

        //// Accomodatie
        public List<Accomodatie> Accomodaties { get; set;}
        //// Review
        public List<AccomodatieReview> AccomodatieReviews { get; set;} 
        //// Wishlist
        public List<Wishlist> Wishlists { get; set;}
        //// Boeking
        public List<Boeking> Boekingen { get; set;}
        
        [NotMapped]
        public List<string> RoleNames {get; set;}
    }
}