﻿using System;
using System.Collections.Generic;
using Accomo.Models.Security;

namespace Accomo.Models
{
    public class Accomodatie
    {
        public Int16 Id { get; set; }
        public string Naam { get; set; }
        public string Beschrijving { get; set; }
        public Int16 MaxAantalGasten { get; set; }
        public Int16 BadkamerAantal { get; set; }
        public bool Beschikbaarheid { get; set; }
        public string MinLengteVerblijf { get; set; }
        public DateTime CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public Nullable<DateTime> DeletedAt { get; set; }

        // FK's 

        // Applicationuser
        public Guid UserId { get; set;}
        public ApplicationUser User { get; set;}

        // Locatie
        public Int16 AccomodatieLocatieId { get; set; }  
        public AccomodatieLocatie AccomodatieLocatie { get; set; }  

        // Prijs
        public Int16 AccomodatiePrijsId { get; set;}
        public AccomodatiePrijs AccomodatiePrijs { get; set;}

        ////// BedTypes
        public List<BedTypeAccomodatie> BedTypes { get; set; }
        
        // AccomodatieType
        public Int16 AccomodatieTypeId { get; set;}
        public AccomodatieType AccomodatieType { get; set;}

        // Categorie
        public Int16 CategorieId { get; set; }
        public Categorie Categorie { get; set; }

        // Veiligheidsvoorziening
        public List<AccomodatieVeiligheidsvoorziening> Veiligheidsvoorzieningen { get; set; }

        ////// Review
        public List<AccomodatieReview> AccomodatieReviews { get; set; }

        ////// AccomodatieMedia
        public List<AccomodatieMedia> AccomodatieMedia { get; set; }

        ////// AccomodatieDetails
        public List<AccomodatieDetails> AccomodatieDetails { get; set; }

        ////// Voorzieningen
        public List<AccomodatieVoorzieningType> Voorzieningen { get; set; }
        
        ////// Wishlists
        public List<AccomodatieWishlist> Wishlists { get; set; }

        ////// Huisregels
        public List<AccomodatieHuisregel> Huisregels { get; set; }

        ////// Boekingen
        public List<Boeking> Boekingen { get; set; }

        ////// Transacties
        public List<Transactie> Transacties { get; set; }
    }
}
