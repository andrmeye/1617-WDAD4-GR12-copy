using System;
using System.Collections.Generic;

namespace Accomo.Models
{
    public class AccomodatieScore
    {
        public Int16 Id { get; set; }
        public string Naam { get; set; }
        public Int16 Score { get; set; }
        public DateTime CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public Nullable<DateTime> DeletedAt { get; set; }

        ////// AccomodatieReview
        public Int16 AccomodatieReviewId { get; set; }
        public AccomodatieReview AccomodatieReview { get; set; }
        
    }
}
