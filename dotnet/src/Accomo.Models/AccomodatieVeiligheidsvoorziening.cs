using System;
using System.Collections.Generic;

namespace Accomo.Models
{
    public class AccomodatieVeiligheidsvoorziening
    {
        public Int16 AccomodatieId { get; set; }
        public Accomodatie Accomodatie { get; set; }
        public Int16 VeiligheidsvoorzieningId { get; set; }
        public Veiligheidsvoorziening Veiligheidsvoorziening { get; set; }
    }
}