using System;
using System.Collections.Generic;

namespace Accomo.Models
{
    public class VoorzieningType
    {
        public Int16 Id { get; set; }
        public string Naam { get; set; }
        public string Beschrijving { get; set; }
        public DateTime CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public Nullable<DateTime> DeletedAt { get; set; }
        
        ////// Accomodaties
        public List<AccomodatieVoorzieningType> Accomodaties { get; set; }

    }
}
