using System;
using System.Collections.Generic;
using Accomo.Models.Security;

namespace Accomo.Models
{
    public class Wishlist
    {
    
        public Int16 Id { get; set; }
        public string Naam { get; set;}
        public DateTime CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public Nullable<DateTime> DeletedAt { get; set; }

        // FK
        //// User
        public Guid UserId { get; set;}
        public ApplicationUser User { get; set;}

        public List<AccomodatieWishlist> Accomodaties { get; set; }
        
        
    }
}
