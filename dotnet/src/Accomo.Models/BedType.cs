using System;
using System.Collections.Generic;

namespace Accomo.Models
{
    public class BedType
    {
        public Int16 Id { get; set; }
        public string Naam { get; set; }
        public string Beschrijving { get; set; }
        public DateTime CreatedAt { get; set; }
        public Nullable<DateTime> UpdatedAt { get; set; }
        public Nullable<DateTime> DeletedAt { get; set; }
        
        ////// BedTypeAccomodatie
        public List<BedTypeAccomodatie> Accomodaties { get; set; }

    }
}
