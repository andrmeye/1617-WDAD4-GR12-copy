using System;
using System.Collections.Generic;

namespace Accomo.Models
{
    public class AccomodatieVoorzieningType
    {
    // many-to-many
       public Int16 AccomodatieId { get; set; }
       public Accomodatie Accomodatie { get; set; }
       public Int16 VoorzieningTypeId { get; set; }
       public VoorzieningType VoorzieningType { get; set; }

    }
}
