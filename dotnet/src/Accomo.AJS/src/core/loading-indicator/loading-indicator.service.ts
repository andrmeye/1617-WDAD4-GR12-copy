import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject }    from 'rxjs/Subject';
import { LoadingIndicatorState } from './loading-indicator.model';

@Injectable()
export class LoadingIndicatorService {
  
  private liStateSubject = new Subject<LoadingIndicatorState>();
  liState = this.liStateSubject.asObservable();

  constructor() { }
  show() {
    console.log("LIS: Show loading indicator...");
    this.liStateSubject.next(<LoadingIndicatorState>{ isActive: true });
  }
  hide() {
    console.log("LIS: Hide loading indicator...");
    this.liStateSubject.next(<LoadingIndicatorState>{ isActive: false });
  }
  state(): Observable<LoadingIndicatorState> {
    return this.liState;
  }

}