export class LoadingIndicatorState {
  title: string;
  message: string;
  isActive: boolean;
}