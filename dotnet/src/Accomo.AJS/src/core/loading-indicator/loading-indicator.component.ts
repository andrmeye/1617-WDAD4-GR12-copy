import { Component, OnInit, Input, Output, ViewChild, EventEmitter } from '@angular/core';
import { LoadingIndicatorService } from "../loading-indicator/loading-indicator.service";
import { LoadingIndicatorState } from "../loading-indicator/loading-indicator.model";

@Component({
  selector: 'app-loading-indicator',
  templateUrl: './loading-indicator.component.html',
  styleUrls: ['./loading-indicator.component.css']
})
export class LoadingIndicatorComponent implements OnInit {
  
  constructor(private loadingIdicatorService: LoadingIndicatorService) {}
  ngOnInit() {
     this.loadingIdicatorService.state().subscribe((loadingIndicatorState: LoadingIndicatorState) => {
         console.log(`LIC: show...${loadingIndicatorState.isActive}`);
     });
  }

}
