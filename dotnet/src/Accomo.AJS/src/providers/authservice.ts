import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Events } from 'ionic-angular';

@Injectable()
export class AuthService {
  public currentUser: any;
  public data: any;
  public allowed: boolean;
 
  constructor(public http: Http, public events: Events) {

  }

  public login(credentials) {
    if (credentials.UserName === null || credentials.password === null) {
      console.log("no input given");
    } else {
      return new Promise(resolve => {
      
      this.http.get('http://localhost:5000/api/users/byUserName/' + credentials.UserName)
        .map(res => res.json())
        .subscribe(data => {

          if (data[0].userName != null) {
            if (data[0].plainPassword == credentials.password) {
              this.currentUser = data[0].id;
              localStorage.setItem("userId", this.currentUser);
              localStorage.setItem("userName", data[0].userName);
              this.events.publish('user:created', this.currentUser, Date.now());
              this.allowed = true;
            } else {
              console.log("password incorrect");
              this.allowed = false;
            }
          } else {
              console.log("no user with this username");
              this.allowed = false;
          }
          
          this.data = data;
          resolve(this.data);
        });
        
      });
    }
  }

  public register(credentials) {
    if (credentials.UserName === null || credentials.plainPassword === null) {
      console.log("no input given");
    } else {
      return new Promise(resolve => {

        let headers = new Headers({"Content-Type":"application/json"});
        let options = new RequestOptions({ headers: headers});

        this.http.post('http://localhost:5000/api/users/', JSON.stringify(credentials), options)
          .map(res => res.json())
          .subscribe(data => {
            console.log("data recieved");
          });
        
      });
    }
  }
 
  public getUserInfo() {
    return this.currentUser;
  }
 
  public logout() {
    this.currentUser = null;
    this.events.publish('user:created', this.currentUser, Date.now());
    localStorage.removeItem("userId");
    localStorage.removeItem("userName");
  }

}