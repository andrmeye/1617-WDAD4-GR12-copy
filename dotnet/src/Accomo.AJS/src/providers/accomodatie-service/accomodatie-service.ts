import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Events } from 'ionic-angular';

import { Accomodatie } from '../../models/accomodatie.model';

/*
  Generated class for the AccomodatieServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class AccomodatieServiceProvider {

  public currentAccomodatie: any;
  public allowed: boolean;
  public accomodaties: any;
  public accomodatie: any;

  constructor(public http: Http, public events: Events) {
    console.log('Hello AccomodatieServiceProvider Provider');
  }

  private accomodatiesUrl: string = 'http://localhost:5000/api/';

  load() {
    if (this.accomodaties) {
      return new Promise(resolve => {
      this.http.get(this.accomodatiesUrl + 'accomodaties')
        .map(res => res.json())
        .subscribe(data => {
          console.log(data);
          this.accomodaties = data;
          resolve(this.accomodaties);
        });
    });
    } else {
      return new Promise(resolve => {
        this.http.get(this.accomodatiesUrl + 'accomodaties')
          .map(res => res.json())
          .subscribe(data => {
            console.log(data);
            this.accomodaties = data;
            resolve(this.accomodaties);
          });
      });
    }
  }

  loadById(accomoId) {
    if (this.accomodatie) {
      //return new Promise.resolve(this.accomodatie);
      return new Promise(resolve => {
      this.http.get(this.accomodatiesUrl + 'accomodaties/' + accomoId)
        .map(res => res.json())
        .subscribe(data => {
          console.log(data);
          this.accomodatie = data;
          resolve(this.accomodatie);
        });
    })
    } else {
    return new Promise(resolve => {
      this.http.get(this.accomodatiesUrl + 'accomodaties/' + accomoId)
        .map(res => res.json())
        .subscribe(data => {
          console.log(data);
          this.accomodatie = data;
          resolve(this.accomodatie);
        });
    })
    }
    
  }

  loadByStad(stad) {
    if (this.accomodaties) {
      return new Promise(resolve => {
      this.http.get(this.accomodatiesUrl + 'accomodaties/byStad/' + stad)
        .map(res => res.json())
        .subscribe(data => {
          console.log(data);
          this.accomodaties = data;
          resolve(this.accomodaties);
        });
    });
    }

    return new Promise(resolve => {
      this.http.get(this.accomodatiesUrl + 'accomodaties/byStad' + stad)
        .map(res => res.json())
        .subscribe(data => {
          console.log(data);
          this.accomodaties = data;
          resolve(this.accomodaties);
        });
    });
    
  }

  /*getAccomodaties(): Observable<Accomodatie[]> {
    let url: string = `${this.accomodatiesUrl}accomodaties`;
    return this.http
      .get(url)
      .map((res: Response) => <Accomodatie[]>res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  getAccomodatie(id: number) {
    let url: string = `${this.accomodatiesUrl}accomodaties/${this.accomodatieId}`;
    return this.http
      .get(url)
      .map((res: Response) => <Accomodatie>res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  postAccomodatie(accomodatie: Accomodatie) {
    let url: string = `${this.accomodatiesUrl}accomodaties/${this.accomodatieId}`,
        body: string = JSON.stringify({ accomodatie: accomodatie }),
        options: RequestOptions = new RequestOptions({ headers: new Headers({ 'Content-Type': 'application/json' }) });
    return this.http
      .post(url, body, options)
      .catch((error: any) => Observable.throw(error || 'Server error'))
    ;
  }

  putAccomodatie(accomodatie: Accomodatie) {
    let url: string = `${this.accomodatiesUrl}accomodaties/${this.accomodatieId}`,
        body: string = JSON.stringify({ accomodatie: accomodatie }),
        options: RequestOptions = new RequestOptions({ headers: new Headers({ 
          'Content-Type': 'application/json' 
        }) });
    return this.http
      .put(url, body, options)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  deleteAccomodatie(accomodatie: Accomodatie) {
    console.log('deleteAccomodatie');
    let url: string = `${this.accomodatiesUrl}accomodaties/${this.accomodatieId}/?softDelete=delete`;
    return this.http
      .delete(url)
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  /*private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }*/




}


