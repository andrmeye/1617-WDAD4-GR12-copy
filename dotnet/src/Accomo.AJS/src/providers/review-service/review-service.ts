import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Events } from 'ionic-angular';
import { Observable } from "rxjs";

/*
  Generated class for the ReviewServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class ReviewServiceProvider {

  public data: any;

  constructor(public http: Http, public events: Events) {
    console.log('Hello ReviewServiceProvider Provider');
  }

  private accomodatiesUrl: string = 'http://localhost:5000/api/';

  public createReview(review) {
    let headers = new Headers({"Content-Type":"application/json"});
    let options = new RequestOptions({ headers: headers});
    this.http.post(this.accomodatiesUrl + 'reviews/', JSON.stringify(review), options)
      .map(res => res.json())
      .subscribe(data => {
        console.log("data recieved");
      });
  }

  public updateReview(id, review) {
    if (id == null) {
      console.log("no input given");
    } else {
      return new Promise(resolve => {
        let headers = new Headers({"Content-Type":"application/json"});
        let options = new RequestOptions({ headers: headers});
        console.log(review);
        this.http.put('http://localhost:5000/api/reviews/' + id, JSON.stringify(review), options)
          .map(res => res.json())
          .subscribe(data => {
            console.log("data recieved");
          });
      });
    }
  }

  public getReviewsById(id) {
    if (id == null) {
      console.log("no input given");
    } else {
      return new Promise(resolve => {
      this.http.get('http://localhost:5000/api/reviews/' + id)
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
        
      });
    }
  }

  public getReviews(accomoId) {
    if (accomoId == null) {
      console.log("no input given");
    } else {
      return new Promise(resolve => {
      this.http.get('http://localhost:5000/api/reviews/byAccomoId/' + accomoId)
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
        
      });
    }
  }

  public delete(id) {
    if (id == null) {
      console.log("no input given");
    } else {
      return new Promise(resolve => {
      this.http.delete('http://localhost:5000/api/reviews/' + id)
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
        
      });
    }
  }
}
