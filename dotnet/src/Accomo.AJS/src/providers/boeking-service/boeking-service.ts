import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Events } from 'ionic-angular';
import { Observable } from "rxjs";

/*
  Generated class for the BoekingServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class BoekingServiceProvider {

  public data: any;

  constructor(public http: Http) {
    console.log('Hello BoekingServiceProvider Provider');
  }

  private accomodatiesUrl: string = 'http://localhost:5000/api/';
  
    public createBoeking(boeking) {
      let headers = new Headers({"Content-Type":"application/json"});
      let options = new RequestOptions({ headers: headers});
      this.http.post(this.accomodatiesUrl + 'boeking/', JSON.stringify(boeking), options)
        .map(res => res.json())
        .subscribe(data => {
          console.log("data recieved");
        });
    }

    public getBoekingById(id) {
      if (id == null) {
        console.log("no input given");
      } else {
        return new Promise(resolve => {
        this.http.get('http://localhost:5000/api/boeking/' + id)
          .map(res => res.json())
          .subscribe(data => {
            this.data = data;
            resolve(this.data);
          });
          
        });
      }
    }
}
