import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Events } from 'ionic-angular';
import { Observable } from "rxjs";


/*
  Generated class for the UserService provider.
  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class UserService {
  public data: any;

  constructor(public http: Http, public events: Events) {
    
  }

  public load(userName) {
    if (userName == null) {
      console.log("no input given");
    } else {
      return new Promise(resolve => {
      this.http.get('http://localhost:5000/api/users/byUserName/' + userName)
        .map(res => res.json())
        .subscribe((data: any[]) => {
          this.data = data;
          resolve(this.data);
        });
        
      });
    }
  }

}