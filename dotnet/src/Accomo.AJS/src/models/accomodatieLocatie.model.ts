export class AccomodatieLocatie {
    id?: number;
    straat?: string;
    huisNummer?: string;
    kamerNummer?: string;
    postCode?: string;
    stad?: string;
    streek?: string;
    land?: string;
}
