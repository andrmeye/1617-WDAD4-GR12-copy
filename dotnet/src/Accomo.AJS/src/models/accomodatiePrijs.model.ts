import { Accomodatie } from "./accomodatie.model";

export class AccomodatiePrijs extends Accomodatie {
    id: number;
    prijs: string;
    prijsTypeNaam: number;
    prijsTypePercentage: number;
    stad: string;
    valuta: string;
}