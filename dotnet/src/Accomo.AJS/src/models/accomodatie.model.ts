export class Accomodatie {
    id: number;
    naam: string;
    beschrijving: string;
    discriminator: string;
    accomodatieLocatieId: number;
    accomodatiePrijsId: number;
    //badkamerAantal: number;
    //beschikbaarheid: boolean;
    //minLengteVerblijf: number;
    //prijs: number;
}