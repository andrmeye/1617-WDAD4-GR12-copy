export class AccomodatieReviews {
    id: number;
    titel: string;
    inhoud: string;
    userId: string;
}