import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { BoekingServiceProvider } from '../../providers/boeking-service/boeking-service';
import { AccomodatiePage } from '../accomodatie/accomodatie';
import { AccomodatieDetailPage } from '../accomodatie/accomodatieDetail/accomodatie.detail';
import { HomePage } from '../home/home';
import { AccomodatieServiceProvider } from '../../providers/accomodatie-service/accomodatie-service';
import { AlertController } from 'ionic-angular';

/**
 * Generated class for the BoekingPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-boeking',
  templateUrl: 'boeking.html',
})
export class BoekingPage {

  private accomodatiePrijs: any;
  private accomoId : string;
  private userId : any;
  boeking = { aantalGasten: "", 
              prijsPerDag: this.navParams.get("accomodatiePrijs"), 
              totaalBedrag: "", 
              checkIn: "", 
              checkOut: "", 
              accomodatieId: this.navParams.get("accomoId"), 
              userId: this.navParams.get("userId")};  
  
  constructor(public navCtrl: NavController, public alertCtrl: AlertController, public navParams: NavParams, public BoekingService: BoekingServiceProvider, public accomodatieService: AccomodatieServiceProvider) {
    this.accomoId = this.navParams.get("accomoId");
    this.accomodatiePrijs = this.navParams.get("accomodatiePrijs");
    this.userId = localStorage.getItem("userId");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BoekingPage');
  }

  confirm() {
    let confirm = this.alertCtrl.create({
      title: 'Verzoek verzenden?',
      message: 'De aanvraag om deze Accomo te reserveren wordt verzonden naar de eigenaar, u ontvangt een email ter bevestiging.',
      buttons: [
        {
          text: 'Annuleer',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Bevestig',
          handler: () => {
            this.createBoeking();
          }
        }
      ]
    });
    confirm.present();
  }

  public createBoeking() {
    console.log(this.boeking);
    this.BoekingService.createBoeking(this.boeking);
    this.navCtrl.setRoot( AccomodatiePage );
  }

}
