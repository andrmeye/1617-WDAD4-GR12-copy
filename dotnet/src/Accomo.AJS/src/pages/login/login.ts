import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { AuthService } from '../../providers/authservice';
import { HomePage } from '../home/home';
import { Registreren } from '../registreren/registreren';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers: [AuthService]
})
export class Login {

  loginCredentials = {UserName: '', password: ''};

  constructor(public navCtrl: NavController, public navParams: NavParams, private Authenticate: AuthService) {}

  public login() {
    this.Authenticate.login(this.loginCredentials).then(data => {
      if (this.Authenticate.allowed == true) {
        if (this.navParams.get("reservationStep") != null) {
          this.navCtrl.setRoot( Registreren )
          console.log("goes to auth");
        } else {
          this.navCtrl.setRoot(HomePage)
        }
      } else {
        console.log("Access Denied");
      }
    });
  }

  pushRegister() {
    if (this.navParams.get("reservationStep") != null) {
          this.navCtrl.setRoot( Registreren )
        } else {
          this.navCtrl.setRoot( Registreren );
        }
  }
}
