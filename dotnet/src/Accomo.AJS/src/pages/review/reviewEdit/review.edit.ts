import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';

import { ReviewServiceProvider } from '../../../providers/review-service/review-service';
import { AccomodatiePage } from '../../accomodatie/accomodatie';
import { AccomodatieDetailPage } from '../../accomodatie/accomodatieDetail/accomodatie.detail';
import { HomePage } from '../../home/home';

/**
 * Generated class for the ReviewPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-review.edit',
  templateUrl: 'review.edit.html',
})
export class ReviewEditPage {

  private review: any;
  private loadingIndicator: any;
  private id: any;
  private usersId: string;
  private reviewId: any;
  private reviewUpdate: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private ReviewService: ReviewServiceProvider, private loadingCtrl: LoadingController) {
    this.createLoadingIndicator();
    this.id = this.navParams.get("id");
    this.getReviewsById(this.id);
    this.usersId = localStorage.getItem("userId");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReviewPage');
  }

  getReviewsById(id) {
    this.ReviewService.getReviewsById(id)
      .then(data => {
        this.review = data;
        this.reviewId = data[0].userId;
        this.loadingIndicator.dismiss();
      })
  }

  updateReview() {
    this.reviewUpdate = {id: this.id, titel: this.review[0].titel, inhoud: this.review[0].inhoud, beoordeling: this.review[0].beoordeling};  

    this.ReviewService.updateReview(this.id, this.reviewUpdate);
    this.navCtrl.setRoot( AccomodatiePage )
  }

  private createLoadingIndicator() {
    this.loadingIndicator = this.loadingCtrl.create({
      content: 'Loading accomodaties...'
    });
    this.loadingIndicator.present();
  }
  
}
