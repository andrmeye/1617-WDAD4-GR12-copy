import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ReviewServiceProvider } from '../../providers/review-service/review-service';
import { AccomodatiePage } from '../accomodatie/accomodatie';
import { AccomodatieDetailPage } from '../accomodatie/accomodatieDetail/accomodatie.detail';
import { HomePage } from '../home/home';
import { AlertController } from 'ionic-angular';

/**
 * Generated class for the ReviewPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-review',
  templateUrl: 'review.html',
})
export class ReviewPage {
  review = {titel: "", inhoud: "", beoordeling: "", accomodatieId: this.navParams.get("accomoId"), userId: this.navParams.get("userId")};  

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, public navParams: NavParams, private ReviewService: ReviewServiceProvider) {
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReviewPage');
  }

  confirm() {
    let confirm = this.alertCtrl.create({
      title: 'Recensie plaatsen?',
      message: 'Deze recensie komt terecht op de pagina van de accomo.',
      buttons: [
        {
          text: 'Annuleer',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Bevestig',
          handler: () => {
            this.createReview();
          }
        }
      ]
    });
    confirm.present();
  }

  public createReview() {
    this.ReviewService.createReview(this.review)
    this.navCtrl.setRoot( AccomodatiePage )
  }
}
