import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';

import { ReviewServiceProvider } from '../../../providers/review-service/review-service';
import { AccomodatiePage } from '../../accomodatie/accomodatie';
import { AccomodatieDetailPage } from '../../accomodatie/accomodatieDetail/accomodatie.detail';
import { HomePage } from '../../home/home';
import { ReviewEditPage } from '../reviewEdit/review.edit';
import { AlertController } from 'ionic-angular';

/**
 * Generated class for the ReviewPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-review.detail',
  templateUrl: 'review.detail.html',
})
export class ReviewDetailPage {

  private review: any;
  private loadingIndicator: any;
  private id: any;
  private usersId: string;
  private reviewId: any;
  reviewUpdate = {titel: "", inhoud: "", beoordeling: "", accomodatieId: this.navParams.get("accomoId"), userId: this.navParams.get("userId")};  


  constructor(public navCtrl: NavController, public alertCtrl: AlertController, public navParams: NavParams, private ReviewService: ReviewServiceProvider, private loadingCtrl: LoadingController) {
    this.createLoadingIndicator();
    this.id = this.navParams.get("id");
    this.getReviewsById(this.id);
    //this.delete(this.id);
    this.usersId = localStorage.getItem("userId");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReviewPage');
  }

  confirm(id) {
    let confirm = this.alertCtrl.create({
      title: 'Recensie verwijderen?',
      message: 'Deze recensie verwijderen van de accomo-pagina',
      buttons: [
        {
          text: 'Annuleer',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Bevestig',
          handler: () => {
            this.delete(this.id);
          }
        }
      ]
    });
    confirm.present();
  }

  delete(id) {
    this.ReviewService.delete(this.id);
    this.navCtrl.setRoot( AccomodatiePage )
  }

  edit() {
    this.navCtrl.push(ReviewEditPage, {
        'id': this.id
      })
  }

  getReviewsById(id) {
    this.ReviewService.getReviewsById(id)
      .then(data => {
        this.review = data;
        this.reviewId = data[0].userId;
        this.loadingIndicator.dismiss();
      })
  }

  private createLoadingIndicator() {
    this.loadingIndicator = this.loadingCtrl.create({
      content: 'Loading accomodaties...'
    });
    this.loadingIndicator.present();
  }
  
}
