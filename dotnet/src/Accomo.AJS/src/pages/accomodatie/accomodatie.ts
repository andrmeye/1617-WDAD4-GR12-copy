import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, IonicPage } from 'ionic-angular';

import { AccomodatieServiceProvider } from '../../providers/accomodatie-service/accomodatie-service';
import { Accomodatie } from '../../models/accomodatie.model';
import { AccomodatieDetailPage } from './accomodatieDetail/accomodatie.detail';
import { AccomodatieResultaatPage } from './accomodatieResultaat/accomodatie.resultaat';

import { AccomodatieLocatieCreatePage } from './accomodatieLocatie/accomodatieLocatie.create';

/*
  Generated class for the Accomodatie page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-accomodatie',
  templateUrl: 'accomodatie.html'
})
export class AccomodatiePage {
  private accomodaties: any;
  private accomodatie: Accomodatie;
  private steden: any[];
  private accomodatiesFilter: any;
  private stedenFilter = [];
  private other: any;

  private loadingIndicator: any;
  filter: string = "alle";

  constructor(public navCtrl: NavController, public navParams: NavParams, private accomodatieService: AccomodatieServiceProvider, private loadingCtrl: LoadingController) {
      this.createLoadingIndicator();
      this.navCtrl = navCtrl;
      this.initializeItems();      
  }

  ionViewDidEnter() {
    this.accomodatieService.load()
    .then(data => {
      this.accomodaties = data;
      this.loadingIndicator.dismiss();
    })
  }

  private createLoadingIndicator() {
    this.loadingIndicator = this.loadingCtrl.create({
      content: 'Loading accomodaties...'
    });
    this.loadingIndicator.present();
  }

  showAccomodatie(accomoId) {
    console.log(accomoId);
      this.navCtrl.push(AccomodatieDetailPage, {
        'accomoId': accomoId
      })
  }

  initializeItems() {
    this.accomodatieService.load()
      .then(data => {
        this.accomodatiesFilter = data;
        console.log(this.accomodatiesFilter);
    });
  }

  getSteden() {
    this.stedenFilter = [];
    Object.keys(this.accomodatiesFilter).forEach(key => {
      this.other = this.accomodatiesFilter[key].accomodatieLocatie.stad;
      console.log(this.other);
      this.stedenFilter.push(this.other);
      
    })
    console.log(this.stedenFilter);
  }

  getItems(ev: any) {
    // Reset items back to all of the items
    this.getSteden();

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.stedenFilter = this.stedenFilter.filter((stad) => {
        return (stad.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  accomoByCity(stad) {
      this.navCtrl.push(AccomodatieResultaatPage, {
        'stad': stad
    })
  }
}
