import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, IonicPage, LoadingController } from 'ionic-angular';
import { AccomodatiePage } from '../accomodatie';
import { AccomodatieServiceProvider } from '../../../providers/accomodatie-service/accomodatie-service';
import { Accomodatie } from '../../../models/accomodatie.model';

import { ReviewServiceProvider } from '../../../providers/review-service/review-service';
import { ReviewPage } from '../../review/review';
import { BoekingPage } from '../../boeking/boeking';
import { ReviewDetailPage } from '../../review/reviewDetail/review.detail';
import { AuthService } from '../../../providers/authservice';

declare var google;
/*
  Generated class for the Accomodatie page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-accomodatieDetail',
  templateUrl: 'accomodatie.detail.html'
})
export class AccomodatieDetailPage {

  @ViewChild('map') mapElement: ElementRef;
  map: any;

  private submitted: boolean = false;
  private accomodatie: any;
  private loadingIndicator: any;
  private accomoId : string;
  private reviews: any; 
  private user: any; 
  private userId : any;
  private city: any;
  private country: any;
  private prijs: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private AuthService: AuthService, private reviewService: ReviewServiceProvider, private accomodatieService: AccomodatieServiceProvider, private loadingCtrl: LoadingController) {

    this.createLoadingIndicator();
    this.accomoId = this.navParams.get("accomoId");
    this.loadAccomodatie(this.accomoId);
    this.getReviews(this.accomoId);
    this.userId = localStorage.getItem("userId");
  }

  public loadAccomodatie(accomoId) {
    this.accomodatieService.loadById(accomoId)
      .then(data => {
        this.accomodatie = data;
        this.city = this.accomodatie[0].accomodatieLocatie.stad;
        this.country = this.accomodatie[0].accomodatieLocatie.land;
        this.prijs = this.accomodatie[0].accomodatiePrijs.prijs;
        this.initMap(this.city, this.country);
      })
  }

  initMap(city, country) {
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    var geocoder = new google.maps.Geocoder();

    this.geocodeAddress(geocoder, map, city, country);
  }

  geocodeAddress(geocoder, resultsMap, city, country) {
    var address = city + ", " + country;
    geocoder.geocode({'address': address}, function(results, status) {
      if (status === 'OK') {
        resultsMap.setCenter(results[0].geometry.location);
        var marker = new google.maps.Marker({
          map: resultsMap,
          position: results[0].geometry.location
        });
      } else {
        alert('Geocode was not successful for the following reason: ' + status);
      }
    });
  }  

  getReviews(accomoId) {
    this.reviewService.getReviews(accomoId)
      .then(data => {
        this.reviews = data;
        console.log(this.reviews);
        this.loadingIndicator.dismiss();
      })
  }

  private createLoadingIndicator() {
    this.loadingIndicator = this.loadingCtrl.create({
      content: 'Loading accomodaties...'
    });
    this.loadingIndicator.present();
  }

  reviewDetail(reviewId) {
    console.log(reviewId);
      this.navCtrl.push(ReviewDetailPage, {
        'id': reviewId
      })
  }

  createReview() {
    this.navCtrl.push(ReviewPage, {
      accomoId: this.accomoId,
      userId: this.userId
    });
  }

  createBoeking() {
    this.navCtrl.push(BoekingPage, {
      accomoId: this.accomoId,
      userId: this.userId,
      accomodatiePrijs: this.prijs
    });
  }

}  
