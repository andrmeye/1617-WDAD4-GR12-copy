import { Component } from '@angular/core';

import { NavController, NavParams } from 'ionic-angular';
import { AccomodatieLocatieServiceProvider } from '../../../providers/accomodatie-locatie-service/accomodatie-locatie-service';
import { AccomodatieLocatie } from '../../../models/accomodatieLocatie.model';
import { AccomodatieDetailPage } from '../accomodatieDetail/accomodatie.detail';

@Component({
  selector: 'page-accomodatieCreate',
  templateUrl: 'accomodatie.create.html'
})
export class AccomodatieCreatePage {
  
  private submitted: boolean = false;


  constructor() {
    
  }

  ionViewDidLoad() {
    console.log('Hello accomodatieLocatie Page');
  }

  onSubmit(form) {
    this.submitted = true;

    /*if (form.valid) {
      console.log('POST accomodatieLocatie: ' + this.accomodatieLocatie);
      this.accomodatieLocatieService
        .postAccomodatieLocatie(this.accomodatieLocatie)
        .subscribe();
    } else {
      console.log('Form is not valid.');
      this.submitted = false;
    }*/
          //this.navCtrl.push(AccomodatieDetailPage);
  }
}

