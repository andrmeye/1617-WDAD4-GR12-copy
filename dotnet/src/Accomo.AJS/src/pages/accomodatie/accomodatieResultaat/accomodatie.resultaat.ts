import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, IonicPage } from 'ionic-angular';

import { AccomodatieServiceProvider } from '../../../providers/accomodatie-service/accomodatie-service';
import { Accomodatie } from '../../../models/accomodatie.model';
import { AccomodatieDetailPage } from '../accomodatieDetail/accomodatie.detail';

import { AccomodatieLocatieCreatePage } from '../accomodatieLocatie/accomodatieLocatie.create';

/*
  Generated class for the Accomodatie page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-accomodatie.resultaat',
  templateUrl: 'accomodatie.resultaat.html'
})
export class AccomodatieResultaatPage {
  private accomodaties: any;
  private accomodatie: Accomodatie;
  private steden: any[];
  private accomodatiesFilter: Accomodatie[];
  private stedenFilter = [];
  private other: any;
  private stad = '';

  private loadingIndicator: any;
  filter: string = "alle";

  constructor(public navCtrl: NavController, public navParams: NavParams, private accomodatieService: AccomodatieServiceProvider, private loadingCtrl: LoadingController) {
      this.createLoadingIndicator();
      this.navCtrl = navCtrl;
      this.stad = this.navParams.get('stad').toLowerCase();
      this.loadAccomodaties();
  }

  loadAccomodaties() {
    if(this.stad == '') {
      this.accomodatieService.load()
      .then(data => {
        this.accomodaties = data;
        console.log('mag ni');        
      })
      this.loadingIndicator.dismiss();
    } else if (this.stad != '') {
      this.accomodatieService.loadByStad(this.stad)
      .then(data => {
        console.log(this.stad);
        this.accomodaties = data;
        console.log(this.accomodaties);
        console.log('mag wel');
      })
      this.loadingIndicator.dismiss();
    } 
  }

  private createLoadingIndicator() {
    this.loadingIndicator = this.loadingCtrl.create({
      content: 'Loading accomodaties...'
    });
    this.loadingIndicator.present();
  }

  showAccomodatie(accomoId) {
    console.log(accomoId);
      this.navCtrl.push(AccomodatieDetailPage, {
        'accomoId': accomoId
      })
  }
  
}
