import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, IonicPage, Platform, ActionSheetController } from 'ionic-angular';

import { UserService } from '../../providers/userService';
import { AuthService } from '../../providers/authservice';
import { HomePage } from '../home/home';
import { BoekingDetailPage } from '../boeking-detail/boeking-detail';

/**
 * Generated class for the UserPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user',
  templateUrl: 'user.html',
})
export class UserPage {
  private userName : any;
  private user: any;
  filter: string = "profiel";

  constructor(public platform: Platform,
              public actionsheetCtrl: ActionSheetController, 
              public navCtrl: NavController, 
              public navParams: NavParams, 
              private userService: UserService,
              private authService: AuthService) {

    this.userName = localStorage.getItem("userName");
    this.loadUser();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserPage');
  }

  public loadUser() {
    if (this.userName != null) {
      this.userService.load(this.userName)
      .then(data => {
        this.user = data;
        console.log(this.user);
      })
    }
  }

  showBoeking(boekingId) {
    console.log(boekingId);
      this.navCtrl.push(BoekingDetailPage, {
        'boekingId': boekingId
      })
  }

  openMenu() {
    let actionSheet = this.actionsheetCtrl.create({
      title: 'Account',
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Afmelden',
          role: 'destructive',
          icon: 'log-out',
          handler: () => {
            this.logout();
          }
        },
        {
          text: 'Annuleer',
          role: 'cancel', // will always sort to be on the bottom
          icon: 'close',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  public logout() {
    this.authService.logout();
    this.navCtrl.setRoot(HomePage);
  }
}
