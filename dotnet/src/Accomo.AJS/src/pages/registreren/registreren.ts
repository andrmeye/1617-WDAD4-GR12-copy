import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { AuthService } from '../../providers/authservice';
import { HomePage } from '../home/home';
import { Login } from '../login/login';

@Component({
  selector: 'page-registreren',
  templateUrl: 'registreren.html',
  providers: [AuthService]
})
export class Registreren {
  registerCredentials = {email: '', plainPassword: '', firstName: "", lastName: "", UserName: ""};  

  constructor(public navCtrl: NavController, public navParams: NavParams, private Authenticate: AuthService) {
    
  }

  public register() {
    this.Authenticate.register(this.registerCredentials)
    this.navCtrl.setRoot( Login )
    console.log('1')
  }

}
