import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { BoekingServiceProvider } from '../../providers/boeking-service/boeking-service';

/**
 * Generated class for the BoekingDetailPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-boeking-detail',
  templateUrl: 'boeking-detail.html',
})
export class BoekingDetailPage {

  private boeking: any;
  private boekingId: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public BoekingService: BoekingServiceProvider) {
    this.boekingId = this.navParams.get("boekingId");
    this.getboekingById(this.boekingId);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BoekingDetailPage');
  }

  getboekingById(id) {
    this.BoekingService.getBoekingById(this.boekingId)
      .then(data => {
        this.boeking = data;
        console.log(data);
      })
  }

}
