import { Component, EventEmitter, Output, ViewChild } from '@angular/core';

import { NavController, ViewController, App, LoadingController } from 'ionic-angular';
import { AccomodatiePage } from '../accomodatie/accomodatie';
import { AccomodatieResultaatPage } from '../accomodatie/accomodatieResultaat/accomodatie.resultaat';
import { Accomodatie } from '../../models/accomodatie.model';
import { AccomodatieServiceProvider } from '../../providers/accomodatie-service/accomodatie-service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChild('myNav') nav: NavController;
  searchQuery: string = '';
  private accomodaties: any;
  private steden = [];
  private other: any;
  private loadingIndicator: any;
  private userName: any;
  
  constructor(public navCtrl: NavController, private loadingCtrl: LoadingController, public viewCtrl: ViewController, public appCtrl: App, private accomodatieService: AccomodatieServiceProvider) {
    this.createLoadingIndicator();    
    this.initializeItems();
    this.userName = localStorage.getItem("userName");
  }

  initializeItems() {
    this.accomodatieService.load()
      .then(data => {
        this.accomodaties = data;
        this.loadingIndicator.dismiss();
        console.log(this.accomodaties);
    });
  }

  getSteden() {
    this.steden = [];
    Object.keys(this.accomodaties).forEach(key => {
      this.other = this.accomodaties[key].accomodatieLocatie.stad;
      console.log(this.other);
      this.steden.push(this.other);
      
    })
    console.log(this.steden);
  }
  
  getItems(ev: any) {
    // Reset items back to all of the items
    this.getSteden();

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.steden = this.steden.filter((stad) => {
        return (stad.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  accomoByCity(stad) {
      this.navCtrl.push(AccomodatieResultaatPage, {
        'stad': stad
    })
  }

  private createLoadingIndicator() {
    this.loadingIndicator = this.loadingCtrl.create({
      content: 'Loading accomodaties...'
    });
    this.loadingIndicator.present();
  }
}
