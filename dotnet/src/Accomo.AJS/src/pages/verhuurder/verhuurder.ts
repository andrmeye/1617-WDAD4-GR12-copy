import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-verhuurder',
  templateUrl: 'verhuurder.html'
})
export class Verhuurder {

  constructor(public navCtrl: NavController) {
    
  }

}
