import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, LoadingController, NavController } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';

import { Verhuurder } from '../pages/verhuurder/verhuurder';
import { Registreren } from '../pages/registreren/registreren';
import { HomePage } from '../pages/home/home';
import { UserPage } from '../pages/user/user';
import { Login } from '../pages/login/login';
import { AccomodatiePage } from '../pages/accomodatie/accomodatie';
import { UserService } from '../providers/userService';

@Component({
  selector: 'page-app',
  templateUrl: 'app.html'
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;
  @ViewChild(NavController) navCtrl: NavController;
  rootPage = HomePage;
  private userName: any;
  private user: any;

  constructor(platform: Platform, private loadingCtrl: LoadingController, private userService: UserService) {

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();
    });

    this.userName = localStorage.getItem("userName");
    this.loadUser();
  }

  menuItems = [
    { title: 'Home', component: HomePage },
    { title: 'Aanmelden', component: Login },
    { title: 'Registreren', component: Registreren },
    { title: 'Accomodaties', component: AccomodatiePage }
  ];

  menuItemSelected(menuItem) {
    console.log("Selected Item", menuItem);

    this.nav.setRoot(menuItem.component);
  }

  public loadUser() {
    if (this.userName != null) {
      this.userService.load(this.userName)
      .then(data => {
        this.user = data;
        console.log(data);
      })
    }
  }

  userPage() {
    this.nav.setRoot(UserPage);
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    if(localStorage.getItem("userName") == null) {
      this.userName = null
    } else if (localStorage.getItem("userName") != null) {
      this.userName = localStorage.getItem("userName");
    }

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }
  
}