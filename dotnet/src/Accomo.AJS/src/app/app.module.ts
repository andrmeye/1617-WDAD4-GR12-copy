import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { Verhuurder } from '../pages/verhuurder/verhuurder';
import { Registreren } from '../pages/registreren/registreren';
import { HomePage } from '../pages/home/home';
import { Login } from '../pages/login/login';
import { HttpModule, Http, XHRBackend, RequestOptions } from '@angular/http';
import { HttpService } from '../providers/http.service';
import { LoadingIndicatorService } from '../core/loading-indicator/loading-indicator.service';
import '../core/rxjs-extensions';
import { LoggerService } from '../providers/logger.service';
import { BrowserModule } from '@angular/platform-browser';

import { AccomodatiePage } from '../pages/accomodatie/accomodatie';
import { ReviewPage } from '../pages/review/review';
import { UserPage } from '../pages/user/user';
import { BoekingPage } from '../pages/boeking/boeking';
import { BoekingDetailPage } from '../pages/boeking-detail/boeking-detail';
import { ReviewDetailPage } from '../pages/review/reviewDetail/review.detail';
import { ReviewEditPage } from '../pages/review/reviewEdit/review.edit';
import { AccomodatieDetailPage } from '../pages/accomodatie/accomodatieDetail/accomodatie.detail';
import { AccomodatieResultaatPage } from '../pages/accomodatie/accomodatieResultaat/accomodatie.resultaat';
import { AuthService } from '../providers/authservice';
import { UserService } from '../providers/userService'
import { AccomodatieServiceProvider } from '../providers/accomodatie-service/accomodatie-service';
import { ReviewServiceProvider } from '../providers/review-service/review-service';
import { BoekingServiceProvider } from '../providers/boeking-service/boeking-service';

@NgModule({
  declarations: [
    MyApp,
    Verhuurder,
    Registreren,
    HomePage,
    Login,
    AccomodatiePage,
    AccomodatieDetailPage,
    ReviewPage,
    AccomodatieResultaatPage,
    ReviewDetailPage,
    ReviewEditPage,
    BoekingPage,
    UserPage,
    BoekingDetailPage
  ],
  imports: [
    IonicModule.forRoot(MyApp, {}, {
      links: [
        { component: AccomodatieDetailPage, name: 'AccomodatieDetail', segment: 'accomodatieDetail/:accomoId' }
      ]
    }), 
    BrowserModule, 
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    Login,
    Verhuurder,
    Registreren,
    AccomodatiePage,
    AccomodatieResultaatPage,
    AccomodatieDetailPage,
    ReviewPage,
    ReviewDetailPage,
    ReviewEditPage,
    BoekingPage,
    UserPage,
    BoekingDetailPage
  ],
  providers: [
    LoadingIndicatorService,
    LoggerService,
    HttpService,
    AuthService,
    UserService,
    {
      provide: Http,
      useFactory: (backend: XHRBackend, defaultOptions: RequestOptions, liService: LoadingIndicatorService, lgService: LoggerService) => new HttpService(backend, defaultOptions, liService, lgService),
      deps: [ XHRBackend, RequestOptions, LoadingIndicatorService, LoggerService]
    },
    AccomodatieServiceProvider,
    ReviewServiceProvider,
    BoekingServiceProvider,
  ]
})
export class AppModule {}
