using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Accomo.Db;
using Accomo.Models;
using Accomo.Models.Security;

namespace Accomo.API.Controllers
{
    [Route("api/[controller]")]
     public class AccomodatiemediaController : BaseController 
     {       
        private const string FAILGETENTITIES = "Mislukt om Accomodaties op te halen van de API";
        private const string FAILGETENTITYBYID = "Mislukt om accomodaties op te halen van de API op basis van ID: {0}";


        public AccomodatiemediaController(ApplicationDbContext applicationDbContext, UserManager<ApplicationUser> userManager):base(applicationDbContext, userManager) 
        {
        }

        [HttpGet(Name = "GetAccomodatieMedia")]
        public async Task<IActionResult> GetAccomodatieMedia()
        {
            var model = await ApplicationDbContext.AccomodatieMedia.Include(o => o.Accomodatie).ToListAsync();
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITIES);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }

        [HttpGet("{accomodatiemediaId:int}", Name = "GetAccomodatieMediaById")]
        public async Task<IActionResult> GetAccomodatieMediaById(Int16 accomodatiemediaId)
        {
            var model = await ApplicationDbContext.AccomodatieMedia.FirstOrDefaultAsync(o => o.Id == accomodatiemediaId);
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITYBYID, accomodatiemediaId);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }

        [HttpGet("accomodatie/{accomodatieId:int}", Name = "GetAccomodatieMediaByAccomodatieId")]
        public async Task<IActionResult> GetAccomodatieMediaByAccomodatieId(Int16 accomodatieId)
        {
            var model = await ApplicationDbContext.AccomodatieMedia.Where(o => o.AccomodatieId == accomodatieId).ToListAsync();
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITYBYID, accomodatieId);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }

        [HttpGet("mediatype/{mediatypeId:int}", Name = "GetAccomodatieMediaByMediatypeId")]
        public async Task<IActionResult> GetAccomodatieMediaByMediatypeId(Int16 mediatypeId)
        {
            var model = await ApplicationDbContext.AccomodatieMedia.Where(o => o.MediaTypeId == mediatypeId).ToListAsync();
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITYBYID, mediatypeId);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }
    }
}
