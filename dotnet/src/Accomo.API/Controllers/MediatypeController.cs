using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Accomo.Db;
using Accomo.Models;
using Accomo.Models.Security;

namespace Accomo.API.Controllers
{
    [Route("api/[controller]")]
     public class MediatypesController : BaseController 
     {       
        private const string FAILGETENTITIES = "Mislukt om Media Types op te halen van de API";
        private const string FAILGETENTITYBYID = "Mislukt om Media Types op te halen van de API op basis van ID: {0}";


        public MediatypesController(ApplicationDbContext applicationDbContext, UserManager<ApplicationUser> userManager):base(applicationDbContext, userManager) 
        {
        }

        [HttpGet(Name = "GetMediaTypes")]
        public async Task<IActionResult> GetMediaTypes()
        {
            var model = await ApplicationDbContext.Mediatypes.ToListAsync();
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITIES);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }

        [HttpGet("{mediatypeId:int}", Name = "GetMediaTypeById")]
        public async Task<IActionResult> GetMediaTypeById(Int16 mediatypeId)
        {
            var model = await ApplicationDbContext.Mediatypes.FirstOrDefaultAsync(o => o.Id == mediatypeId);
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITYBYID, mediatypeId);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }

        
    }
}
