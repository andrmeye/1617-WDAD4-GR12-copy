using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Accomo.Db;
using Accomo.Models.Security;

namespace Accomo.API.Controllers {
    public class BaseController : Controller {
        protected ApplicationDbContext ApplicationDbContext { get; set; }
        protected UserManager<ApplicationUser> UserManager { get; set; }
        public BaseController(ApplicationDbContext applicationDbContext, UserManager<ApplicationUser> userManager) {
            ApplicationDbContext = applicationDbContext;
            UserManager = userManager;
        }
    }
}