using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Accomo.Db;
using Accomo.Models;
using Accomo.Models.Security;
using Accomo.Models.Utilities;
using Accomo.Models.ViewModels;

namespace Accomo.API.Controllers
{
    [Route("api/[controller]")]

    public class AccountController : Controller
    {
        
        private const string FAILGETENTITIES = "Mislukt om locaties op te halen van de API";
        private const string FAILGETENTITYBYID = "Mislukt om locaties op te halen van de API op basis van ID: {0}";
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ILogger _logger;
        public AccountController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            ILoggerFactory loggerFactory)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = loggerFactory.CreateLogger<AccountController>();
            
        }
        public async Task<IActionResult> Index() {
            var model = await _userManager.Users
            .Include(u => u.Roles)        
            .ToListAsync();

            if (model == null)
            {
                var msg = String.Format(FAILGETENTITIES);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }
        /*
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LogOff()
        {
            
            await _signInManager.SignOutAsync();
            _logger.LogInformation(4, "User logged out.");
            return RedirectToAction("Login", "Account", new { area = "" });
        }
        /*
        [HttpGet]
        public async Task<IActionResult> Create() 
        {  
            var viewModel = await ViewModel();
            
            return View(viewModel);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(RoleViewModel model)
        {
            var alert = new Alert();
            if (ModelState.IsValid)
            {
                try{
                    var role = new ApplicationRole{ Name = model.ApplicationRole.Name, Beschrijving = model.ApplicationRole.Beschrijving};
                    var result = await _roleManager.CreateAsync(role);
                    if (result.Succeeded)
                    {
                    TempData["Status"] = "Role met naam: " + model.ApplicationRole.Name + " is succesvol aangemaakt.";
                    }
                    return RedirectToAction("Index");
                }catch(Exception ex)
            {
               
                TempData["Status"] = ex.Message;
                                
                return View(model);

            }
                

            }
            return View(model);
        }
        
        [HttpGet]
        public async Task<IActionResult> Edit(string id)
        {
            
            var model = await _roleManager.FindByIdAsync(id);         
            if(model == null)
            {
                TempData["Status"] = "Er is geen gelijknamige record gevonden.";
                return RedirectToAction("Index");
            }

            var viewModel = await ViewModel(model);
            
            return View(viewModel);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(RoleViewModel model)
        {
            var alert = new Alert();
            try 
            {
                if(!ModelState.IsValid)
                {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.INVALID;
                    TempData["Status"] = ApplicationDbContextMessage.INVALID;
                    throw new Exception();
                }   
                    
                var originalModel = _roleManager.Roles.FirstOrDefault(m => m.Id == model.ApplicationRole.Id);
                
                if(originalModel == null) 
                {
                    TempData["Status"] = ApplicationDbContextMessage.NOTEXISTS;
                    throw new Exception();
                }

                originalModel.Name = model.ApplicationRole.Name;
                originalModel.Beschrijving = model.ApplicationRole.Beschrijving;


                var result = await _roleManager.UpdateAsync(originalModel);
                if (result.Succeeded)
                    {
                    TempData["Status"] = "Role met naam: " + model.ApplicationRole.Name + " is succesvol bewerkt.";
                    }
                    return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                alert.Type = AlertType.Error;
                alert.ExceptionMessage = ex.Message;
                TempData["Status"] = ex.Message;

                model = await ViewModel(model.ApplicationRole);
                ModelState.AddModelError(string.Empty, alert.ExceptionMessage);
            }
            return View(model);
        }
        
        //[HttpGet("[area]/[controller]/[action]/{id:int}")]
        [HttpGet]
        public async Task<IActionResult> Delete(string id, [FromQuery] ActionType actionType)
        {
            var model = await _roleManager.FindByIdAsync(id);  
            Console.WriteLine("Naam van de role: " + id);
            //var newId = new Guid(id);
            //var model = await _applicationdbContext.ApplicationRoles.FirstOrDefaultAsync(o => o.Name == roleName);
            if(model == null)
            {
                Console.WriteLine("BROLLLL");
                return RedirectToAction("Index");
            }
            
            var viewModel = new ActionRoleViewModel()
            {
                ApplicationRole = model,
                ActionType = actionType
            };
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(ActionRoleViewModel model)
        {
            var alert = new Alert();// Alert    
            try
            {
                var originalModel = _roleManager.Roles.FirstOrDefault(m => m.Id == model.ApplicationRole.Id);
                if(originalModel == null)
                {
                    alert.Message = alert.ExceptionMessage = ApplicationDbContextMessage.NOTEXISTS;
                    TempData["Status"] = ApplicationDbContextMessage.NOTEXISTS;
                    throw new Exception();
                }
                try{

                
                    switch(model.ActionType)
                    {
                        case ActionType.Delete:
                            alert.Message = ApplicationDbContextMessage.DELETEOK;
                            TempData["Status"] = ApplicationDbContextMessage.DELETEOK;
                            await _roleManager.DeleteAsync(originalModel);
                            break;
                        case ActionType.SoftDelete:
                            alert.Message = ApplicationDbContextMessage.SOFTDELETEOK;
                            TempData["Status"] = ApplicationDbContextMessage.SOFTDELETEOK;
                            originalModel.DeletedAt = DateTime.Now;
                            await _roleManager.UpdateAsync(originalModel);
                            break;
                        case ActionType.SoftUnDelete:
                            alert.Message = ApplicationDbContextMessage.SOFTUNDELETEOK;
                            TempData["Status"] = ApplicationDbContextMessage.SOFTUNDELETEOK;
                            originalModel.DeletedAt = (Nullable<DateTime>)null;
                            await _roleManager.UpdateAsync(originalModel);
                            break;
                    }
                }catch(Exception ex)
                {
                        switch(model.ActionType)
                    {
                        case ActionType.Delete:
                            TempData["Status"] = ApplicationDbContextMessage.DELETENOK;
                            break;
                        case ActionType.SoftDelete:
                            TempData["Status"] = ApplicationDbContextMessage.SOFTDELETENOK;
                            break;
                        case ActionType.SoftUnDelete:
                            TempData["Status"] = ApplicationDbContextMessage.SOFTUNDELETENOK;
                            break;
                    }

                }
                
                    
                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(alert);
                }
                else
                {
                    return RedirectToAction("Index");
                }

            }
            catch (Exception ex)
            {
                alert.Type = AlertType.Error;
                alert.ExceptionMessage = ex.Message;

                if (this.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
                {
                    return Json(alert);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
        }
        */
        private async Task<RoleViewModel> ViewModel(ApplicationRole applicationRole = null) 
        {
            var viewModel = new RoleViewModel 
            {
                ApplicationRole = (applicationRole != null)?applicationRole:new ApplicationRole(),
            };

            return viewModel;
        }
    }
}