using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Accomo.Db;
using Accomo.Models;
using Accomo.Models.Security;

namespace Accomo.API.Controllers
{
    [Route("api/[controller]")]
     public class UsersController : BaseController 
     {       
        private const string FAILGETENTITIES = "Failed to get users from the API";
        private const string FAILGETENTITYBYID = "Failed to get user from the API by Id: {0}";


        public UsersController(ApplicationDbContext applicationDbContext, UserManager<ApplicationUser> userManager):base(applicationDbContext, userManager) 
        {
        }

        [HttpGet(Name = "GetUsers")]
        public async Task<IActionResult> GetUsers()
        {
            var model = await ApplicationDbContext.Users.ToListAsync();
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITIES);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }

        [HttpGet("byId/{UserId:guid}", Name = "GetUserById")]
        public async Task<IActionResult> GetUserById(Guid userId)
        {
            var model = await ApplicationDbContext.Users
                                    .Where(u => u.Id == userId)
                                    .Include(u => u.UserName)
                                    .Include(u => u.Email)
                                    .Include(u => u.Description)
                                    .Include(u => u.FirstName)
                                    .Include(u => u.LastName)
                                    .Include(u => u.Gender)
                                    .Include(u => u.DayOfBirth)
                                    .ToListAsync();
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITYBYID, userId);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }

        [HttpGet("byUserName/{UserName}", Name = "GetUserByUserName")]
        public async Task<IActionResult> GetUserByUserName(string UserName)
        {
            var model = await ApplicationDbContext.Users.Where(u => u.UserName == UserName)
            .Include(a => a.AccomodatieReviews)
            .Include(a => a.Accomodaties)
            .Include(a => a.Boekingen)
            .ToListAsync();
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITYBYID, UserName);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }

        [HttpPost]
        public async Task<IActionResult> CreateUser([FromBody] ApplicationUser user) {
            if (user == null) {
                return BadRequest();
            }
            ApplicationDbContext.Users.Add(user);
            await ApplicationDbContext.SaveChangesAsync();

            return Json("user data recieved");
        }
    }
}