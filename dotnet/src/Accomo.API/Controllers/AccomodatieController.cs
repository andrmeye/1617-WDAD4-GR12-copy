using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Accomo.Db;
using Accomo.Models;
using Accomo.Models.Security;

namespace Accomo.API.Controllers
{
    [Route("api/[controller]")]
     public class AccomodatiesController : BaseController 
     {       
        private const string FAILGETENTITIES = "Mislukt om accomodaties op te halen van de API";
        private const string FAILGETENTITYBYID = "Mislukt om accomodaties op te halen van de API op basis van ID: {0}";


        public AccomodatiesController(ApplicationDbContext applicationDbContext, UserManager<ApplicationUser> userManager):base(applicationDbContext, userManager) 
        {
        }

        [HttpGet(Name = "GetAccomodaties")]
        public async Task<IActionResult> GetAccomodaties()
        {
            var model = await ApplicationDbContext.Accomodaties
            .Include(al => al.AccomodatieLocatie)
            .Include(ca => ca.Categorie)
            .Include(p => p.AccomodatiePrijs)
            .Include(at => at.AccomodatieType)
            .Include(a => a.User)
            .Include(a => a.Veiligheidsvoorzieningen)
            .Include(a => a.AccomodatieReviews)
            .Include(a => a.AccomodatieMedia)
            .Include(a => a.AccomodatieDetails)
            .Include(a => a.Voorzieningen)
            .Include(a => a.Wishlists)
            .Include(a => a.Huisregels)
            .Include(a => a.Boekingen)
            .Include(a => a.Transacties)
            .ToListAsync();

            if (model == null)
            {
                var msg = String.Format(FAILGETENTITIES);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }

        [HttpGet("{accomodatieId:int}", Name = "GetAccomodatieById")]
        public async Task<IActionResult> GetAccomodatieById(Int16 accomodatieId)
        {
            var model = await ApplicationDbContext.Accomodaties.Where(o => o.Id == accomodatieId)
            .Include(al => al.AccomodatieLocatie)
            .Include(ca => ca.Categorie)
            .Include(p => p.AccomodatiePrijs)
            .Include(at => at.AccomodatieType)
            .Include(u => u.User)
            .Include(a => a.Veiligheidsvoorzieningen)
            .Include(a => a.AccomodatieReviews)
            .Include(a => a.AccomodatieMedia)
            .Include(a => a.AccomodatieDetails)
            .Include(a => a.Voorzieningen)
            .Include(a => a.Wishlists)
            .Include(a => a.Huisregels)
            .Include(a => a.Boekingen)
            .Include(a => a.Transacties)
            .ToListAsync();
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITYBYID, accomodatieId);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }

        [HttpGet("byStad/{stad}", Name = "GetAccomodatieByStad")]
        public async Task<IActionResult> GetAccomodatieByStad(string stad)
        {
            var model = await ApplicationDbContext.Accomodaties.Where(o => o.AccomodatieLocatie.Stad.ToLower() == stad)
            .Include(al => al.AccomodatieLocatie)
            .Include(ca => ca.Categorie)
            .Include(p => p.AccomodatiePrijs)
            .Include(at => at.AccomodatieType)
            .Include(u => u.User)
            .Include(a => a.Veiligheidsvoorzieningen)
            .Include(a => a.AccomodatieReviews)
            .Include(a => a.AccomodatieMedia)
            .Include(a => a.AccomodatieDetails)
            .Include(a => a.Voorzieningen)
            .Include(a => a.Wishlists)
            .Include(a => a.Huisregels)
            .Include(a => a.Boekingen)
            .Include(a => a.Transacties)
            .ToListAsync();
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITYBYID, stad);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }

        [HttpPost(Name = "CreateAccomodatie")]
        public async Task<IActionResult> CreateAccomodatie([FromBody] Accomodatie item)
        {
            if(item == null)
            {
                return BadRequest();
            }
            ApplicationDbContext.Accomodaties.Add(item);
            await ApplicationDbContext.SaveChangesAsync();

            return this.CreatedAtRoute("GetAccomodatieById", new { Controller = "AccomodatiesController", accomodatieId = item.Id }, item);
        }

        [HttpPut("{accomodatieId:int}", Name = "UpdateAccomodatie")]
        public async Task<IActionResult> UpdateAccomodatie(Int16 accomodatieId, [FromBody] Accomodatie item)
        {
            if(item == null || item.Id != accomodatieId)
            {
                return BadRequest();
            }

            var model = await ApplicationDbContext.Accomodaties.FirstOrDefaultAsync(o => o.Id == accomodatieId);

            if(model == null)
            {
                var msg = String.Format(FAILGETENTITYBYID, accomodatieId);
                return NotFound(msg);
            }
            model.Naam = item.Naam;
            model.Beschrijving = item.Beschrijving;
            model.MaxAantalGasten = item.MaxAantalGasten;
            model.AccomodatieLocatieId = item.AccomodatieLocatieId;


            ApplicationDbContext.Accomodaties.Attach(model);
            ApplicationDbContext.Entry(model).State = EntityState.Modified;
            await ApplicationDbContext.SaveChangesAsync();

            return new NoContentResult();
        }
        
        
    }
}
