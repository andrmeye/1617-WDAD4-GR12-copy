using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Accomo.Db;
using Accomo.Models;
using Accomo.Models.Security;

namespace Accomo.API.Controllers
{
    [Route("api/[controller]")]
     public class ReviewsController : BaseController 
     {       
        private const string FAILGETENTITIES = "Mislukt om reviews op te halen van de API";
        private const string FAILGETENTITYBYID = "Mislukt om reviews op te halen van de API op basis van ID: {0}";


        public ReviewsController(ApplicationDbContext applicationDbContext, UserManager<ApplicationUser> userManager):base(applicationDbContext, userManager) 
        {
        }

        [HttpGet(Name = "GetReviews")]
        public async Task<IActionResult> GetReviews()
        {
            var model = await ApplicationDbContext.AccomodatieReviews
            .Include(a => a.User)
            .ToListAsync();
            
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITIES);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }

        [HttpGet("{reviewId:int}", Name = "GetReviewById")]
        public async Task<IActionResult> GetReviewById(Int16 reviewId)
        {
            var model = await ApplicationDbContext.AccomodatieReviews.Where(o => o.Id == reviewId)
            .Include(a => a.User)
            .ToListAsync();
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITYBYID, reviewId);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }

        [HttpGet("byAccomoId/{AccomoId}", Name = "GetReviewByAccomoId")]
        public async Task<IActionResult> GetReviewByAccomoId(int AccomoId)
        {
            var model = await ApplicationDbContext.AccomodatieReviews.Where(a => a.AccomodatieId == AccomoId)
            .Include(a => a.User)
            .ToListAsync();
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITYBYID, AccomoId);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }

        [HttpPost]
        public async Task<IActionResult> CreateReview([FromBody] AccomodatieReview review) {
            if (review == null) {
                return BadRequest();
            }
            ApplicationDbContext.AccomodatieReviews.Add(review);
            await ApplicationDbContext.SaveChangesAsync();

            return CreatedAtRoute("GetReviews", new { id = review.Id }, review);
        }

        [HttpPut("{reviewId:int}", Name = "UpdateAccomodatieReview")]
        public async Task<IActionResult> UpdateAccomodatieReview(Int16 reviewId, [FromBody] AccomodatieReview review)
        {
            if(review == null || review.Id != reviewId)
            {
                return BadRequest();
            }

            var model = await ApplicationDbContext.AccomodatieReviews.FirstOrDefaultAsync(o => o.Id == reviewId);

            if(model == null)
            {
                var msg = String.Format(FAILGETENTITYBYID, reviewId);
                return NotFound(msg);
            }
            model.Titel = review.Titel;
            model.Inhoud = review.Inhoud;
            model.Beoordeling = review.Beoordeling;

            ApplicationDbContext.AccomodatieReviews.Attach(model);
            ApplicationDbContext.Entry(model).State = EntityState.Modified;
            await ApplicationDbContext.SaveChangesAsync();

            return new NoContentResult();
        }

        [HttpDelete("{reviewId:int}", Name = "DeleteReviewById")]
        public async Task<IActionResult> DeleteAccomodatieReview(Int16 reviewId, [FromQuery] string softDelete)
        {
            var model = await ApplicationDbContext.AccomodatieReviews.FirstOrDefaultAsync(o => o.Id == reviewId);
            if(model == null)
            {
                var msg = String.Format(FAILGETENTITYBYID, reviewId);
                return NotFound(msg);
            }

            if(!string.IsNullOrEmpty(softDelete))
            {
                ApplicationDbContext.AccomodatieReviews.Attach(model);
                model.DeletedAt = (softDelete == "delete")?DateTime.Now:(Nullable<DateTime>)null;
                ApplicationDbContext.Entry(model).State = EntityState.Modified;
                await ApplicationDbContext.SaveChangesAsync();
            }
            else
            {
                ApplicationDbContext.AccomodatieReviews.Remove(model);
                await ApplicationDbContext.SaveChangesAsync();
            }

            return new NoContentResult();
        }
    }
}
