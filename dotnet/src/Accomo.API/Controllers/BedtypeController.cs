using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Accomo.Db;
using Accomo.Models;
using Accomo.Models.Security;

namespace Accomo.API.Controllers
{
    [Route("api/[controller]")]
     public class BedTypesController : BaseController 
     {       
        private const string FAILGETENTITIES = "Mislukt om bedtypes op te halen van de API";
        private const string FAILGETENTITYBYID = "Mislukt om bedtypes op te halen van de API op basis van ID: {0}";


        public BedTypesController(ApplicationDbContext applicationDbContext, UserManager<ApplicationUser> userManager):base(applicationDbContext, userManager) 
        {
        }

        [HttpGet(Name = "GetBedTypes")]
        public async Task<IActionResult> GetBedTypes()
        {
            var model = await ApplicationDbContext.BedTypes.ToListAsync();
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITIES);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }

        [HttpGet("{bedTypeId:int}", Name = "GetBedTypeById")]
        public async Task<IActionResult> GetBedTypeById(Int16 bedTypeId)
        {
            var model = await ApplicationDbContext.BedTypes.FirstOrDefaultAsync(o => o.Id == bedTypeId);
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITYBYID, bedTypeId);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }

        
    }
}
