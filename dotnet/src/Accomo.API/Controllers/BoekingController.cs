using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Accomo.Db;
using Accomo.Models;
using Accomo.Models.Security;

namespace Accomo.API.Controllers
{
    [Route("api/[controller]")]
     public class BoekingController : BaseController 
     {       
        private const string FAILGETENTITIES = "Mislukt om boekingen op te halen van de API";
        private const string FAILGETENTITYBYID = "Mislukt om boekingen op te halen van de API op basis van ID: {0}";


        public BoekingController(ApplicationDbContext applicationDbContext, UserManager<ApplicationUser> userManager):base(applicationDbContext, userManager) 
        {
        }

        [HttpGet(Name = "GetBoekingen")]
        public async Task<IActionResult> GetBoekingen()
        {
            var model = await ApplicationDbContext.Boekingen
            .Include(a => a.User)
            .Include(a => a.Accomodatie)
            .ToListAsync();
            
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITIES);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }

        [HttpGet("{boekingId:int}", Name = "GetBoekingById")]
        public async Task<IActionResult> GetBoekingById(Int16 boekingId)
        {
            var model = await ApplicationDbContext.Boekingen.Where(o => o.Id == boekingId)
            .Include(a => a.User)
            .ToListAsync();
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITYBYID, boekingId);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }

        [HttpGet("byAccomoId/{AccomoId}", Name = "GetBoekingByAccomoId")]
        public async Task<IActionResult> GetBoekingByAccomoId(int AccomoId)
        {
            var model = await ApplicationDbContext.Boekingen.Where(a => a.AccomodatieId == AccomoId)
            .Include(a => a.User)
            .Include(a => a.Accomodatie)
            .ToListAsync();
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITYBYID, AccomoId);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }

        [HttpPost]
        public async Task<IActionResult> CreateBoeking([FromBody] Boeking boeking) {
            if (boeking == null) {
                return BadRequest();
            }
            ApplicationDbContext.Boekingen.Add(boeking);
            await ApplicationDbContext.SaveChangesAsync();

            return CreatedAtRoute("GetBoekingen", new { id = boeking.Id }, boeking);
        }

        [HttpPut("{boekingId:int}", Name = "UpdateBoeking")]
        public async Task<IActionResult> UpdateBoeking(Int16 boekingId, [FromBody] Boeking boeking)
        {
            if(boeking == null || boeking.Id != boekingId)
            {
                return BadRequest();
            }

            var model = await ApplicationDbContext.Boekingen.FirstOrDefaultAsync(o => o.Id == boekingId);

            if(model == null)
            {
                var msg = String.Format(FAILGETENTITYBYID, boekingId);
                return NotFound(msg);
            }
            model.PrijsPerDag = boeking.PrijsPerDag;
            model.TotaalBedrag = boeking.TotaalBedrag;
            model.CheckIn = boeking.CheckIn;
            model.CheckOut = boeking.CheckOut;
            model.AantalGasten = boeking.AantalGasten;

            ApplicationDbContext.Boekingen.Attach(model);
            ApplicationDbContext.Entry(model).State = EntityState.Modified;
            await ApplicationDbContext.SaveChangesAsync();

            return new NoContentResult();
        }

        [HttpDelete("{boekingId:int}", Name = "DeleteBoekingById")]
        public async Task<IActionResult> DeleteBoeking(Int16 boekingId, [FromQuery] string softDelete)
        {
            var model = await ApplicationDbContext.Boekingen.FirstOrDefaultAsync(o => o.Id == boekingId);
            if(model == null)
            {
                var msg = String.Format(FAILGETENTITYBYID, boekingId);
                return NotFound(msg);
            }

            if(!string.IsNullOrEmpty(softDelete))
            {
                ApplicationDbContext.Boekingen.Attach(model);
                model.DeletedAt = (softDelete == "delete")?DateTime.Now:(Nullable<DateTime>)null;
                ApplicationDbContext.Entry(model).State = EntityState.Modified;
                await ApplicationDbContext.SaveChangesAsync();
            }
            else
            {
                ApplicationDbContext.Boekingen.Remove(model);
                await ApplicationDbContext.SaveChangesAsync();
            }

            return new NoContentResult();
        }
    }
}
