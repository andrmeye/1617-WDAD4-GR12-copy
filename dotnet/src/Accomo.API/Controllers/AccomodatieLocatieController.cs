using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Accomo.Db;
using Accomo.Models;
using Accomo.Models.Security;

namespace Accomo.API.Controllers
{
    [Route("api/[controller]")]
     public class AccomodatieLocatiesController : BaseController 
     {       
        private const string FAILGETENTITIES = "Mislukt om locaties op te halen van de API";
        private const string FAILGETENTITYBYID = "Mislukt om locaties op te halen van de API op basis van ID: {0}";


        public AccomodatieLocatiesController(ApplicationDbContext applicationDbContext, UserManager<ApplicationUser> userManager):base(applicationDbContext, userManager) 
        {
        }

        [HttpGet(Name = "GetAccomodatieLocaties")]
        public async Task<IActionResult> GetAccomodatieLocaties()
        {
            var model = await ApplicationDbContext.AccomodatieLocaties
            .Include(sd => sd.Stad)
            .Include(a => a.Accomodatie)
            .ToListAsync();

            if (model == null)
            {
                var msg = String.Format(FAILGETENTITIES);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }

        [HttpGet("{AccomodatielocatieId:int}", Name = "GetAccomodatieLocatieById")]
        public async Task<IActionResult> GetAccomodatieLocatieById(Int16 accomodatieLocatieId)
        {
            var model = await ApplicationDbContext.AccomodatieLocaties.FirstOrDefaultAsync(o => o.Id == accomodatieLocatieId);
            if (model == null)
            {
                var msg = String.Format(FAILGETENTITYBYID, accomodatieLocatieId);
                return NotFound(msg);
            }
            return new OkObjectResult(model);
        }

        [HttpPost(Name = "CreateAccomodatieLocatie")]
        public async Task<IActionResult> CreateAccomodatieLocatie([FromBody] AccomodatieLocatie item)
        {
            if(item == null)
            {
                return BadRequest();
            }
            ApplicationDbContext.AccomodatieLocaties.Add(item);
            await ApplicationDbContext.SaveChangesAsync();

            return this.CreatedAtRoute("GetAccomodatieLocatieById", new { Controller = "AccomodatieLocatiesController", accomodatieLocatieId = item.Id }, item);
        }

        [HttpPut("{accomodatieLocatieId:int}", Name = "UpdateAccomodatieLocatie")]
        public async Task<IActionResult> UpdateAccomodatieLocatie(Int16 accomodatieLocatieId, [FromBody] AccomodatieLocatie item)
        {
            if(item == null || item.Id != accomodatieLocatieId)
            {
                return BadRequest();
            }

            var model = await ApplicationDbContext.AccomodatieLocaties.FirstOrDefaultAsync(o => o.Id == accomodatieLocatieId);

            if(model == null)
            {
                var msg = String.Format(FAILGETENTITYBYID, accomodatieLocatieId);
                return NotFound(msg);
            }
            model.Straat = item.Straat;
            model.HuisNummer = item.HuisNummer;
            model.KamerNummer = item.KamerNummer;

            ApplicationDbContext.AccomodatieLocaties.Attach(model);
            ApplicationDbContext.Entry(model).State = EntityState.Modified;
            await ApplicationDbContext.SaveChangesAsync();

            return new NoContentResult();
        }

        [HttpDelete("{accomodatieLocatieId:int}", Name = "DeleteAccomodatieLocatie")]
        public async Task<IActionResult> DeleteAccomodatieLocatie(Int16 accomodatieLocatieId, [FromQuery] string softDelete)
        {
            var model = await ApplicationDbContext.AccomodatieLocaties.FirstOrDefaultAsync(o => o.Id == accomodatieLocatieId);
            if(model == null)
            {
                var msg = String.Format(FAILGETENTITYBYID, accomodatieLocatieId);
                return NotFound(msg);
            }

            if(!string.IsNullOrEmpty(softDelete))
            {
                ApplicationDbContext.AccomodatieLocaties.Attach(model);
                model.DeletedAt = (softDelete == "delete")?DateTime.Now:(Nullable<DateTime>)null;
                ApplicationDbContext.Entry(model).State = EntityState.Modified;
                await ApplicationDbContext.SaveChangesAsync();
            }
            else
            {
                ApplicationDbContext.AccomodatieLocaties.Remove(model);
                await ApplicationDbContext.SaveChangesAsync();
            }

            return new NoContentResult();
        }
        
    }
}
