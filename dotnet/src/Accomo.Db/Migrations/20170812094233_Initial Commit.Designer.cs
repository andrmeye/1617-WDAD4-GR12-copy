﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Accomo.Db;

namespace Accomo.Db.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20170812094233_Initial Commit")]
    partial class InitialCommit
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("Npgsql:PostgresExtension:.uuid-ossp", "'uuid-ossp', '', ''")
                .HasAnnotation("ProductVersion", "1.1.0-rtm-22752");

            modelBuilder.Entity("Accomo.Models.Accomodatie", b =>
                {
                    b.Property<short>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<short>("AccomodatieLocatieId");

                    b.Property<short>("AccomodatiePrijsId");

                    b.Property<short>("AccomodatieTypeId");

                    b.Property<short>("BadkamerAantal");

                    b.Property<bool>("Beschikbaarheid");

                    b.Property<string>("Beschrijving")
                        .IsRequired();

                    b.Property<short>("CategorieId");

                    b.Property<DateTime>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("now()");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<short>("MaxAantalGasten");

                    b.Property<string>("MinLengteVerblijf");

                    b.Property<string>("Naam")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 255);

                    b.Property<DateTime?>("UpdatedAt")
                        .ValueGeneratedOnAddOrUpdate()
                        .HasDefaultValueSql("now()");

                    b.Property<Guid>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("AccomodatieLocatieId")
                        .IsUnique();

                    b.HasIndex("AccomodatiePrijsId")
                        .IsUnique();

                    b.HasIndex("AccomodatieTypeId");

                    b.HasIndex("CategorieId");

                    b.HasIndex("UserId");

                    b.ToTable("Accomodaties");
                });

            modelBuilder.Entity("Accomo.Models.AccomodatieDetails", b =>
                {
                    b.Property<short>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<short>("AccomodatieId");

                    b.Property<string>("Beschrijving")
                        .IsRequired();

                    b.Property<DateTime>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("now()");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<string>("Naam")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 255);

                    b.Property<DateTime?>("UpdatedAt")
                        .ValueGeneratedOnAddOrUpdate()
                        .HasDefaultValueSql("now()");

                    b.HasKey("Id");

                    b.HasIndex("AccomodatieId");

                    b.ToTable("AccomodatieDetails");
                });

            modelBuilder.Entity("Accomo.Models.AccomodatieHuisregel", b =>
                {
                    b.Property<short>("AccomodatieId");

                    b.Property<short>("HuisregelId");

                    b.HasKey("AccomodatieId", "HuisregelId");

                    b.HasIndex("HuisregelId");

                    b.ToTable("AccomodatieHuisregel");
                });

            modelBuilder.Entity("Accomo.Models.AccomodatieLocatie", b =>
                {
                    b.Property<short>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("now()");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<string>("HuisNummer")
                        .IsRequired();

                    b.Property<string>("KamerNummer");

                    b.Property<string>("Land")
                        .IsRequired();

                    b.Property<string>("PostCode")
                        .IsRequired();

                    b.Property<string>("Stad")
                        .IsRequired();

                    b.Property<string>("Straat")
                        .IsRequired();

                    b.Property<string>("Streek");

                    b.Property<DateTime?>("UpdatedAt")
                        .ValueGeneratedOnAddOrUpdate()
                        .HasDefaultValueSql("now()");

                    b.HasKey("Id");

                    b.ToTable("AccomodatieLocaties");
                });

            modelBuilder.Entity("Accomo.Models.AccomodatieMedia", b =>
                {
                    b.Property<short>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<short>("AccomodatieId");

                    b.Property<DateTime>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("now()");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<short>("MediaTypeId");

                    b.Property<string>("Naam")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 255);

                    b.Property<string>("URL")
                        .IsRequired();

                    b.Property<DateTime?>("UpdatedAt")
                        .ValueGeneratedOnAddOrUpdate()
                        .HasDefaultValueSql("now()");

                    b.HasKey("Id");

                    b.HasIndex("AccomodatieId");

                    b.HasIndex("MediaTypeId");

                    b.ToTable("AccomodatieMedia");
                });

            modelBuilder.Entity("Accomo.Models.AccomodatiePrijs", b =>
                {
                    b.Property<short>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("now()");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<double>("Prijs");

                    b.Property<string>("PrijsTypeNaam");

                    b.Property<double>("PrijsTypePercentage");

                    b.Property<DateTime?>("UpdatedAt")
                        .ValueGeneratedOnAddOrUpdate()
                        .HasDefaultValueSql("now()");

                    b.Property<short>("ValutaId");

                    b.HasKey("Id");

                    b.HasIndex("ValutaId");

                    b.ToTable("AccomodatiePrijzen");
                });

            modelBuilder.Entity("Accomo.Models.AccomodatieReview", b =>
                {
                    b.Property<short>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<short>("AccomodatieId");

                    b.Property<short>("Beoordeling");

                    b.Property<DateTime>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("now()");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<string>("Inhoud")
                        .IsRequired();

                    b.Property<string>("Titel")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 255);

                    b.Property<DateTime?>("UpdatedAt")
                        .ValueGeneratedOnAddOrUpdate()
                        .HasDefaultValueSql("now()");

                    b.Property<Guid>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("AccomodatieId");

                    b.HasIndex("UserId");

                    b.ToTable("AccomodatieReviews");
                });

            modelBuilder.Entity("Accomo.Models.AccomodatieScore", b =>
                {
                    b.Property<short>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<short>("AccomodatieReviewId");

                    b.Property<DateTime>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("now()");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<string>("Naam")
                        .IsRequired();

                    b.Property<short>("Score");

                    b.Property<DateTime?>("UpdatedAt")
                        .ValueGeneratedOnAddOrUpdate()
                        .HasDefaultValueSql("now()");

                    b.HasKey("Id");

                    b.HasIndex("AccomodatieReviewId");

                    b.ToTable("AccomodatieScores");
                });

            modelBuilder.Entity("Accomo.Models.AccomodatieType", b =>
                {
                    b.Property<short>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Beschrijving")
                        .IsRequired();

                    b.Property<DateTime>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("now()");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<string>("Naam")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 255);

                    b.Property<DateTime?>("UpdatedAt")
                        .ValueGeneratedOnAddOrUpdate()
                        .HasDefaultValueSql("now()");

                    b.HasKey("Id");

                    b.ToTable("AccomodatieTypes");
                });

            modelBuilder.Entity("Accomo.Models.AccomodatieVeiligheidsvoorziening", b =>
                {
                    b.Property<short>("AccomodatieId");

                    b.Property<short>("VeiligheidsvoorzieningId");

                    b.HasKey("AccomodatieId", "VeiligheidsvoorzieningId");

                    b.HasIndex("VeiligheidsvoorzieningId");

                    b.ToTable("AccomodatieVeiligheidsvoorziening");
                });

            modelBuilder.Entity("Accomo.Models.AccomodatieVoorzieningType", b =>
                {
                    b.Property<short>("AccomodatieId");

                    b.Property<short>("VoorzieningTypeId");

                    b.HasKey("AccomodatieId", "VoorzieningTypeId");

                    b.HasIndex("VoorzieningTypeId");

                    b.ToTable("AccomodatieVoorzieningType");
                });

            modelBuilder.Entity("Accomo.Models.AccomodatieWishlist", b =>
                {
                    b.Property<short>("AccomodatieId");

                    b.Property<short>("WishlistId");

                    b.HasKey("AccomodatieId", "WishlistId");

                    b.HasIndex("WishlistId");

                    b.ToTable("AccomodatieWishlist");
                });

            modelBuilder.Entity("Accomo.Models.BedType", b =>
                {
                    b.Property<short>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Beschrijving")
                        .IsRequired();

                    b.Property<DateTime>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("now()");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<string>("Naam")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 255);

                    b.Property<DateTime?>("UpdatedAt")
                        .ValueGeneratedOnAddOrUpdate()
                        .HasDefaultValueSql("now()");

                    b.HasKey("Id");

                    b.ToTable("BedTypes");
                });

            modelBuilder.Entity("Accomo.Models.BedTypeAccomodatie", b =>
                {
                    b.Property<short>("BedTypeId");

                    b.Property<short>("AccomodatieId");

                    b.HasKey("BedTypeId", "AccomodatieId");

                    b.HasIndex("AccomodatieId");

                    b.ToTable("BedTypeAccomodatie");
                });

            modelBuilder.Entity("Accomo.Models.Boeking", b =>
                {
                    b.Property<short>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AantalGasten");

                    b.Property<short>("AccomodatieId");

                    b.Property<DateTime>("AnnuleringsDatum");

                    b.Property<DateTime>("CheckIn");

                    b.Property<DateTime>("CheckOut");

                    b.Property<DateTime>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("now()");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<double>("PrijsPerDag");

                    b.Property<double>("PrijsPerVerblijf");

                    b.Property<double>("TotaalBedrag");

                    b.Property<DateTime?>("UpdatedAt")
                        .ValueGeneratedOnAddOrUpdate()
                        .HasDefaultValueSql("now()");

                    b.Property<Guid>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("AccomodatieId");

                    b.HasIndex("UserId");

                    b.ToTable("Boekingen");
                });

            modelBuilder.Entity("Accomo.Models.Categorie", b =>
                {
                    b.Property<short>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Beschrijving");

                    b.Property<DateTime>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("now()");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<string>("Naam");

                    b.Property<DateTime?>("UpdatedAt")
                        .ValueGeneratedOnAddOrUpdate()
                        .HasDefaultValueSql("now()");

                    b.HasKey("Id");

                    b.ToTable("Categorieën");
                });

            modelBuilder.Entity("Accomo.Models.Huisregel", b =>
                {
                    b.Property<short>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Beschrijving");

                    b.Property<DateTime>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("now()");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<string>("Naam");

                    b.Property<DateTime?>("UpdatedAt")
                        .ValueGeneratedOnAddOrUpdate()
                        .HasDefaultValueSql("now()");

                    b.HasKey("Id");

                    b.ToTable("Huisregels");
                });

            modelBuilder.Entity("Accomo.Models.Korting", b =>
                {
                    b.Property<short>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Beschrijving")
                        .IsRequired();

                    b.Property<DateTime>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("now()");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<string>("Naam")
                        .IsRequired();

                    b.Property<double>("Percentage");

                    b.Property<DateTime?>("UpdatedAt")
                        .ValueGeneratedOnAddOrUpdate()
                        .HasDefaultValueSql("now()");

                    b.HasKey("Id");

                    b.ToTable("Kortingen");
                });

            modelBuilder.Entity("Accomo.Models.MediaType", b =>
                {
                    b.Property<short>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Beschrijving");

                    b.Property<DateTime>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("now()");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<string>("Naam")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 255);

                    b.Property<DateTime?>("UpdatedAt")
                        .ValueGeneratedOnAddOrUpdate()
                        .HasDefaultValueSql("now()");

                    b.HasKey("Id");

                    b.ToTable("Mediatypes");
                });

            modelBuilder.Entity("Accomo.Models.Security.ApplicationRole", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Beschrijving")
                        .HasAnnotation("MaxLength", 511);

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<DateTime>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("now()");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<string>("Name")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("NormalizedName")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<DateTime?>("UpdatedAt")
                        .ValueGeneratedOnAddOrUpdate()
                        .HasDefaultValueSql("now()");

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex");

                    b.ToTable("AspNetRoles");
                });

            modelBuilder.Entity("Accomo.Models.Security.ApplicationUser", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<DateTime>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("now()");

                    b.Property<DateTime?>("DayOfBirth");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<string>("Description")
                        .HasAnnotation("MaxLength", 511);

                    b.Property<string>("Email")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<string>("FirstName")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 255);

                    b.Property<byte>("Gender");

                    b.Property<string>("LastName")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 255);

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("NormalizedUserName")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<DateTime?>("UpdatedAt")
                        .ValueGeneratedOnAddOrUpdate()
                        .HasDefaultValueSql("now()");

                    b.Property<string>("UserName")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("plainPassword");

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex");

                    b.ToTable("AspNetUsers");
                });

            modelBuilder.Entity("Accomo.Models.Transactie", b =>
                {
                    b.Property<short>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<short>("AccomodatieId");

                    b.Property<double>("BetaaldBedrag");

                    b.Property<short>("BoekingId");

                    b.Property<DateTime>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("now()");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<short>("KortingId");

                    b.Property<double>("TotaalBedrag");

                    b.Property<DateTime>("TransferAt");

                    b.Property<DateTime?>("UpdatedAt")
                        .ValueGeneratedOnAddOrUpdate()
                        .HasDefaultValueSql("now()");

                    b.Property<short>("ValutaId");

                    b.HasKey("Id");

                    b.HasIndex("AccomodatieId");

                    b.HasIndex("BoekingId");

                    b.HasIndex("KortingId");

                    b.HasIndex("ValutaId");

                    b.ToTable("Transacties");
                });

            modelBuilder.Entity("Accomo.Models.Valuta", b =>
                {
                    b.Property<short>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Code")
                        .IsRequired();

                    b.Property<string>("Naam")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("Valutas");
                });

            modelBuilder.Entity("Accomo.Models.Veiligheidsvoorziening", b =>
                {
                    b.Property<short>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Beschrijving");

                    b.Property<DateTime>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("now()");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<string>("Naam");

                    b.Property<DateTime?>("UpdatedAt")
                        .ValueGeneratedOnAddOrUpdate()
                        .HasDefaultValueSql("now()");

                    b.HasKey("Id");

                    b.ToTable("Veiligheidsvoorzieningen");
                });

            modelBuilder.Entity("Accomo.Models.VoorzieningType", b =>
                {
                    b.Property<short>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Beschrijving");

                    b.Property<DateTime>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("now()");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<string>("Naam")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 255);

                    b.Property<DateTime?>("UpdatedAt")
                        .ValueGeneratedOnAddOrUpdate()
                        .HasDefaultValueSql("now()");

                    b.HasKey("Id");

                    b.ToTable("VoorzieningTypes");
                });

            modelBuilder.Entity("Accomo.Models.Wishlist", b =>
                {
                    b.Property<short>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("now()");

                    b.Property<DateTime?>("DeletedAt");

                    b.Property<string>("Naam")
                        .IsRequired()
                        .HasAnnotation("MaxLength", 255);

                    b.Property<DateTime?>("UpdatedAt")
                        .ValueGeneratedOnAddOrUpdate()
                        .HasDefaultValueSql("now()");

                    b.Property<Guid>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("Wishlists");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<System.Guid>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<Guid>("RoleId");

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<System.Guid>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<Guid>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<System.Guid>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<Guid>("UserId");

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<System.Guid>", b =>
                {
                    b.Property<Guid>("UserId");

                    b.Property<Guid>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserToken<System.Guid>", b =>
                {
                    b.Property<Guid>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens");
                });

            modelBuilder.Entity("Accomo.Models.Accomodatie", b =>
                {
                    b.HasOne("Accomo.Models.AccomodatieLocatie", "AccomodatieLocatie")
                        .WithOne("Accomodatie")
                        .HasForeignKey("Accomo.Models.Accomodatie", "AccomodatieLocatieId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Accomo.Models.AccomodatiePrijs", "AccomodatiePrijs")
                        .WithOne("Accomodatie")
                        .HasForeignKey("Accomo.Models.Accomodatie", "AccomodatiePrijsId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Accomo.Models.AccomodatieType", "AccomodatieType")
                        .WithMany("Accomodaties")
                        .HasForeignKey("AccomodatieTypeId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Accomo.Models.Categorie", "Categorie")
                        .WithMany("Accomodaties")
                        .HasForeignKey("CategorieId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Accomo.Models.Security.ApplicationUser", "User")
                        .WithMany("Accomodaties")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Accomo.Models.AccomodatieDetails", b =>
                {
                    b.HasOne("Accomo.Models.Accomodatie", "Accomodatie")
                        .WithMany("AccomodatieDetails")
                        .HasForeignKey("AccomodatieId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Accomo.Models.AccomodatieHuisregel", b =>
                {
                    b.HasOne("Accomo.Models.Accomodatie", "Accomodatie")
                        .WithMany("Huisregels")
                        .HasForeignKey("AccomodatieId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Accomo.Models.Huisregel", "Huisregel")
                        .WithMany("Accomodaties")
                        .HasForeignKey("HuisregelId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Accomo.Models.AccomodatieMedia", b =>
                {
                    b.HasOne("Accomo.Models.Accomodatie", "Accomodatie")
                        .WithMany("AccomodatieMedia")
                        .HasForeignKey("AccomodatieId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Accomo.Models.MediaType", "MediaType")
                        .WithMany("AccomodatieMedia")
                        .HasForeignKey("MediaTypeId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Accomo.Models.AccomodatiePrijs", b =>
                {
                    b.HasOne("Accomo.Models.Valuta", "Valuta")
                        .WithMany("AccomodatiePrijzen")
                        .HasForeignKey("ValutaId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Accomo.Models.AccomodatieReview", b =>
                {
                    b.HasOne("Accomo.Models.Accomodatie", "Accomodatie")
                        .WithMany("AccomodatieReviews")
                        .HasForeignKey("AccomodatieId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Accomo.Models.Security.ApplicationUser", "User")
                        .WithMany("AccomodatieReviews")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Accomo.Models.AccomodatieScore", b =>
                {
                    b.HasOne("Accomo.Models.AccomodatieReview", "AccomodatieReview")
                        .WithMany("AccomodatieScores")
                        .HasForeignKey("AccomodatieReviewId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Accomo.Models.AccomodatieVeiligheidsvoorziening", b =>
                {
                    b.HasOne("Accomo.Models.Accomodatie", "Accomodatie")
                        .WithMany("Veiligheidsvoorzieningen")
                        .HasForeignKey("AccomodatieId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Accomo.Models.Veiligheidsvoorziening", "Veiligheidsvoorziening")
                        .WithMany("Accomodaties")
                        .HasForeignKey("VeiligheidsvoorzieningId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Accomo.Models.AccomodatieVoorzieningType", b =>
                {
                    b.HasOne("Accomo.Models.Accomodatie", "Accomodatie")
                        .WithMany("Voorzieningen")
                        .HasForeignKey("AccomodatieId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Accomo.Models.VoorzieningType", "VoorzieningType")
                        .WithMany("Accomodaties")
                        .HasForeignKey("VoorzieningTypeId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Accomo.Models.AccomodatieWishlist", b =>
                {
                    b.HasOne("Accomo.Models.Accomodatie", "Accomodatie")
                        .WithMany("Wishlists")
                        .HasForeignKey("AccomodatieId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Accomo.Models.Wishlist", "Wishlist")
                        .WithMany("Accomodaties")
                        .HasForeignKey("WishlistId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Accomo.Models.BedTypeAccomodatie", b =>
                {
                    b.HasOne("Accomo.Models.Accomodatie", "Accomodatie")
                        .WithMany("BedTypes")
                        .HasForeignKey("AccomodatieId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Accomo.Models.BedType", "BedType")
                        .WithMany("Accomodaties")
                        .HasForeignKey("BedTypeId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Accomo.Models.Boeking", b =>
                {
                    b.HasOne("Accomo.Models.Accomodatie", "Accomodatie")
                        .WithMany("Boekingen")
                        .HasForeignKey("AccomodatieId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Accomo.Models.Security.ApplicationUser", "User")
                        .WithMany("Boekingen")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Accomo.Models.Transactie", b =>
                {
                    b.HasOne("Accomo.Models.Accomodatie", "Accomodatie")
                        .WithMany("Transacties")
                        .HasForeignKey("AccomodatieId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Accomo.Models.Boeking", "Boeking")
                        .WithMany("Transacties")
                        .HasForeignKey("BoekingId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Accomo.Models.Korting", "Korting")
                        .WithMany("Transacties")
                        .HasForeignKey("KortingId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Accomo.Models.Valuta", "Valuta")
                        .WithMany("Transacties")
                        .HasForeignKey("ValutaId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Accomo.Models.Wishlist", b =>
                {
                    b.HasOne("Accomo.Models.Security.ApplicationUser", "User")
                        .WithMany("Wishlists")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<System.Guid>", b =>
                {
                    b.HasOne("Accomo.Models.Security.ApplicationRole")
                        .WithMany("Claims")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<System.Guid>", b =>
                {
                    b.HasOne("Accomo.Models.Security.ApplicationUser")
                        .WithMany("Claims")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<System.Guid>", b =>
                {
                    b.HasOne("Accomo.Models.Security.ApplicationUser")
                        .WithMany("Logins")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<System.Guid>", b =>
                {
                    b.HasOne("Accomo.Models.Security.ApplicationRole")
                        .WithMany("Users")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Accomo.Models.Security.ApplicationUser")
                        .WithMany("Roles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
