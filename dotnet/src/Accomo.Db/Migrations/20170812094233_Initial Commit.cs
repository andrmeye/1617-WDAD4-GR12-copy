﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Accomo.Db.Migrations
{
    public partial class InitialCommit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreatePostgresExtension("uuid-ossp");

            migrationBuilder.CreateTable(
                name: "AccomodatieLocaties",
                columns: table => new
                {
                    Id = table.Column<short>(nullable: false)
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    CreatedAt = table.Column<DateTime>(nullable: false, defaultValueSql: "now()")
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    HuisNummer = table.Column<string>(nullable: false),
                    KamerNummer = table.Column<string>(nullable: true),
                    Land = table.Column<string>(nullable: false),
                    PostCode = table.Column<string>(nullable: false),
                    Stad = table.Column<string>(nullable: false),
                    Straat = table.Column<string>(nullable: false),
                    Streek = table.Column<string>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true, defaultValueSql: "now()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccomodatieLocaties", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AccomodatieTypes",
                columns: table => new
                {
                    Id = table.Column<short>(nullable: false)
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    Beschrijving = table.Column<string>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false, defaultValueSql: "now()")
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    Naam = table.Column<string>(maxLength: 255, nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true, defaultValueSql: "now()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccomodatieTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BedTypes",
                columns: table => new
                {
                    Id = table.Column<short>(nullable: false)
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    Beschrijving = table.Column<string>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false, defaultValueSql: "now()")
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    Naam = table.Column<string>(maxLength: 255, nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true, defaultValueSql: "now()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BedTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Categorieën",
                columns: table => new
                {
                    Id = table.Column<short>(nullable: false)
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    Beschrijving = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false, defaultValueSql: "now()")
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    Naam = table.Column<string>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true, defaultValueSql: "now()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categorieën", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Huisregels",
                columns: table => new
                {
                    Id = table.Column<short>(nullable: false)
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    Beschrijving = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false, defaultValueSql: "now()")
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    Naam = table.Column<string>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true, defaultValueSql: "now()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Huisregels", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Kortingen",
                columns: table => new
                {
                    Id = table.Column<short>(nullable: false)
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    Beschrijving = table.Column<string>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false, defaultValueSql: "now()")
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    Naam = table.Column<string>(nullable: false),
                    Percentage = table.Column<double>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true, defaultValueSql: "now()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Kortingen", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Mediatypes",
                columns: table => new
                {
                    Id = table.Column<short>(nullable: false)
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    Beschrijving = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false, defaultValueSql: "now()")
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    Naam = table.Column<string>(maxLength: 255, nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true, defaultValueSql: "now()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Mediatypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    Beschrijving = table.Column<string>(maxLength: 511, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false, defaultValueSql: "now()")
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true, defaultValueSql: "now()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false)
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false, defaultValueSql: "now()")
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    DayOfBirth = table.Column<DateTime>(nullable: true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    Description = table.Column<string>(maxLength: 511, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    FirstName = table.Column<string>(maxLength: 255, nullable: false),
                    Gender = table.Column<byte>(nullable: false),
                    LastName = table.Column<string>(maxLength: 255, nullable: false),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    PasswordHash = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    SecurityStamp = table.Column<string>(nullable: true),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true, defaultValueSql: "now()"),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    plainPassword = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Valutas",
                columns: table => new
                {
                    Id = table.Column<short>(nullable: false)
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    Code = table.Column<string>(nullable: false),
                    Naam = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Valutas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Veiligheidsvoorzieningen",
                columns: table => new
                {
                    Id = table.Column<short>(nullable: false)
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    Beschrijving = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false, defaultValueSql: "now()")
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    Naam = table.Column<string>(nullable: true),
                    UpdatedAt = table.Column<DateTime>(nullable: true, defaultValueSql: "now()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Veiligheidsvoorzieningen", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "VoorzieningTypes",
                columns: table => new
                {
                    Id = table.Column<short>(nullable: false)
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    Beschrijving = table.Column<string>(nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false, defaultValueSql: "now()")
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    Naam = table.Column<string>(maxLength: 255, nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true, defaultValueSql: "now()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VoorzieningTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true),
                    RoleId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Wishlists",
                columns: table => new
                {
                    Id = table.Column<short>(nullable: false)
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    CreatedAt = table.Column<DateTime>(nullable: false, defaultValueSql: "now()")
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    Naam = table.Column<string>(maxLength: 255, nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true, defaultValueSql: "now()"),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Wishlists", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Wishlists_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<Guid>(nullable: false),
                    RoleId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AccomodatiePrijzen",
                columns: table => new
                {
                    Id = table.Column<short>(nullable: false)
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    CreatedAt = table.Column<DateTime>(nullable: false, defaultValueSql: "now()")
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    Prijs = table.Column<double>(nullable: false),
                    PrijsTypeNaam = table.Column<string>(nullable: true),
                    PrijsTypePercentage = table.Column<double>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true, defaultValueSql: "now()"),
                    ValutaId = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccomodatiePrijzen", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccomodatiePrijzen_Valutas_ValutaId",
                        column: x => x.ValutaId,
                        principalTable: "Valutas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Accomodaties",
                columns: table => new
                {
                    Id = table.Column<short>(nullable: false)
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    AccomodatieLocatieId = table.Column<short>(nullable: false),
                    AccomodatiePrijsId = table.Column<short>(nullable: false),
                    AccomodatieTypeId = table.Column<short>(nullable: false),
                    BadkamerAantal = table.Column<short>(nullable: false),
                    Beschikbaarheid = table.Column<bool>(nullable: false),
                    Beschrijving = table.Column<string>(nullable: false),
                    CategorieId = table.Column<short>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false, defaultValueSql: "now()")
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    MaxAantalGasten = table.Column<short>(nullable: false),
                    MinLengteVerblijf = table.Column<string>(nullable: true),
                    Naam = table.Column<string>(maxLength: 255, nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true, defaultValueSql: "now()"),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accomodaties", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Accomodaties_AccomodatieLocaties_AccomodatieLocatieId",
                        column: x => x.AccomodatieLocatieId,
                        principalTable: "AccomodatieLocaties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Accomodaties_AccomodatiePrijzen_AccomodatiePrijsId",
                        column: x => x.AccomodatiePrijsId,
                        principalTable: "AccomodatiePrijzen",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Accomodaties_AccomodatieTypes_AccomodatieTypeId",
                        column: x => x.AccomodatieTypeId,
                        principalTable: "AccomodatieTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Accomodaties_Categorieën_CategorieId",
                        column: x => x.CategorieId,
                        principalTable: "Categorieën",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Accomodaties_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AccomodatieDetails",
                columns: table => new
                {
                    Id = table.Column<short>(nullable: false)
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    AccomodatieId = table.Column<short>(nullable: false),
                    Beschrijving = table.Column<string>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false, defaultValueSql: "now()")
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    Naam = table.Column<string>(maxLength: 255, nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true, defaultValueSql: "now()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccomodatieDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccomodatieDetails_Accomodaties_AccomodatieId",
                        column: x => x.AccomodatieId,
                        principalTable: "Accomodaties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AccomodatieHuisregel",
                columns: table => new
                {
                    AccomodatieId = table.Column<short>(nullable: false),
                    HuisregelId = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccomodatieHuisregel", x => new { x.AccomodatieId, x.HuisregelId });
                    table.ForeignKey(
                        name: "FK_AccomodatieHuisregel_Accomodaties_AccomodatieId",
                        column: x => x.AccomodatieId,
                        principalTable: "Accomodaties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AccomodatieHuisregel_Huisregels_HuisregelId",
                        column: x => x.HuisregelId,
                        principalTable: "Huisregels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AccomodatieMedia",
                columns: table => new
                {
                    Id = table.Column<short>(nullable: false)
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    AccomodatieId = table.Column<short>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false, defaultValueSql: "now()")
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    MediaTypeId = table.Column<short>(nullable: false),
                    Naam = table.Column<string>(maxLength: 255, nullable: false),
                    URL = table.Column<string>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true, defaultValueSql: "now()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccomodatieMedia", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccomodatieMedia_Accomodaties_AccomodatieId",
                        column: x => x.AccomodatieId,
                        principalTable: "Accomodaties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AccomodatieMedia_Mediatypes_MediaTypeId",
                        column: x => x.MediaTypeId,
                        principalTable: "Mediatypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AccomodatieReviews",
                columns: table => new
                {
                    Id = table.Column<short>(nullable: false)
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    AccomodatieId = table.Column<short>(nullable: false),
                    Beoordeling = table.Column<short>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false, defaultValueSql: "now()")
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    Inhoud = table.Column<string>(nullable: false),
                    Titel = table.Column<string>(maxLength: 255, nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true, defaultValueSql: "now()"),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccomodatieReviews", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccomodatieReviews_Accomodaties_AccomodatieId",
                        column: x => x.AccomodatieId,
                        principalTable: "Accomodaties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AccomodatieReviews_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AccomodatieVeiligheidsvoorziening",
                columns: table => new
                {
                    AccomodatieId = table.Column<short>(nullable: false),
                    VeiligheidsvoorzieningId = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccomodatieVeiligheidsvoorziening", x => new { x.AccomodatieId, x.VeiligheidsvoorzieningId });
                    table.ForeignKey(
                        name: "FK_AccomodatieVeiligheidsvoorziening_Accomodaties_AccomodatieId",
                        column: x => x.AccomodatieId,
                        principalTable: "Accomodaties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AccomodatieVeiligheidsvoorziening_Veiligheidsvoorzieningen_VeiligheidsvoorzieningId",
                        column: x => x.VeiligheidsvoorzieningId,
                        principalTable: "Veiligheidsvoorzieningen",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AccomodatieVoorzieningType",
                columns: table => new
                {
                    AccomodatieId = table.Column<short>(nullable: false),
                    VoorzieningTypeId = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccomodatieVoorzieningType", x => new { x.AccomodatieId, x.VoorzieningTypeId });
                    table.ForeignKey(
                        name: "FK_AccomodatieVoorzieningType_Accomodaties_AccomodatieId",
                        column: x => x.AccomodatieId,
                        principalTable: "Accomodaties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AccomodatieVoorzieningType_VoorzieningTypes_VoorzieningTypeId",
                        column: x => x.VoorzieningTypeId,
                        principalTable: "VoorzieningTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AccomodatieWishlist",
                columns: table => new
                {
                    AccomodatieId = table.Column<short>(nullable: false),
                    WishlistId = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccomodatieWishlist", x => new { x.AccomodatieId, x.WishlistId });
                    table.ForeignKey(
                        name: "FK_AccomodatieWishlist_Accomodaties_AccomodatieId",
                        column: x => x.AccomodatieId,
                        principalTable: "Accomodaties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AccomodatieWishlist_Wishlists_WishlistId",
                        column: x => x.WishlistId,
                        principalTable: "Wishlists",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BedTypeAccomodatie",
                columns: table => new
                {
                    BedTypeId = table.Column<short>(nullable: false),
                    AccomodatieId = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BedTypeAccomodatie", x => new { x.BedTypeId, x.AccomodatieId });
                    table.ForeignKey(
                        name: "FK_BedTypeAccomodatie_Accomodaties_AccomodatieId",
                        column: x => x.AccomodatieId,
                        principalTable: "Accomodaties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BedTypeAccomodatie_BedTypes_BedTypeId",
                        column: x => x.BedTypeId,
                        principalTable: "BedTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Boekingen",
                columns: table => new
                {
                    Id = table.Column<short>(nullable: false)
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    AantalGasten = table.Column<int>(nullable: false),
                    AccomodatieId = table.Column<short>(nullable: false),
                    AnnuleringsDatum = table.Column<DateTime>(nullable: false),
                    CheckIn = table.Column<DateTime>(nullable: false),
                    CheckOut = table.Column<DateTime>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false, defaultValueSql: "now()")
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    PrijsPerDag = table.Column<double>(nullable: false),
                    PrijsPerVerblijf = table.Column<double>(nullable: false),
                    TotaalBedrag = table.Column<double>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true, defaultValueSql: "now()"),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Boekingen", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Boekingen_Accomodaties_AccomodatieId",
                        column: x => x.AccomodatieId,
                        principalTable: "Accomodaties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Boekingen_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AccomodatieScores",
                columns: table => new
                {
                    Id = table.Column<short>(nullable: false)
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    AccomodatieReviewId = table.Column<short>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false, defaultValueSql: "now()")
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    Naam = table.Column<string>(nullable: false),
                    Score = table.Column<short>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true, defaultValueSql: "now()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccomodatieScores", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AccomodatieScores_AccomodatieReviews_AccomodatieReviewId",
                        column: x => x.AccomodatieReviewId,
                        principalTable: "AccomodatieReviews",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Transacties",
                columns: table => new
                {
                    Id = table.Column<short>(nullable: false)
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    AccomodatieId = table.Column<short>(nullable: false),
                    BetaaldBedrag = table.Column<double>(nullable: false),
                    BoekingId = table.Column<short>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false, defaultValueSql: "now()")
                        .Annotation("Npgsql:ValueGeneratedOnAdd", true),
                    DeletedAt = table.Column<DateTime>(nullable: true),
                    KortingId = table.Column<short>(nullable: false),
                    TotaalBedrag = table.Column<double>(nullable: false),
                    TransferAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: true, defaultValueSql: "now()"),
                    ValutaId = table.Column<short>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Transacties", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Transacties_Accomodaties_AccomodatieId",
                        column: x => x.AccomodatieId,
                        principalTable: "Accomodaties",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Transacties_Boekingen_BoekingId",
                        column: x => x.BoekingId,
                        principalTable: "Boekingen",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Transacties_Kortingen_KortingId",
                        column: x => x.KortingId,
                        principalTable: "Kortingen",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Transacties_Valutas_ValutaId",
                        column: x => x.ValutaId,
                        principalTable: "Valutas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Accomodaties_AccomodatieLocatieId",
                table: "Accomodaties",
                column: "AccomodatieLocatieId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Accomodaties_AccomodatiePrijsId",
                table: "Accomodaties",
                column: "AccomodatiePrijsId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Accomodaties_AccomodatieTypeId",
                table: "Accomodaties",
                column: "AccomodatieTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Accomodaties_CategorieId",
                table: "Accomodaties",
                column: "CategorieId");

            migrationBuilder.CreateIndex(
                name: "IX_Accomodaties_UserId",
                table: "Accomodaties",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AccomodatieDetails_AccomodatieId",
                table: "AccomodatieDetails",
                column: "AccomodatieId");

            migrationBuilder.CreateIndex(
                name: "IX_AccomodatieHuisregel_HuisregelId",
                table: "AccomodatieHuisregel",
                column: "HuisregelId");

            migrationBuilder.CreateIndex(
                name: "IX_AccomodatieMedia_AccomodatieId",
                table: "AccomodatieMedia",
                column: "AccomodatieId");

            migrationBuilder.CreateIndex(
                name: "IX_AccomodatieMedia_MediaTypeId",
                table: "AccomodatieMedia",
                column: "MediaTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_AccomodatiePrijzen_ValutaId",
                table: "AccomodatiePrijzen",
                column: "ValutaId");

            migrationBuilder.CreateIndex(
                name: "IX_AccomodatieReviews_AccomodatieId",
                table: "AccomodatieReviews",
                column: "AccomodatieId");

            migrationBuilder.CreateIndex(
                name: "IX_AccomodatieReviews_UserId",
                table: "AccomodatieReviews",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AccomodatieScores_AccomodatieReviewId",
                table: "AccomodatieScores",
                column: "AccomodatieReviewId");

            migrationBuilder.CreateIndex(
                name: "IX_AccomodatieVeiligheidsvoorziening_VeiligheidsvoorzieningId",
                table: "AccomodatieVeiligheidsvoorziening",
                column: "VeiligheidsvoorzieningId");

            migrationBuilder.CreateIndex(
                name: "IX_AccomodatieVoorzieningType_VoorzieningTypeId",
                table: "AccomodatieVoorzieningType",
                column: "VoorzieningTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_AccomodatieWishlist_WishlistId",
                table: "AccomodatieWishlist",
                column: "WishlistId");

            migrationBuilder.CreateIndex(
                name: "IX_BedTypeAccomodatie_AccomodatieId",
                table: "BedTypeAccomodatie",
                column: "AccomodatieId");

            migrationBuilder.CreateIndex(
                name: "IX_Boekingen_AccomodatieId",
                table: "Boekingen",
                column: "AccomodatieId");

            migrationBuilder.CreateIndex(
                name: "IX_Boekingen_UserId",
                table: "Boekingen",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Transacties_AccomodatieId",
                table: "Transacties",
                column: "AccomodatieId");

            migrationBuilder.CreateIndex(
                name: "IX_Transacties_BoekingId",
                table: "Transacties",
                column: "BoekingId");

            migrationBuilder.CreateIndex(
                name: "IX_Transacties_KortingId",
                table: "Transacties",
                column: "KortingId");

            migrationBuilder.CreateIndex(
                name: "IX_Transacties_ValutaId",
                table: "Transacties",
                column: "ValutaId");

            migrationBuilder.CreateIndex(
                name: "IX_Wishlists_UserId",
                table: "Wishlists",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPostgresExtension("uuid-ossp");

            migrationBuilder.DropTable(
                name: "AccomodatieDetails");

            migrationBuilder.DropTable(
                name: "AccomodatieHuisregel");

            migrationBuilder.DropTable(
                name: "AccomodatieMedia");

            migrationBuilder.DropTable(
                name: "AccomodatieScores");

            migrationBuilder.DropTable(
                name: "AccomodatieVeiligheidsvoorziening");

            migrationBuilder.DropTable(
                name: "AccomodatieVoorzieningType");

            migrationBuilder.DropTable(
                name: "AccomodatieWishlist");

            migrationBuilder.DropTable(
                name: "BedTypeAccomodatie");

            migrationBuilder.DropTable(
                name: "Transacties");

            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "Huisregels");

            migrationBuilder.DropTable(
                name: "Mediatypes");

            migrationBuilder.DropTable(
                name: "AccomodatieReviews");

            migrationBuilder.DropTable(
                name: "Veiligheidsvoorzieningen");

            migrationBuilder.DropTable(
                name: "VoorzieningTypes");

            migrationBuilder.DropTable(
                name: "Wishlists");

            migrationBuilder.DropTable(
                name: "BedTypes");

            migrationBuilder.DropTable(
                name: "Boekingen");

            migrationBuilder.DropTable(
                name: "Kortingen");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "Accomodaties");

            migrationBuilder.DropTable(
                name: "AccomodatieLocaties");

            migrationBuilder.DropTable(
                name: "AccomodatiePrijzen");

            migrationBuilder.DropTable(
                name: "AccomodatieTypes");

            migrationBuilder.DropTable(
                name: "Categorieën");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Valutas");
        }
    }
}
