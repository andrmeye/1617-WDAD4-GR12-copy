using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Bogus;
using Bogus.DataSets;
using Bogus.Extensions;
using Accomo.Models;
using Accomo.Models.Security;

namespace Accomo.Db
{
    public class ApplicationDbContextSeeder {
        
        public static async void Initialize(IServiceProvider serviceProvider)
        {
            using(var context = serviceProvider.GetService<ApplicationDbContext>()) {
                
                var FKUserID = new Guid();
                var FKRoleId = new Guid();
                string rolenaam = null;
                var userManager = serviceProvider.GetService<UserManager<ApplicationUser>>();
                
                var roleManager = serviceProvider.GetService<RoleManager<ApplicationRole>>();

                
                // Random instance
                var random = new Random();
                // Bogus Data https://www.nuget.org/packages/Bogus/
                // It's like faker in JavaScript, PHP and Ruby
                // ------------------------------------------------
                // Role
                if(!roleManager.Roles.Any()) {      

                        var newroleSub = new ApplicationRole { Name = "Subscriber", Beschrijving = "Niet meer dan een gast, bezit enkel READ-rechten"};
                        await roleManager.CreateAsync(newroleSub);

                        var newroleEditor = new ApplicationRole { Name = "Editor", Beschrijving = "Kan dingen zelf aanmaken, bewerken en verwijderen, maar kan ook andermans zaken bewerken/verwijderen."};
                        await roleManager.CreateAsync(newroleEditor);

                        var newroleAdmin = new ApplicationRole { Name = "Administrator", Beschrijving = " De beheerder is de admin van de website, hij bezit de hoogste rechten."};
                        await roleManager.CreateAsync(newroleAdmin);

                }
                // User
                if(!userManager.Users.Any()) {                   
                         var userSkeleton = new Faker<Accomo.Models.Security.ApplicationUser>()
                        .RuleFor(u => u.UserName, f => f.Internet.UserName())
                        .RuleFor(u => u.Email, f => f.Internet.Email())
                        .RuleFor(u => u.Description, f => f.Lorem.Paragraphs(1))
                        .RuleFor(u => u.FirstName, f => f.Name.FirstName())
                        .RuleFor(u => u.LastName, f => f.Name.LastName())
                        .RuleFor(u => u.Gender, f => f.PickRandom<GenderType>())
                        .RuleFor(u => u.DayOfBirth, f => f.Date.Recent())    
        
                        .FinishWith((f, u) =>
                        {
                            Console.WriteLine("User created with Bogus: Id= {0} !", u.Id);
                        });

                        for(var i=0; i < 10; i++){
                            var user = userSkeleton.Generate();
                            if (i == 1){
                                FKUserID = user.Id;
                            }
                            await userManager.CreateAsync(user);
                            await userManager.AddToRoleAsync(user, "Administrator");
                        }
                                            
                }
                // User (Administrator) - Add custom user
                                 
                    var newCustomUser = new ApplicationUser { UserName = "CustomUser", 
                    Email = "andrmeye@student.arteveldehs.be", 
                    Description="This is the admin", 
                    FirstName="Andries",
                    LastName="Meyers",
                    plainPassword="Webdesign4",
                    Gender= GenderType.Male,
                    DayOfBirth= DateTime.Now,
                    };
                    await userManager.CreateAsync(newCustomUser, "Webdesign4");
                    await context.SaveChangesAsync();
                    await userManager.AddToRoleAsync(newCustomUser, "Administrator");
                                                            
                
                // Valuta
                if(!context.Valutas.Any())
                {

                    var lorem = new Bogus.DataSets.Lorem(locale: "nl" );
                    var valutaSkeleton = new Faker<Accomo.Models.Valuta>()
                        .RuleFor(u => u.Naam, f => lorem.Letter(2))
                        .RuleFor(u => u.Code, f => lorem.Letter(2))
                        .FinishWith((f, u) =>
                        {
                            Console.WriteLine("Valuta created with Bogus: Id= {0} !", u.Id);
                        });
                    
                    var valutas = new List<Valuta>();

                    for(var i = 0; i < 6;i++)
                    {
                        var valuta = valutaSkeleton.Generate();
                        valutas.Add(valuta);
                    }

                    context.Valutas.AddRange(valutas);
                    await context.SaveChangesAsync();
                }

                // Bed Type
                if(!context.BedTypes.Any())
                {
                    var lorem = new Bogus.DataSets.Lorem(locale: "nl" );
                    var bedtypeSkeleton = new Faker<Accomo.Models.BedType>()
                        .RuleFor(u => u.Naam, f => f.Hacker.Noun())
                        .RuleFor(u => u.Beschrijving, f => lorem.Paragraph(1))
                        
                        .FinishWith((f, u) =>
                        {
                            Console.WriteLine("BedType created with Bogus: Id= {0} !", u.Id);
                        });
                    
                    var bedtypes = new List<BedType>();

                    for(var i = 0; i < 6;i++)
                    {
                        var bedtype = bedtypeSkeleton.Generate();
                        bedtypes.Add(bedtype);
                    }

                    context.BedTypes.AddRange(bedtypes);
                    await context.SaveChangesAsync();
                }

                // Voorziening Type
                 if(!context.VoorzieningTypes.Any())
                {
                    var lorem = new Bogus.DataSets.Lorem(locale: "nl" );
                    var voorzieningtypeSkeleton = new Faker<Accomo.Models.VoorzieningType>()
                        .RuleFor(u => u.Naam, f => f.Hacker.Noun())                        
                        .RuleFor(u => u.Beschrijving, f => lorem.Paragraph(1))                        
                        .FinishWith((f, u) =>
                        {
                            Console.WriteLine("VoorzieningType created with Bogus: Id= {0} !", u.Id);
                        });
                    
                    var voorzieningtypes = new List<VoorzieningType>();

                    for(var i = 0; i < 6;i++)
                    {
                        var voorzieningtype = voorzieningtypeSkeleton.Generate();
                        voorzieningtypes.Add(voorzieningtype);
                    }
                    context.VoorzieningTypes.AddRange(voorzieningtypes);
                    await context.SaveChangesAsync();
                }

                // Wishlist
                 if(!context.Wishlists.Any())
                {
                    var Userke= userManager.Users.First();
                    FKUserID = Userke.Id;
                    var lorem = new Bogus.DataSets.Lorem(locale: "nl" );
                    var wishlistSkeleton = new Faker<Accomo.Models.Wishlist>()
                        .RuleFor(u => u.Naam, f => lorem.Word())
                        .RuleFor(u => u.UserId, f => FKUserID )                        
                        .FinishWith((f, u) =>
                        {
                            Console.WriteLine("Wishlist created with Bogus: Id={0}", u.Id);
                        });
                    
                    var wishlists = new List<Wishlist>();
                    
                    var wishlist = wishlistSkeleton.Generate();
                    wishlists.Add(wishlist);
                        
                    context.Wishlists.AddRange(wishlists);
                    await context.SaveChangesAsync();
                }

                // Media Type
                if(!context.Mediatypes.Any()) {
                    context.Mediatypes.AddRange(new List<MediaType>()
                    {
                        new MediaType { Naam = "Afbeelding", Beschrijving = "Afbeelding"},
                    });
                    await context.SaveChangesAsync();
                }



                // Accomodatie Type
                if(!context.AccomodatieTypes.Any()) 
                {
                    var lorem = new Bogus.DataSets.Lorem(locale: "nl");
                    var accomodatietypeSkeleton = new Faker<Accomo.Models.AccomodatieType>()
                        .RuleFor(u => u.Naam, f => f.Hacker.Noun())
                        .RuleFor(u => u.Beschrijving, f => lorem.Paragraph(1))
                        .FinishWith((f, u) =>
                        {
                            Console.WriteLine("AccomodatieType created with Bogus: Id= {0} !", u.Id);
                        });
                    
                    var accomodatietypes = new List<AccomodatieType>();

                    for(var i = 0; i < 3;i++)
                    {
                        var accomodatietype = accomodatietypeSkeleton.Generate();
                        accomodatietypes.Add(accomodatietype);
                    }

                    context.AccomodatieTypes.AddRange(accomodatietypes);
                    await context.SaveChangesAsync();
                }
                                
                 // AccomodatiePrijs 
                if(!context.AccomodatiePrijzen.Any()) 
                {
                    var lorem = new Bogus.DataSets.Lorem(locale: "nl");
                    var accomodatiePrijsSkeleton = new Faker<Accomo.Models.AccomodatiePrijs>()
                        .RuleFor(u => u.Prijs, f => (double) random.Next(20,150))
                        .RuleFor(u => u.PrijsTypeNaam, f => lorem.Word())
                        .RuleFor(u => u.PrijsTypePercentage, f => (double) f.Random.Double(0, 1))
                        .RuleFor(u => u.ValutaId, f => (short) 1)
                        .FinishWith((f, u) =>
                        {
                            Console.WriteLine("AccomodatiePrijs created with Bogus: Id= {0} !", u.Id);
                        });
                    
                    var accomodatiePrijzen = new List<AccomodatiePrijs>();

                    var accomodatiePrijs = accomodatiePrijsSkeleton.Generate();
                    accomodatiePrijzen.Add(accomodatiePrijs);

                    context.AccomodatiePrijzen.AddRange(accomodatiePrijzen);
                    await context.SaveChangesAsync();
                }
                
                

                // AccomodatieLocatie
                if(!context.AccomodatieLocaties.Any()) 
                {
                    var lorem = new Bogus.DataSets.Lorem(locale: "nl");
                    var accomodatieLocatieSkeleton = new Faker<Accomo.Models.AccomodatieLocatie>()
                        .RuleFor(u => u.Straat, f => f.Address.StreetAddress())
                        .RuleFor(u => u.HuisNummer, f => f.Address.BuildingNumber())
                        .RuleFor(u => u.KamerNummer, f => f.Address.SecondaryAddress())
                        .RuleFor(u => u.Stad, f => f.Address.City())
                        .RuleFor(u => u.PostCode, f => f.Address.ZipCode())
                        .RuleFor(u => u.Streek, f => f.Address.State())
                        .RuleFor(u => u.Land, f => f.Address.County())
                        .FinishWith((f, u) =>
                        {
                            Console.WriteLine("AccomodatieLocatie created with Bogus: Id= {0} !", u.Id);
                        });
                    
                    var accomodatieLocaties = new List<AccomodatieLocatie>();
                    
                    var accomodatieLocatie = accomodatieLocatieSkeleton.Generate();
                    accomodatieLocaties.Add(accomodatieLocatie);

                    context.AccomodatieLocaties.AddRange(accomodatieLocaties);
                    await context.SaveChangesAsync();
                }

                // Categorie
                if(!context.Categorieën.Any()) 
                {
                    var lorem = new Bogus.DataSets.Lorem(locale: "nl");
                    var categorieSkeleton = new Faker<Accomo.Models.Categorie>()
                        .RuleFor(u => u.Naam, f => lorem.Word())
                        .RuleFor(u => u.Beschrijving, f => String.Join(" ", lorem.Words(5)))
                        .FinishWith((f, u) =>
                        {
                            Console.WriteLine("Categorie created with Bogus {0} !", u.Naam);
                        });
                    
                    var categorieën = new List<Categorie>();

                    for(var i = 0; i < 10;i++)
                    {
                        var categorie = categorieSkeleton.Generate();
                        categorieën.Add(categorie);
                    }

                    context.Categorieën.AddRange(categorieën);
                    await context.SaveChangesAsync();
                }

                // Huisregel
                if(!context.Huisregels.Any()) 
                {
                    var lorem = new Bogus.DataSets.Lorem(locale: "nl");
                    var huisregelSkeleton = new Faker<Accomo.Models.Huisregel>()
                        .RuleFor(u => u.Naam, f => lorem.Word())
                        .RuleFor(u => u.Beschrijving, f => String.Join(" ", lorem.Words(10)))
                        .FinishWith((f, u) =>
                        {
                            Console.WriteLine("Huisregel created with Bogus {0} !", u.Naam);
                        });
                    
                    var huisregels = new List<Huisregel>();

                    for(var i = 0; i < 20; i++)
                    {
                        var huisregel = huisregelSkeleton.Generate();
                        huisregels.Add(huisregel);
                    }

                    context.Huisregels.AddRange(huisregels);
                    await context.SaveChangesAsync();
                }

                // Veiligheidsvoorziening
                if(!context.Veiligheidsvoorzieningen.Any()) 
                {
                    var lorem = new Bogus.DataSets.Lorem(locale: "nl");
                    var veiligheidsvoorzieningSkeleton = new Faker<Accomo.Models.Veiligheidsvoorziening>()
                        .RuleFor(u => u.Naam, f => lorem.Word())
                        .RuleFor(u => u.Beschrijving, f => String.Join(" ", lorem.Words(7)))
                        .FinishWith((f, u) =>
                        {
                            Console.WriteLine("Veiligheidsvoorziening created with Bogus {0} !", u.Naam);
                        });
                    
                    var veiligheidsvoorzieningen = new List<Veiligheidsvoorziening>();

                    for(var i = 0; i < 15; i++)
                    {
                        var veiligheidsvoorziening = veiligheidsvoorzieningSkeleton.Generate();
                        veiligheidsvoorzieningen.Add(veiligheidsvoorziening);
                    }

                    context.Veiligheidsvoorzieningen.AddRange(veiligheidsvoorzieningen);
                    await context.SaveChangesAsync();
                }

                // Accomodatie
                if(!context.Accomodaties.Any()) 
                {
                    var lorem = new Bogus.DataSets.Lorem(locale: "nl");
                    var accomodatieSkeleton = new Faker<Accomo.Models.Accomodatie>()
                        .RuleFor(u => u.Naam, f => lorem.Sentence(5))
                        .RuleFor(u => u.Beschrijving, f => lorem.Paragraph(1))
                        .RuleFor(u => u.MaxAantalGasten, f => (short)random.Next(1,6))
                        .RuleFor(u => u.BadkamerAantal, f => (short) random.Next(1,3))
                        .RuleFor(u => u.Beschikbaarheid, f => random.Next(1) == 0)
                        .RuleFor(u => u.MinLengteVerblijf, f => Convert.ToString(random.Next(1, 10)))
                        .RuleFor(u => u.UserId, f => FKUserID)
                        .RuleFor(u => u.AccomodatieLocatieId, f => (short) 1)                        
                        .RuleFor(u => u.AccomodatiePrijsId, f => (short) 1)                        
                        .RuleFor(u => u.AccomodatieTypeId, f => (short) 1)
                        .RuleFor(u => u.CategorieId, f => (short) 1)
                        .FinishWith((f, u) =>
                        {
                            Console.WriteLine("Accomodatie created with Bogus: Id= {0} !", u.Id);
                        });
                    
                    var accomodaties = new List<Accomodatie>();
                    
                    //for(var i = 0; i < 2; i++)
                    //{
                    var accomodatie = accomodatieSkeleton.Generate();
                    accomodaties.Add(accomodatie);
                    //}                 

                    context.Accomodaties.AddRange(accomodaties);
                    await context.SaveChangesAsync();
                }

                // Accomodatie Details
                if(!context.AccomodatieDetails.Any()) 
                {
                    var lorem = new Bogus.DataSets.Lorem(locale: "nl");
                    var accomodatiedetailSkeleton = new Faker<Accomo.Models.AccomodatieDetails>()
                        .RuleFor(u => u.Naam, f => lorem.Sentence(2))
                        .RuleFor(u => u.Beschrijving, f => lorem.Paragraphs(1))
                        .RuleFor(u => u.AccomodatieId, f => (short)1)                        
                        .FinishWith((f, u) =>
                        {
                            Console.WriteLine("Accomodatiedetail created with Bogus: Id= {0}", u.Id);
                        });
                    
                    var accomodatiedetails = new List<AccomodatieDetails>();

                    var accomodatiedetail = accomodatiedetailSkeleton.Generate();
                    accomodatiedetails.Add(accomodatiedetail);

                    context.AccomodatieDetails.AddRange(accomodatiedetails);
                    await context.SaveChangesAsync();
                }

                // Accomodatie Media
                if(!context.AccomodatieMedia.Any()) 
                {
                    var lorem = new Bogus.DataSets.Lorem(locale: "nl");
                    var mediumSkeleton = new Faker<Accomo.Models.AccomodatieMedia>()
                        .RuleFor(u => u.Naam, f => lorem.Sentence(3))
                        .RuleFor(u => u.URL, f => f.Internet.Url())
                        .RuleFor(u => u.AccomodatieId, f => (short)1)
                        .RuleFor(u => u.MediaTypeId, f => (short)1)                        
                        .FinishWith((f, u) =>
                        {
                            Console.WriteLine("Accomodatie Media created with Bogus: Id= {0} !", u.Id);
                        });
                    
                    var media = new List<AccomodatieMedia>();

                    var medium = mediumSkeleton.Generate();
                    media.Add(medium);

                    context.AccomodatieMedia.AddRange(media);
                    await context.SaveChangesAsync();
                }

                // AccomodatieReview
                if(!context.AccomodatieReviews.Any()) 
                {
                    var lorem = new Bogus.DataSets.Lorem(locale: "nl");
                    var accomodatieReviewSkeleton = new Faker<Accomo.Models.AccomodatieReview>()
                        .RuleFor(u => u.Titel, f => lorem.Sentence(4))
                        .RuleFor(u => u.Inhoud, f => lorem.Paragraphs(3))
                        .RuleFor(u => u.Beoordeling, f => (short) random.Next(1,5))
                        .RuleFor(u => u.AccomodatieId, f => (short) 1)
                        .RuleFor(u => u.UserId, f => FKUserID)                        
                        .FinishWith((f, u) =>
                        {
                            Console.WriteLine("AccomodatieReview created with Bogus: Id= {0} !", u.Id);
                        });
                    
                    var accomodatieReviews = new List<AccomodatieReview>();

                    var accomodatieReview = accomodatieReviewSkeleton.Generate();
                    accomodatieReviews.Add(accomodatieReview);

                    context.AccomodatieReviews.AddRange(accomodatieReviews);
                    await context.SaveChangesAsync();
                }
                
                // AccomodatieScore 
                if(!context.AccomodatieScores.Any()) 
                {
                    var lorem = new Bogus.DataSets.Lorem(locale: "nl");
                    var accomodatieScoreSkeleton = new Faker<Accomo.Models.AccomodatieScore>()
                        .RuleFor(u => u.Naam, f => lorem.Word())
                        .RuleFor(u => u.Score, f => (short)random.Next(1,5))
                        .RuleFor(u => u.AccomodatieReviewId, f => (short) 1)
                        .FinishWith((f, u) =>
                        {
                            Console.WriteLine("AccomodatieScore created with Bogus: Id= {0} !", u.Id);
                        });
                    
                    var accomodatieScores = new List<AccomodatieScore>();

                    var accomodatieScore = accomodatieScoreSkeleton.Generate();
                    accomodatieScores.Add(accomodatieScore);

                    context.AccomodatieScores.AddRange(accomodatieScores);
                    await context.SaveChangesAsync();
                }
                // Korting 
                if(!context.Kortingen.Any())
                {
                    var lorem = new Bogus.DataSets.Lorem(locale: "nl");
                    var kortingSkeleton = new Faker<Accomo.Models.Korting>()
                        .RuleFor(u => u.Naam, f => lorem.Word())
                        .RuleFor(u => u.Beschrijving, f => lorem.Paragraph(1))
                        .RuleFor(u => u.Percentage, f => f.Random.Double(5, 80))
                        .FinishWith((f, u) =>
                        {
                            Console.WriteLine("Korting created with Bogus: Name= {0} !", u.Naam);
                        });
                    
                    var kortingen = new List<Korting>();

                    var korting = kortingSkeleton.Generate();
                    kortingen.Add(korting);

                    context.Kortingen.AddRange(kortingen);
                    await context.SaveChangesAsync();
                }
                
                // Boeking
                if(!context.Boekingen.Any()) 
                {
                    var lorem = new Bogus.DataSets.Lorem(locale: "nl");
                    var boekingSkeleton = new Faker<Accomo.Models.Boeking>()
                        .RuleFor(u => u.PrijsPerDag, f => (double) 100)
                        .RuleFor(u => u.PrijsPerVerblijf, f => (double) 50)
                        .RuleFor(u => u.TotaalBedrag, f => (double) 500)
                        .RuleFor(u => u.CheckIn, f => f.Date.Recent())
                        .RuleFor(u => u.CheckOut, f => f.Date.Future())
                        .RuleFor(u => u.AantalGasten, f => random.Next(1, 5))
                        .RuleFor(u => u.AnnuleringsDatum, f => f.Date.Between(f.Date.Recent(), f.Date.Future()))
                        .RuleFor(u => u.AccomodatieId, f => (short) 1)
                        .RuleFor(u => u.UserId, f => FKUserID)
                        .FinishWith((f, u) =>
                        {
                            Console.WriteLine("Boeking created with Bogus: Id= {0} !", u.Id);
                        });
                    
                    var boekingen = new List<Boeking>();

                    var boeking = boekingSkeleton.Generate();
                    boekingen.Add(boeking);

                    context.Boekingen.AddRange(boekingen);
                    await context.SaveChangesAsync();
                }
                // Transactie
                if(!context.Transacties.Any()) 
                {
                    var lorem = new Bogus.DataSets.Lorem(locale: "nl");
                    var transactiesSkeleton = new Faker<Accomo.Models.Transactie>()
                        .RuleFor(u => u.TotaalBedrag, f => (double) 200)
                        .RuleFor(u => u.BetaaldBedrag, f => (double) 200)
                        .RuleFor(u => u.TransferAt, f => f.Date.Recent())
                        .RuleFor(u => u.AccomodatieId, f => (short) 1)
                        .RuleFor(u => u.KortingId, f => (short) 1)
                        .RuleFor(u => u.BoekingId, f => (short) 1)
                        .RuleFor(u => u.ValutaId, f => (short) 1)
                        .FinishWith((f, u) =>
                        {
                            Console.WriteLine("Transactie created with Bogus: Id= {0} !", u.Id);
                        });
                    
                    var transacties = new List<Transactie>();

                    var transactie = transactiesSkeleton.Generate();
                    transacties.Add(transactie);

                    context.Transacties.AddRange(transacties);
                    await context.SaveChangesAsync();
                }

            }
        }

        private static DateTime GenerateDateTime(int yFrom, int yTo, int mFrom, int mTo, int dFrom, int dTo) {
            var random = new Random();
            try
            {
                return new DateTime(random.Next(yFrom, yTo), random.Next(mFrom, mTo), random.Next(dFrom, dTo));
            }
            catch(Exception ex) {
                return GenerateDateTime(yFrom, yTo, mFrom, mTo, dFrom, dTo);
            }
        }
    }
}