using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using OpenIddict;
using OpenIddict.Core;
using OpenIddict.EntityFrameworkCore;
using OpenIddict.Mvc;
using Npgsql.EntityFrameworkCore.PostgreSQL;
using Accomo.Models;
using Accomo.Models.Security;

namespace Accomo.Db
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, Guid>
    {
        public DbSet<Accomodatie> Accomodaties { get; set; }
        public DbSet<AccomodatieType> AccomodatieTypes { get; set; }
        public DbSet<AccomodatieLocatie> AccomodatieLocaties { get; set; }
        public DbSet<AccomodatieReview> AccomodatieReviews { get; set; }
        public DbSet<BedType> BedTypes { get; set; }
        public DbSet<MediaType> Mediatypes { get; set; }
        public DbSet<AccomodatieMedia> AccomodatieMedia { get; set; }
        public DbSet<AccomodatieDetails> AccomodatieDetails { get; set; }
        public DbSet<AccomodatiePrijs> AccomodatiePrijzen { get; set; }
        public DbSet<AccomodatieScore> AccomodatieScores { get; set; }
        public DbSet<VoorzieningType> VoorzieningTypes { get; set; }
        public DbSet<Wishlist> Wishlists { get; set; }
        public DbSet<Categorie> Categorieën { get; set; }
        public DbSet<Boeking> Boekingen { get; set; }
        public DbSet<Transactie> Transacties { get; set; }
        public DbSet<Valuta> Valutas { get; set; }
        public DbSet<Huisregel> Huisregels { get; set; }
        public DbSet<Veiligheidsvoorziening> Veiligheidsvoorzieningen { get; set; }
        public DbSet<Korting> Kortingen { get; set;}
        public DbSet<ApplicationUser> ApplicationUsers { get; set;}
        public DbSet<ApplicationRole> ApplicationRoles { get; set;}

        
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.HasPostgresExtension("uuid-ossp");

            base.OnModelCreating(builder);

            // Model: ApplicationRole
            builder.Entity<ApplicationRole>()
                .Property(u => u.Beschrijving)
                .HasMaxLength(511);

            builder.Entity<ApplicationRole>()
                .Property(u => u.CreatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAdd();

            builder.Entity<ApplicationRole>()
                .Property(u => u.UpdatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAddOrUpdate();


            // Model: Accomodatie
            builder.Entity<Accomodatie>()
                .HasKey(a => a.Id);

            builder.Entity<Accomodatie>()
                .Property(a => a.Naam)
                .HasMaxLength(255)
                .IsRequired();

            builder.Entity<Accomodatie>()
                .Property(a => a.Beschrijving)
                .IsRequired();

            builder.Entity<Accomodatie>()
                .Property(a => a.MaxAantalGasten)
                .IsRequired();

            builder.Entity<Accomodatie>()
                .Property(a => a.BadkamerAantal)
                .IsRequired();
            
            builder.Entity<Accomodatie>()
                .Property(a => a.Beschikbaarheid)
                .IsRequired();

            builder.Entity<Accomodatie>()
                .Property(a => a.MinLengteVerblijf);

            builder.Entity<Accomodatie>()
                .Property(a => a.CreatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAdd();

            builder.Entity<Accomodatie>()
                .Property(a => a.UpdatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAddOrUpdate();

            // One-To-One in EF7 (10-10-2016)
            // Accomodatie kan één prijs hebben
            // Prijs kan meerdere Accomodaties hebben
            builder.Entity<Accomodatie>()
                .HasOne(a => a.AccomodatiePrijs)
                .WithOne(p => p.Accomodatie)
                .HasForeignKey<Accomodatie>(a => a.AccomodatiePrijsId);

            // One-To-One in EF7 (10-10-2016)
            // Accomodatie kan één AccomodatieLocatie hebben
            // AccomodatieLocatie kan één Accomodatie hebben
            builder.Entity<Accomodatie>()
                .HasOne(a => a.AccomodatieLocatie)
                .WithOne(al => al.Accomodatie)
                .HasForeignKey<Accomodatie>(a => a.AccomodatieLocatieId);

            // One-To-Many in EF7 (10-10-2016)
            // Accomodatie kan één Type hebben
            // Accomodatie Type kan meerdere Accomodaties hebben
            builder.Entity<Accomodatie>()
                .HasOne(a => a.AccomodatieType)
                .WithMany(at => at.Accomodaties)
                .HasForeignKey(a => a.AccomodatieTypeId);

            // One-To-Many in EF7 (10-10-2016)
            // Accomodatie kan één Categorie hebben
            // Categorie kan meerdere Accomodaties hebben
            builder.Entity<Accomodatie>()
                .HasOne(a => a.Categorie)
                .WithMany(ca => ca.Accomodaties)
                .HasForeignKey(a => a.CategorieId);
            
            // One-To-Many in EF7 (10-10-2016)
            // Accomodatie kan één User hebben
            // User kan meerdere Accomodaties hebben
            builder.Entity<Accomodatie>()
                .HasOne(a => a.User)
                .WithMany(u => u.Accomodaties)
                .HasForeignKey(a => a.UserId);
                
            // Model: AccomodatieReview
            builder.Entity<AccomodatieReview>()
                .HasKey(r => r.Id);

            builder.Entity<AccomodatieReview>()
                .Property(r => r.Titel)
                .HasMaxLength(255)
                .IsRequired();

            builder.Entity<AccomodatieReview>()
                .Property(r => r.Inhoud)
                .IsRequired();

            builder.Entity<AccomodatieReview>()
                .Property(r => r.Beoordeling)
                .IsRequired();

            builder.Entity<AccomodatieReview>()
                .Property(r => r.CreatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAdd();

            builder.Entity<AccomodatieReview>()
                .Property(r => r.UpdatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAddOrUpdate();

            // One-To-Many in EF7 (10-10-2016)
            // AccomodatieReview kan één accomodatie hebben
            // Accomodatie kan meerdere AccomodatieReviews hebben
            builder.Entity<AccomodatieReview>()
                .HasOne(r => r.Accomodatie)
                .WithMany(a => a.AccomodatieReviews)
                .HasForeignKey(r => r.AccomodatieId);
                
            // One-To-Many in EF7 (10-10-2016)
            // User kan één Review hebben
            // Review kan meerdere AccomodatieScores hebben
            builder.Entity<AccomodatieReview>()
                .HasOne(ar => ar.User)
                .WithMany(u => u.AccomodatieReviews)
                .HasForeignKey(ar => ar.UserId);
                    
            // Model: AccomodatiePrijs
            builder.Entity<AccomodatiePrijs>()
                .HasKey(ap => ap.Id);

            builder.Entity<AccomodatiePrijs>()
                .Property(ap => ap.Prijs)
                .IsRequired();

            builder.Entity<AccomodatiePrijs>()
                .Property(ap => ap.PrijsTypeNaam);

            builder.Entity<AccomodatiePrijs>()
                .Property(ap => ap.PrijsTypePercentage);

            builder.Entity<AccomodatiePrijs>()
                .Property(ap => ap.CreatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAdd();

            builder.Entity<AccomodatiePrijs>()
                .Property(ap => ap.UpdatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAddOrUpdate();

            // One-To-Many in EF7 (10-10-2016)
            // AccomodatiePrijs kan één Valuta hebben
            // Valuta kan meerdere AccomodatiePrijzen hebben
            builder.Entity<AccomodatiePrijs>()
                .HasOne(ap => ap.Valuta)
                .WithMany(v => v.AccomodatiePrijzen)
                .HasForeignKey(ap => ap.ValutaId);

            // Model: Categorie
            builder.Entity<Categorie>()
                .HasKey(ca => ca.Id);

            builder.Entity<Categorie>()
                .Property(ca => ca.Naam);

            builder.Entity<Categorie>()
                .Property(ca => ca.Beschrijving);

            builder.Entity<Categorie>()
                .Property(ca => ca.CreatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAdd();

            builder.Entity<Categorie>()
                .Property(ca => ca.UpdatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAddOrUpdate();

            // Model: Huisregel
            builder.Entity<Huisregel>()
                .HasKey(hr => hr.Id);

            builder.Entity<Huisregel>()
                .Property(hr => hr.Naam);

            builder.Entity<Huisregel>()
                .Property(hr => hr.Beschrijving);

            builder.Entity<Huisregel>()
                .Property(hr => hr.CreatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAdd();

            builder.Entity<Huisregel>()
                .Property(hr => hr.UpdatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAddOrUpdate();

            // Many-To-Many in EF7 (10-10-2016)
            // Accomodatie can have many Huisregels
            // Huisregels can be assigned to many accomodaties
            builder.Entity<AccomodatieHuisregel>()
                .HasKey(ah => new { ah.AccomodatieId, ah.HuisregelId });

            builder.Entity<AccomodatieHuisregel>()
                .HasOne(ah => ah.Accomodatie)
                .WithMany(a => a.Huisregels)
                .HasForeignKey(ah => ah.AccomodatieId);

            builder.Entity<AccomodatieHuisregel>()
                .HasOne(ah => ah.Huisregel)
                .WithMany(hr => hr.Accomodaties)
                .HasForeignKey(ah => ah.HuisregelId);
            
            // Model: Veiligheidsvoorziening
            builder.Entity<Veiligheidsvoorziening>()
                .HasKey(vv => vv.Id);

            builder.Entity<Veiligheidsvoorziening>()
                .Property(vv => vv.Naam);

            builder.Entity<Veiligheidsvoorziening>()
                .Property(vv => vv.Beschrijving);

            builder.Entity<Veiligheidsvoorziening>()
                .Property(vv => vv.CreatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAdd();

            builder.Entity<Veiligheidsvoorziening>()
                .Property(vv => vv.UpdatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAddOrUpdate();

            // Many-To-Many in EF7 (10-10-2016)
            // Accomodatie can have many Veiligheidsvoorzieningen
            // Veiligheidsvoorziening can be assigned to many accomodaties
            builder.Entity<AccomodatieVeiligheidsvoorziening>()
                .HasKey(avv => new { avv.AccomodatieId, avv.VeiligheidsvoorzieningId });

            builder.Entity<AccomodatieVeiligheidsvoorziening>()
                .HasOne(avv => avv.Accomodatie)
                .WithMany(a => a.Veiligheidsvoorzieningen)
                .HasForeignKey(avv => avv.AccomodatieId);

            builder.Entity<AccomodatieVeiligheidsvoorziening>()
                .HasOne(avv => avv.Veiligheidsvoorziening)
                .WithMany(vv => vv.Accomodaties)
                .HasForeignKey(avv => avv.VeiligheidsvoorzieningId);

            // Model: Bed Type
            builder.Entity<BedType>()
                .HasKey(bt => bt.Id);

            builder.Entity<BedType>()
                .Property(bt => bt.Naam)
                .HasMaxLength(255)
                .IsRequired();

            builder.Entity<BedType>()
                .Property(bt => bt.Beschrijving)
                .IsRequired();

            builder.Entity<BedType>()
                .Property(bt => bt.CreatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAdd();

            builder.Entity<BedType>()
                .Property(bt => bt.UpdatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAddOrUpdate();

            // Many-To-Many in EF7 (10-10-2016)
            // Accomodatie can have many Bed Types
            // Bed Type can be assigned to many Accomodaties
            builder.Entity<BedTypeAccomodatie>()
                .HasKey(bta => new { bta.BedTypeId, bta.AccomodatieId });

            builder.Entity<BedTypeAccomodatie>()
                .HasOne(bta => bta.Accomodatie)
                .WithMany(a => a.BedTypes)
                .HasForeignKey(bta => bta.AccomodatieId);

            builder.Entity<BedTypeAccomodatie>()
                .HasOne(bta => bta.BedType)
                .WithMany(a => a.Accomodaties)
                .HasForeignKey(bta => bta.BedTypeId);


            // Model: AccomodatieDetails
            builder.Entity<AccomodatieDetails>()
                .HasKey(ad => ad.Id);

            builder.Entity<AccomodatieDetails>()
                .Property(ad => ad.Naam)
                .HasMaxLength(255)
                .IsRequired();

            builder.Entity<AccomodatieDetails>()
                .Property(ad => ad.Beschrijving)
                .IsRequired();

            builder.Entity<AccomodatieDetails>()
                .Property(ad => ad.CreatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAdd();

            builder.Entity<AccomodatieDetails>()
                .Property(ad => ad.UpdatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAddOrUpdate();

            // One-To-Many in EF7 (10-10-2016)
            // Detail kan één accomodatie hebben
            // Accomodatie kan meerdere details hebben
            builder.Entity<AccomodatieDetails>()
                .HasOne(ad => ad.Accomodatie)
                .WithMany(a => a.AccomodatieDetails)
                .HasForeignKey(ad => ad.AccomodatieId);

            // Model: Accomodatie Type
            builder.Entity<AccomodatieType>()
                .HasKey(at => at.Id);

            builder.Entity<AccomodatieType>()
                .Property(at => at.Naam)
                .HasMaxLength(255)
                .IsRequired();

            builder.Entity<AccomodatieType>()
                .Property(at => at.Beschrijving)
                .IsRequired();

            builder.Entity<AccomodatieType>()
                .Property(at => at.CreatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAdd();

            builder.Entity<AccomodatieType>()
                .Property(at => at.UpdatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAddOrUpdate();

            // Model: Accomodatie Media
            builder.Entity<AccomodatieMedia>()
                .HasKey(am => am.Id);

            builder.Entity<AccomodatieMedia>()
                .Property(am => am.Naam)
                .HasMaxLength(255)
                .IsRequired();

            builder.Entity<AccomodatieMedia>()
                .Property(am => am.URL)
                .IsRequired();

            builder.Entity<AccomodatieMedia>()
                .Property(am => am.CreatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAdd();

            builder.Entity<AccomodatieMedia>()
                .Property(am => am.UpdatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAddOrUpdate();

            // One-To-Many in EF7 (10-10-2016)
            // Media kan één accomodatie hebben
            // Accomodatie kan meerdere Media hebben
            builder.Entity<AccomodatieMedia>()
                .HasOne(am => am.Accomodatie)
                .WithMany(a => a.AccomodatieMedia)
                .HasForeignKey(am => am.AccomodatieId);

            // One-To-Many in EF7 (10-10-2016)
            // Media kan één Media Type hebben
            // Media Type kan meerdere Media hebben
            builder.Entity<AccomodatieMedia>()
                .HasOne(am => am.MediaType)
                .WithMany(mt => mt.AccomodatieMedia)
                .HasForeignKey(am => am.MediaTypeId);

            // Model: Media Type
            builder.Entity<MediaType>()
                .HasKey(mt => mt.Id);

            builder.Entity<MediaType>()
                .Property(mt => mt.Naam)
                .HasMaxLength(255)
                .IsRequired();

             builder.Entity<MediaType>()
                .Property(mt => mt.Beschrijving);

            builder.Entity<MediaType>()
                .Property(mt => mt.CreatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAdd();

            builder.Entity<MediaType>()
                .Property(mt => mt.UpdatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAddOrUpdate();


            // Model: Voorziening Type
            builder.Entity<VoorzieningType>()
                .HasKey(vt => vt.Id);

            builder.Entity<VoorzieningType>()
                .Property(vt => vt.Naam)
                .HasMaxLength(255)
                .IsRequired();

             builder.Entity<VoorzieningType>()
                .Property(vt => vt.Beschrijving);

            builder.Entity<VoorzieningType>()
                .Property(vt => vt.CreatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAdd();

            builder.Entity<VoorzieningType>()
                .Property(vt => vt.UpdatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAddOrUpdate();

            // Many-To-Many in EF7 (10-10-2016)
            // Accomodatie can have many Voorzieningen
            // Voorziening Type can be assigned to many accomodatie
            builder.Entity<AccomodatieVoorzieningType>()
                .HasKey(avt => new { avt.AccomodatieId, avt.VoorzieningTypeId });

            builder.Entity<AccomodatieVoorzieningType>()
                .HasOne(avt => avt.Accomodatie)
                .WithMany(a => a.Voorzieningen)
                .HasForeignKey(avt => avt.AccomodatieId);

            builder.Entity<AccomodatieVoorzieningType>()
                .HasOne(avt => avt.VoorzieningType)
                .WithMany(vt => vt.Accomodaties)
                .HasForeignKey(avt => avt.VoorzieningTypeId);

            // Model: Wishlist
            builder.Entity<Wishlist>()
                .HasKey(w => w.Id);

            builder.Entity<Wishlist>()
                .Property(w => w.Naam)
                .HasMaxLength(255)
                .IsRequired();

            builder.Entity<Wishlist>()
                .Property(w => w.CreatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAdd();

            builder.Entity<Wishlist>()
                .Property(w => w.UpdatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAddOrUpdate();
                
            // One-To-Many in EF7 (10-10-2016)
            // Wishlist kan één User hebben
            // User kan meerdere Wishlists hebben
            builder.Entity<Wishlist>()
                .HasOne(w => w.User)
                .WithMany(u => u.Wishlists)
                .HasForeignKey(w => w.UserId);

            

            // Many-To-Many in EF7 (10-10-2016)
            // Accomodatie can have many wishlists
            // wishlist can be assigned to many accomodaties
            builder.Entity<AccomodatieWishlist>()
                .HasKey(aw => new { aw.AccomodatieId, aw.WishlistId });

            builder.Entity<AccomodatieWishlist>()
                .HasOne(aw => aw.Accomodatie)
                .WithMany(a => a.Wishlists)
                .HasForeignKey(aw => aw.AccomodatieId);

            builder.Entity<AccomodatieWishlist>()
                .HasOne(aw => aw.Wishlist)
                .WithMany(w => w.Accomodaties)
                .HasForeignKey(aw => aw.WishlistId);
            
            

            // Model: AccomodatieLocatie
            builder.Entity<AccomodatieLocatie>()
                .HasKey(al => al.Id);

            builder.Entity<AccomodatieLocatie>()
                .Property(al => al.Straat)
                .IsRequired();

            builder.Entity<AccomodatieLocatie>()
                .Property(al => al.HuisNummer)
                .IsRequired();

            builder.Entity<AccomodatieLocatie>()
                .Property(al => al.KamerNummer);

            builder.Entity<AccomodatieLocatie>()
                .Property(al => al.Stad)
                .IsRequired();

            builder.Entity<AccomodatieLocatie>()
                .Property(al => al.PostCode)
                .IsRequired();

            builder.Entity<AccomodatieLocatie>()
                .Property(al => al.Streek);

            builder.Entity<AccomodatieLocatie>()
                .Property(al => al.Land)
                .IsRequired();

            builder.Entity<AccomodatieLocatie>()
                .Property(al => al.CreatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAdd();

            builder.Entity<AccomodatieLocatie>()
                .Property(al => al.UpdatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAddOrUpdate();

            // Model: Boeking
            builder.Entity<Boeking>()
                .HasKey(b => b.Id);

            builder.Entity<Boeking>()
                .Property(b => b.PrijsPerDag)
                .IsRequired();

            builder.Entity<Boeking>()
                .Property(b => b.PrijsPerVerblijf)
                .IsRequired();

            builder.Entity<Boeking>()
                .Property(b => b.TotaalBedrag)
                .IsRequired();

            builder.Entity<Boeking>()
                .Property(b => b.CheckIn)
                .IsRequired();

            builder.Entity<Boeking>()
                .Property(b => b.CheckOut)
                .IsRequired();

            builder.Entity<Boeking>()
                .Property(b => b.AantalGasten)
                .IsRequired();

            builder.Entity<Boeking>()
                .Property(b => b.AnnuleringsDatum)
                .IsRequired();

            builder.Entity<Boeking>()
                .Property(b => b.CreatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAdd();

            builder.Entity<Boeking>()
                .Property(b => b.UpdatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAddOrUpdate();
            
            // One-To-Many in EF7 (10-10-2016)
            // Boeking kan één User hebben
            // User kan meerdere Boekingen hebben
            builder.Entity<Boeking>()
                .HasOne(b => b.User)
                .WithMany(u => u.Boekingen)
                .HasForeignKey(b => b.UserId);
                
            // One-To-Many in EF7 (10-10-2016)
            // Boeking kan één Accomodatie hebben
            // Accomodatie kan meerdere Boekingen hebben
            builder.Entity<Boeking>()
                .HasOne(b => b.Accomodatie)
                .WithMany(a => a.Boekingen)
                .HasForeignKey(a => a.AccomodatieId);
                

            // Model: Transactie
            builder.Entity<Transactie>()
                .HasKey(t => t.Id);

            builder.Entity<Transactie>()
                .Property(t => t.TotaalBedrag)
                .IsRequired();

            builder.Entity<Transactie>()
                .Property(t => t.BetaaldBedrag)
                .IsRequired();

            builder.Entity<Transactie>()
                .Property(t => t.TransferAt);

            builder.Entity<Transactie>()
                .Property(t => t.CreatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAdd();

            builder.Entity<Transactie>()
                .Property(t => t.UpdatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAddOrUpdate();
            
            // One-To-Many in EF7 (10-10-2016)
            // Transactie kan één Accomodatie hebben
            // Accomodatie kan meerdere Transacties hebben
            builder.Entity<Transactie>()
                .HasOne(t => t.Accomodatie)
                .WithMany(a => a.Transacties)
                .HasForeignKey(t => t.AccomodatieId);
            
            // One-To-Many in EF7 (10-10-2016)
            // Transactie kan één Boeking hebben
            // Boeking kan meerdere Transacties hebben
            builder.Entity<Transactie>()
                .HasOne(t => t.Boeking)
                .WithMany(b => b.Transacties)
                .HasForeignKey(t => t.BoekingId);

            
            // One-To-Many in EF7 (10-10-2016)
            // Transactie kan één Korting hebben
            // Korting kan meerdere Transacties hebben
            builder.Entity<Transactie>()
                .HasOne(t => t.Korting)
                .WithMany(k => k.Transacties)
                .HasForeignKey(t => t.KortingId);

            // Model: Korting
            builder.Entity<Korting>()
                .HasKey(k => k.Id);

            builder.Entity<Korting>()
                .Property(k => k.Naam)
                .IsRequired();

            builder.Entity<Korting>()
                .Property(k => k.Beschrijving)
                .IsRequired();

            builder.Entity<Korting>()
                .Property(k => k.Percentage);

            builder.Entity<Korting>()
                .Property(k => k.CreatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAdd();

            builder.Entity<Korting>()
                .Property(k => k.UpdatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAddOrUpdate();

            // Model: Valuta
            builder.Entity<Valuta>()
                .HasKey(v => v.Id);

            builder.Entity<Valuta>()
                .Property(v => v.Naam)
                .IsRequired();

            builder.Entity<Valuta>()
                .Property(v => v.Code)
                .IsRequired();

            // Model: AccomondatieScore
            builder.Entity<AccomodatieScore>()
                .HasKey(sc => sc.Id);

            builder.Entity<AccomodatieScore>()
                .Property(sc => sc.Naam)
                .IsRequired();

            builder.Entity<AccomodatieScore>()
                .Property(sc => sc.Score)
                .IsRequired();

            builder.Entity<AccomodatieScore>()
                .Property(sc => sc.CreatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAdd();

            builder.Entity<AccomodatieScore>()
                .Property(sc => sc.UpdatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAddOrUpdate();

            // One-To-Many in EF7 (10-10-2016)
            // AccomodatieScore kan één Review hebben
            // Review kan meerdere AccomodatieScores hebben
            builder.Entity<AccomodatieScore>()
                .HasOne(sc => sc.AccomodatieReview)
                .WithMany(ar => ar.AccomodatieScores)
                .HasForeignKey(sc => sc.AccomodatieReviewId);

            // Model: ApplicationUser
            builder.Entity<ApplicationUser>()
                .HasKey(u => u.Id);

            builder.Entity<ApplicationUser>()
                .Property(u => u.FirstName)
                .HasMaxLength(255)
                .IsRequired();

            builder.Entity<ApplicationUser>()
                .Property(u => u.LastName)
                .HasMaxLength(255)
                .IsRequired();
                
            builder.Entity<ApplicationUser>()
                .Property(u => u.Gender)
                .IsRequired();

            builder.Entity<ApplicationUser>()
                .Property(u => u.plainPassword);

            builder.Entity<ApplicationUser>()
                .Property(u => u.Description)
                .HasMaxLength(511);

            builder.Entity<ApplicationUser>()
                .Property(sc => sc.CreatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAdd();

            builder.Entity<ApplicationUser>()
                .Property(sc => sc.UpdatedAt)
                .HasDefaultValueSql("now()")
                .ValueGeneratedOnAddOrUpdate();

            
            
        }
    }
}
