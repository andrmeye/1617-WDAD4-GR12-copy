using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Npgsql.EntityFrameworkCore.PostgreSQL;
using Accomo.Models;
using Accomo.Models.Security;

namespace Accomo.Db
{
    public class ApplicationDbContextFactory : IDbContextFactory<ApplicationDbContext>
    {
        public ApplicationDbContext Create(DbContextFactoryOptions options)
        {
            var builder = new DbContextOptionsBuilder<ApplicationDbContext>();
            builder.UseNpgsql("User ID=postgres;Password=postgres;Host=localhost;Port=5432;Database=accomo;Pooling=true;");
            return new ApplicationDbContext(builder.Options);
        }
    }
}